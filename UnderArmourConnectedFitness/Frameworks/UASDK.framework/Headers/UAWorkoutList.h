//
//  UAWorkoutList.h
//  UA
//
//  Created by Corey Roberts on 5/2/14.
//  Copyright (c) 2014 MapMyFitness Inc. All rights reserved.
//

#import <UASDK/UAEntityList.h>
#import <UASDK/UAEntityListRef.h>

@class UAUserRef, UAActivityTypeReference;

@interface UAWorkoutListRef : UAEntityListRef <NSCoding>

+ (UAWorkoutListRef *)workoutListRefWithUserReference:(UAUserRef *)userRef;

+ (UAWorkoutListRef *)workoutListRefWithUserReference:(UAUserRef *)userRef
                                        createdBefore:(NSDate *)createdBeforeDate;

@end

/**
 *  An object that represents a collection of UAWorkout objects.
 */
@interface UAWorkoutList : UAEntityList <UAEntityListRefInterface, NSCoding>

/**
 *  Reference to this object.
 */
@property (nonatomic, strong) UAWorkoutListRef *ref;

/**
 *  A reference to the previous collection of entities.
 *  @discussion If no more entities exist, this value will be nil.
 */
@property (nonatomic, strong) UAWorkoutListRef *previousRef;

/**
 *  A reference to the next collection of entities.
 *  @discussion If no more entities exist, this value will be nil.
 */
@property (nonatomic, strong) UAWorkoutListRef *nextRef;

@end
