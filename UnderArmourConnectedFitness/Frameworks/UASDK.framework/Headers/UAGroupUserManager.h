/*
 * UAGroupUserManager.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <Foundation/Foundation.h>
#import <UASDK/UAManager.h>

@class UAGroupUserRef, UAGroupUser;
@class UAGroupUserListRef, UAGroupUserList;

typedef void (^UAGroupUserResponseBlock)(UAGroupUser *object, NSError *error);
typedef void (^UAGroupUserListResponseBlock)(UAGroupUserList *object, NSError *error);

@interface UAGroupUserManager : UAManager

- (void)fetchGroupUsersWithListRef:(UAGroupUserListRef *)reference response:(UAGroupUserListResponseBlock)response;

- (void)createGroupUser:(UAGroupUser *)groupUser response:(UAGroupUserResponseBlock)response;

- (void)deleteGroupUser:(UAGroupUserRef *)reference response:(UAResponseBlock)response;

@end
