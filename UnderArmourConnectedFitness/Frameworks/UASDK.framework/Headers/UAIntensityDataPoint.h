//
//  UAIntensityDataPoint.h
//  UASDK
//
//  Created by Roberts, Corey on 12/9/14.
//  Copyright (c) 2014 Under Armour. All rights reserved.
//

#import <UASDK/UADataPoint.h>

@interface UAIntensityDataPoint : UADataPoint

/**
 *  Intensity (double, percentage).
 */
@property (nonatomic, readonly) NSNumber *intensity;

/**
 *  Initialize an Intensity data point.
 *  @param intensity Intensity (double, percentage, must not be nil).
 *  @return Initialized data point.
 */
- (instancetype)initWithIntensity:(NSNumber *)intensity;

@end
