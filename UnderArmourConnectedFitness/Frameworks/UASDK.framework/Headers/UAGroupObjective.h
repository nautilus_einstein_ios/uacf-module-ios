/*
 * UAGroupObjective.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <Foundation/Foundation.h>

#import <UASDK/UAValidationProtocol.h>

@class UACriteria, UADataTypeRef, UAPeriod;

@interface UAGroupObjective : NSObject <UAValidationProtocol, NSCoding>

@property (nonatomic, copy) NSString *name;

@property (nonatomic, strong) UADataTypeRef *dataTypeRef;

@property (nonatomic, copy) NSString *dataTypeField;

@property (nonatomic, strong) NSDate *startDatetime;

@property (nonatomic, strong) NSDate *endDatetime;

@property (nonatomic, strong) NSDate *iterationStartDatetime;

@property (nonatomic, strong) NSDate *iterationEndDatetime;

@property (nonatomic, strong) NSNumber *iteration;

@property (nonatomic, strong) UAPeriod *period;

@property (nonatomic, strong) UACriteria *criteria;

@end
