/*
 * UATimeSeriesTimerStop.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UATimeSeries.h>

@class UATimeSeriesTimerStopPoint;

@interface UATimeSeriesTimerStop : UATimeSeries <UATimeSeriesProtocol>

- (void)addPoint:(UATimeSeriesTimerStopPoint *)point;

- (UATimeSeriesTimerStopPoint *)pointAtIndex:(NSUInteger)index;

@end
