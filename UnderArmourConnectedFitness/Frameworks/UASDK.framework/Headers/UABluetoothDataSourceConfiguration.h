/*
 * UABluetoothDataSourceConfiguration.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UADataSourceConfiguration.h>
#import <UASDK/UABluetoothServiceTypes.h>

/**
 *  A producer configuration used for connecting to Bluetooth LE workout sensors.
 */
@interface UABluetoothDataSourceConfiguration : UADataSourceConfiguration <UADataSourceConfigurationInterface>

/**
 *  UUID of the device the producer will connect to. Required.
 */
@property (nonatomic, strong) NSUUID *deviceUUID;

/**
 *  Services supported by the device which the producer will attempt to
 *  discover and use. More than one service type can be combined using
 *  a bitwise OR. Required.
 */
@property (nonatomic) UABluetoothServiceType services;

@end
