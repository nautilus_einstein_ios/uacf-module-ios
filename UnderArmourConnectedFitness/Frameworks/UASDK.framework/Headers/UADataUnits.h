/*
 * UADataUnits.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <Foundation/Foundation.h>

@interface UADataUnits : NSObject <NSCoding>

/*
 * Symbol of the unit.
 */
@property (nonatomic, readonly) NSString *symbol;

/*
 * ID of the unit
 */
@property (nonatomic, readonly) NSString *unitsID;

+ (instancetype)beatsPerMinuteUnits;

+ (instancetype)degreeUnits;

+ (instancetype)jouleUnits;

+ (instancetype)kilogramUnits;

+ (instancetype)metersPerSecondUnits;

+ (instancetype)meterUnits;

+ (instancetype)secondUnits;

- (instancetype)init __attribute__((unavailable("init not available")));
+ (instancetype)new __attribute__((unavailable("new not available")));

@end
