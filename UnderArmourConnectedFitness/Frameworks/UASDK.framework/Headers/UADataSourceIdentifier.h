/*
 * UADataSourceIdentifier.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <Foundation/Foundation.h>

/**
 *  An object that gives identification values for a message producer.
 *  @discussion: This class is very much in flux and is slated to change pretty soon.
 */
@interface UADataSourceIdentifier : NSObject

@property (nonatomic, copy, readonly) NSString *manufacturerCode;
@property (nonatomic, copy, readonly) NSString *modelCode;
@property (nonatomic, copy, readonly) NSString *consumerKey;
@property (nonatomic, copy, readonly) NSString *installationId;
@property (nonatomic, copy, readonly) NSString *serialNumber;

- (instancetype)initWithManufacturerCode:(NSString *)manufacturerCode
							   modelCode:(NSString *)modelCode
							 consumerKey:(NSString *)consumerKey
						  installationId:(NSString *)installationId
							serialNumber:(NSString *)serialNumber NS_DESIGNATED_INITIALIZER;

@end
