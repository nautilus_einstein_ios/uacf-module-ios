/*
 * UARecorder.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "UASensorDataSource.h"

@class UAActivityTypeReference, UADataFrame, UARecorder, UARecorderConfiguration, UADataType, UADataTypeRef, UADataSourceConfiguration, UAHeartRateZonesRef;

/**
 *  A delegate that handles callbacks for observing data point types.
 */
@protocol UARecordDataObserver <NSObject>

/**
 *  A method that is triggered when data has been updated for a given UADataPointType.
 *
 *  @param data          The updated state of recording data.
 *  @param dataType      The UADatatype that is being observed.
 */
- (void)dataUpdated:(UADataFrame *)data forDataType:(UADataType *)dataType;

@end

/**
 *  A protocol designated to listen to the states of a given session.
 * 
 *  @discussion Note: This protocol may be removed in future releases. 
 *  Dependency on this is not recommended.
 */
@protocol UARecorderObserver <NSObject>

- (void)timeUpdated:(UADataFrame *)dataFrame;
- (void)segmentUpdated:(UADataFrame *)dataFrame;

- (void)recorderDidStart;
- (void)recorderDidResume;
- (void)recorderDidStop;
- (void)recorderDidPause;

@end

@interface UARecorder : NSObject

/**
 *  The current state of data for a recorder.
 */
@property (nonatomic, readonly) UADataFrame *dataFrame;

/**
 *  The name of this recorder. This can be set with UARecorderConfiguration.
 *  @see createRecorderWithRecorderConfiguration:completionHandler:.
 */
@property (nonatomic, copy, readonly) NSString *name;

/**
 *  A delegate that responds to a recorder's current state (if implemented).
 */
@property (nonatomic, weak) id<UARecorderObserver> delegate;

/**
 *  Initializes a recorder based on configurations.
 *
 *  @param recorderConfiguration A configuration object to set up the recorder.
 *
 *  @return A new, configured recorder, or an error if an error occurs.
 */
- (void)createRecorderWithRecorderConfiguration:(UARecorderConfiguration *)recorderConfiguration
							  completionHandler:(void (^)(UARecorder *recorder, NSError *error))completionHandler;


/**
 *  Updates the activity type of a recorder.
 *
 *  @param activityTypeRef   A reference to an activity type.
 *
 *  @discussion You must first create a UARecorder using createRecorderWithRecorderConfiguration:completionHandler: if you intend to update
 *  the activity type.
 */
- (void)setActivityType:(UAActivityTypeReference *)activityTypeRef;

/**
 *  Updates the user's heart rate zones in the recorder.
 *
 *  @param heartRateZonesRef A reference to a heart rate zones object.
 *
 *  @discussion You must first create a UARecorder using createRecorderWithRecorderConfiguration:completionHandler: if you intend to update
 *  the heart rate zones.
 */
- (void)setHeartRateZones:(UAHeartRateZonesRef *)heartRateZonesRef;

/**
 *  Adds a data source to the recorder with a designated configuration.
 *
 *  @param dataSourceConfiguration A configuration for a data source.
 */
- (void)addDataSourceConfiguration:(UADataSourceConfiguration *)dataSourceConfiguration;

/**
 *  Adds a list of data source configurations to the recorder.
 *
 *  @param dataSourceConfigurations An array of UADataSourceConfiguration objects.
 */
- (void)addDataSourceConfigurations:(NSArray *)dataSourceConfigurations;

/**
 *  Removes a data source from the recorder with a given name.
 *
 *  @param name A data source's name.
 */
- (BOOL)removeDataSourceWithName:(NSString *)dataSourceIdentifier;

/**
 *  Gets the current list of data sources connected to this recorder.
 *
 *  @return A list of data sources.
 */
- (NSArray *)dataSources;

/**
 *  Starts or resumes a session.
 */
- (void)startSegment;

/**
 *  Pauses a session.
 */
- (void)stopSegment;

/**
 *  Destroys a session.
 */
- (void)destroy;

/**
 *  Adds a list of data point types to observer.
 *
 *  @param observer      An object to register as an observer.
 *  @param dataType		 A list of UADataTypeRefs to observe.
 */
- (void)addObserver:(id<UARecordDataObserver>)observer forDataTypeRefs:(NSArray *)dataTypeRefs;

/**
 *  Removes an observer from any data types it has registered for.
 *
 *  @param observer An object to remove as an observer.
 */
- (void)removeObserver:(id<UARecordDataObserver>)observer;

/**
 *  Adds an observer for a data source of a given name.
 *
 *  @param name The name of the sensor data source.
 */
- (void)addSensorDataSourceObserver:(id<UASensorDataSourceObserver>)observer withName:(NSString *)name;

/**
 *  Removes an observer for a data source of a given name.
 *
 *  @param name The name of the sensor data source.
 */
- (void)removeSensorDataSourceObserver:(id<UASensorDataSourceObserver>)observer withName:(NSString *)name;

@end
