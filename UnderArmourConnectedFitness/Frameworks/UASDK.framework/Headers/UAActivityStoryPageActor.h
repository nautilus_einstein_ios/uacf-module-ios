//
//  UAActivityStoryPageActor.h
//  UA
//
//  Created by Jeremy Zedell on 5/29/14.
//  Copyright (c) 2014 MapMyFitness Inc. All rights reserved.
//

#import <UASDK/UAActivityStoryActor.h>
#import <UASDK/UAPageRef.h>

@class UAImage;

@interface UAActivityStoryPageActor : UAActivityStoryActor

@property (nonatomic, strong) UAPageRef *pageReference;

@property (nonatomic, copy) NSString *alias;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, strong) UAImage *profilePhoto;
@property (nonatomic, strong) UAImage *coverPhoto;

@end
    
