/*
 * UAWorkout.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAEntity.h>
#import <UASDK/UAEntityRef.h>

@class UARouteRef, UAPrivacyRef, UAUserRef, UAActivityTypeReference, UAWorkoutAggregate, UATimeSeries, UAWorkoutTimeSeries,
       UATimeSeriesSpeed, UATimeSeriesPower, UATimeSeriesDistance, UATimeSeriesPosition, UATimeSeriesTimerStop, UATimeSeriesCadence,
       UATimeSeriesHeartRate;

@interface UAWorkoutRef : UAEntityRef

@end

/**
 The UAWorkout object is the object that makes up a workout
 */
@interface UAWorkout : UAEntity <UAEntityInterface, NSCoding>

/**
 *  The reference to this workout.
 */
@property (nonatomic, strong) UAWorkoutRef *ref;

/**
 *  The privacy reference to this workout.
 */
@property (nonatomic, strong) UAPrivacyRef *privacyRef;

/**
 *  The route reference to this workout.
 */
@property (nonatomic, strong) UARouteRef *routeRef;

/**
 *  The user reference for this workout.
 */
@property (nonatomic, strong) UAUserRef *userRef;

/**
 *  The user reference for this workout.
 */
@property (nonatomic, strong) UAActivityTypeReference *activityTypeRef;

/**
 The workout name
 */
@property (nonatomic, copy) NSString *workoutName;

/**
 *  Notes for the workout
 */
@property (nonatomic, copy) NSString *notes;

/**
 Represents a workout uniquely in environment in which the workout was recorded.
 */
@property (nonatomic, copy) NSString *workoutReferenceKey;

/**
 The name of the environment that recorded the workout
 */
@property (nonatomic, copy) NSString *workoutSource;

/**
 The date the workout was created on
 */
@property (nonatomic, strong) NSDate *createdDatetime;

/**
 The date the workout started
 */
@property (nonatomic, strong) NSDate *startDatetime;

/**
 The locale timezone for the workout
 */
@property (nonatomic, strong) NSTimeZone *startLocaleTimezone;

/**
 The date when the workout was updated
 */
@property (nonatomic, strong) NSDate *updatedDatetime;

/**
 *  Indicates whether timeseries are available for this workout
 */
@property (nonatomic, assign) BOOL hasTimeSeries;

/**
 BOOL indicating if the workout was verified. I.e., the workout can reasonable be presumed to have happened.
 */
@property (nonatomic, assign, getter=isVerified) BOOL verified;

/**
 *  The aggregate data for the workout
 */
@property (nonatomic, strong) UAWorkoutAggregate *aggregate;

/**
 *  The timeseries for the workout
 */
@property (nonatomic, strong) UAWorkoutTimeSeries *timeSeries;

@end

/**
 *  Class that encapsulates the possible time series associated with a without
 */
@interface UAWorkoutTimeSeries : NSObject <NSCoding>

/**
 *  Time series for distance
 */
@property (nonatomic, strong) UATimeSeriesDistance *distanceTimeSeries;

/**
 *  Time series for speed
 */
@property (nonatomic, strong) UATimeSeriesSpeed *speedTimeSeries;

/**
 *  Time series for cadence
 */
@property (nonatomic, strong) UATimeSeriesCadence *cadenceTimeSeries;

/**
 *  Time series for heartrate
 */
@property (nonatomic, strong) UATimeSeriesHeartRate *heartRateTimeSeries;

/**
 *  Time series for power
 */
@property (nonatomic, strong) UATimeSeriesPower *powerTimeSeries;

/**
 *  Time series for position
 */
@property (nonatomic, strong) UATimeSeriesPosition *positionTimeSeries;

/**
 *  Time series for the stop times
 */
@property (nonatomic, strong) UATimeSeriesTimerStop *timerStopTimeSeries;

@end

/**
 *  Class that encapsulates the aggregate data for the workout
 */
@interface UAWorkoutAggregate : NSObject <NSCoding>

/**
 *  The total cumulative distance moved during the workout in meters
 *  [distanceTotal doubleValue]
 */
@property (nonatomic, strong) NSNumber *distanceTotal;

/**
 *  The total cumulative time moving or active during the workout in seconds
 *  [activeTimeTotal unsignedInteger]
 */
@property (nonatomic, strong) NSNumber *activeTimeTotal;

/**
 *  The total cumulative time active and/or inactive during the workout in seconds
 *  [elapsedTimeTotal unsignedInteger]
 */
@property (nonatomic, strong) NSNumber *elapsedTimeTotal;

/**
 *  The highest discrete cadence measurement during the workout in revolutions/minute
 *  [cadenceMax unsignedInteger]
 */
@property (nonatomic, strong) NSNumber *cadenceMax;

/**
 *  The lowest discrete cadence measurement during the workout in revolutions/minute
 *  [cadenceMin unsignedInteger]
 */
@property (nonatomic, strong) NSNumber *cadenceMin;

/**
 *  The cumulative average cadence measured during the workout in revolutions/minute
 *  [cadenceAvg unsignedInteger]
 */
@property (nonatomic, strong) NSNumber *cadenceAvg;

/**
 *  The highest discrete heart rate measurement during the workout in beats/minute
 *  [heartrateMax unsignedInteger]
 */
@property (nonatomic, strong) NSNumber *heartrateMax;

/**
 *  The lowest discrete heart rate measurement during the workout in beats/minute
 *  [heartrateMin unsignedInteger]
 */
@property (nonatomic, strong) NSNumber *heartrateMin;

/**
 *  The cumulative average heart rate measured during the workout in beats/minute
 *  [heartrateAvg unsignedInteger]
 */
@property (nonatomic, strong) NSNumber *heartrateAvg;

/**
 *  The highest discrete speed measurement during the workout in meters/second
 *  [speedMax double]
 */
@property (nonatomic, strong) NSNumber *speedMax;

/**
 *  The lowest discrete speed measurement during the workout in meters/second
 *  [speedMin double]
 */
@property (nonatomic, strong) NSNumber *speedMin;

/**
 *  The cumulative average speed measured during the workout in meters/second
 *  [speedAvg double]
 */
@property (nonatomic, strong) NSNumber *speedAvg;

/**
 *  The highest discrete power measurement during the workout in watts
 *  [powerMax unsignedInteger]
 */
@property (nonatomic, strong) NSNumber *powerMax;

/**
 *  The lowest discrete power measurement during the workout in watts
 *  [powerMin unsignedInteger]
 */
@property (nonatomic, strong) NSNumber *powerMin;

/**
 *  The cumulative average power measured during the workout in watts
 *  [powerAvg unsignedInteger]
 */
@property (nonatomic, strong) NSNumber *powerAvg;

/**
 *  The highest discrete torque measurement during the workout
 *  [torqueMax double]
 */
@property (nonatomic, strong) NSNumber *torqueMax;

/**
 *  The highest discrete torque measurement during the workout
 *  [torqueMin double]
 */
@property (nonatomic, strong) NSNumber *torqueMin;

/**
 *  The cumulative average torque measured during the workout
 *  [torqueAvg double]
 */
@property (nonatomic, strong) NSNumber *torqueAvg;

/**
 *  The elapsed time of the workout (paused and active) in seconds
 *  [stepsTotal unsignedInteger]
 */
@property (nonatomic, strong) NSNumber *stepsTotal;

/**
 *  The calculated willpower for this workout.
 *  [willpower double]
 */
@property (nonatomic, strong) NSNumber *willpower;

/**
 *  The total cumulative metabolic energy burned during the workout in joules.
 *  [metabolicEnergyTotal unsignedInteger]
 */
@property (nonatomic, strong) NSNumber *metabolicEnergyTotal;

@end


