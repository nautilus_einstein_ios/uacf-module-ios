/*
 * UAFriendshipSummary.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UAEntity.h>
#import <UASDK/UAEntityRef.h>

@class UAUserRef;

typedef NS_ENUM(NSUInteger, UAFriendshipStatus)
{
	UAFriendshipStatusUnknown = 0,
	UAFriendshipStatusActive,
	UAFriendshipStatusPending,
};

@interface UAFriendshipSummaryRef : UAEntityRef

@end

@interface UAFriendshipSummary : UAEntity <NSCoding>

/**
 *  Reference to this friendship
 */
@property (nonatomic, strong) UAFriendshipSummaryRef *ref;

/**
 *  Date the friendship was created
 */
@property (nonatomic, strong) NSDate *createdDate;

/**
 *  Message send when creating the friend request
 */
@property (nonatomic, copy) NSString *message;

/**
 *  Current status of the friendship
 */
@property (nonatomic, assign) UAFriendshipStatus friendshipStatus;

/**
 *  Reference to the user which is the friend (active or pending) of the |userRef|
 */
@property (nonatomic, strong) UAUserRef *friendUserReference;

/**
 *  Reference to the current user whom the friendship request was made for
 */
@property (nonatomic, strong) UAUserRef *userRef;

@end
