/*
 * UAPageSettings.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <Foundation/Foundation.h>

@interface UAPageSettings : NSObject

/**
 *  A boolean determining if a featured gallery is available for a page.
 */
@property (nonatomic, assign, readonly) BOOL featuredGalleryEnabled;

/**
 *  A boolean determining if a actigraphy graph is available for a page.
 */
@property (nonatomic, assign, readonly) BOOL graphEnabled;

/**
 *  The text for a CTA.
 */
@property (nonatomic, copy, readonly) NSString *ctaText;

/**
 *  The link for a CTA.
 */
@property (nonatomic, copy, readonly) NSString *ctaLink;

/**
 *  The target for a CTA.
 */
@property (nonatomic, copy, readonly) NSString *ctaTarget;

/**
 *  The name of the template used for a page.
 */
@property (nonatomic, copy, readonly) NSString *templateName;

@end
