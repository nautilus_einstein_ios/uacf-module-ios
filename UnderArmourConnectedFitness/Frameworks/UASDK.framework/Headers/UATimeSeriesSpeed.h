/*
 * UATimeSeriesSpeed.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UATimeSeries.h>

@class UATimeSeriesSpeedPoint;

@interface UATimeSeriesSpeed : UATimeSeries <UATimeSeriesProtocol>

- (void)addPoint:(UATimeSeriesSpeedPoint *)point;

- (UATimeSeriesSpeedPoint *)pointAtIndex:(NSUInteger)index;

@end
