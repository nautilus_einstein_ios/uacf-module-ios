/*
 * UARecordClock.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <Foundation/Foundation.h>

@interface UARecordClock : NSObject

- (NSTimeInterval)timestamp;

- (NSDate *)date;

@end
