/*
 * UARemoteConnectionManager.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UAManager.h>

@class UARemoteConnection, UARemoteConnectionRef, UARemoteConnectionList, UARemoteConnectionListRef;

typedef void (^UARemoteConnectionResponseBlock)(UARemoteConnection *object, NSError *error);
typedef void (^UARemoteConnectionListResponseBlock)(UARemoteConnectionList *object, NSError *error);

@interface UARemoteConnectionManager : UAManager

/**
 *  Fetches a remote connection for the currently authed user.
 *
 *  @param reference The reference to the remote connection
 *  @param response A block called on completion.
 */
- (void)fetchRemoteConnectionWithRef:(UARemoteConnectionRef *)reference
							response:(UARemoteConnectionResponseBlock)response;

/**
 *  Fetches a list of remote connections for the currently authed user.
 *
 *  @param reference The reference to the list of remote connections.
 *  @param response  A block called on completion.
 */
- (void)fetchRemoteConnectionsWithListRef:(UARemoteConnectionListRef *)reference
								 response:(UARemoteConnectionListResponseBlock)response;

/**
 *  Deletes a remote connection
 *
 *  @param reference The reference to the remote connection to delete.
 *  @param response  A block called on completion.
 */
- (void)deleteRemoteConnectionWithRef:(UARemoteConnectionRef *)reference
							 response:(UAResponseBlock)response;

@end
