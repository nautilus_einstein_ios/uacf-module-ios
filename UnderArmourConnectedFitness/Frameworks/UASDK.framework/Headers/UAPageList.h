//
//  UAPageList.h
//  UA
//
//  Created by Chris Green on 6/26/14.
//  Copyright (c) 2014 MapMyFitness Inc. All rights reserved.
//

#import <UASDK/UAEntityList.h>
#import <UASDK/UAPageListRef.h>

@interface UAPageList : UAEntityList <UAEntityListRefInterface>

@property (nonatomic, strong) UAPageListRef *ref;

/**
 *  A reference to the previous collection of entities.
 *  @discussion If no more entities exist, this value will be nil.
 */
@property (nonatomic, strong) UAPageListRef *previousRef;

/**
 *  A reference to the next collection of entities.
 *  @discussion If no more entities exist, this value will be nil.
 */
@property (nonatomic, strong) UAPageListRef *nextRef;

@end
