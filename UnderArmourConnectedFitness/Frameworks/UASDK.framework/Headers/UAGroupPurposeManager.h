/*
 * UAGroupPurposeManager.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAManager.h>

@class UAGroupPurpose, UAGroupPurposeRef, UAGroupPurposeList, UAGroupPurposeListRef;

typedef void (^UAGroupPurposeResponseBlock)(UAGroupPurpose *object, NSError *error);
typedef void (^UAGroupPurposeListResponseBlock)(UAGroupPurposeList *object, NSError *error);

@interface UAGroupPurposeManager : UAManager

/**
 *  Fetches a group purpose object.
 *  @param ref A reference to the desired group purpose. Must not be nil.
 *  @param response A response block to be called when the fetch finishes. Must not be nil.
 */
- (void)fetchGroupPurposeWithRef:(UAGroupPurposeRef *)reference response:(UAGroupPurposeResponseBlock)response;

/**
 *  Fetches a list of group purpose objects.
 *  @param ref A reference to the desired list of group purposes. Must not be nil.
 *  @param response A response block to be called when the fetch finishes. Must not be nil.
 */
- (void)fetchGroupPurposesWithListRef:(UAGroupPurposeListRef *)reference response:(UAGroupPurposeListResponseBlock)response;

@end
