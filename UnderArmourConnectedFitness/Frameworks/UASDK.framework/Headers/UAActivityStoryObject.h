//
//  UAActivityStoryObject.h
//  UA
//
//  Created by Jeremy Zedell on 5/29/14.
//  Copyright (c) 2014 MapMyFitness Inc. All rights reserved.
//

#import <UASDK/UAEntity.h>

typedef NS_ENUM(NSUInteger, UAActivityStoryObjectType)
{
    UAActivityStoryObjectTypeUnknown,
	UAActivityStoryObjectTypeWorkout,
	UAActivityStoryObjectTypeRoute,
	UAActivityStoryObjectTypeUser,
	UAActivityStoryObjectTypeActigraphy,
	UAActivityStoryObjectTypeComment,
	UAActivityStoryObjectTypeLike,
	UAActivityStoryObjectTypeTout,
	UAActivityStoryObjectTypeAd,
    UAActivityStoryObjectTypeStatus,
    UAActivityStoryObjectTypeRepost,
    UAActivityStoryObjectTypeGroup,
    UAActivityStoryObjectTypeGroupLeaderboard
};

@interface UAActivityStoryObject : NSObject <NSCoding>

@property (nonatomic, readonly) UAActivityStoryObjectType type;

@end
