//
//  UAActivityStoryRepost.h
//  UASDK
//
//  Created by Jeremiah Smith on 9/25/14.
//  Copyright (c) 2014 Under Armour. All rights reserved.
//

#import <UASDK/UAActivityStoryActor.h>
#import <UASDK/UAActivityStoryRepostObject.h>

@interface UAActivityStoryRepost : NSObject

@property (nonatomic, copy) NSString *repostID;
@property (nonatomic, strong) UAActivityStoryActor *actor;
@property (nonatomic, strong) UAActivityStoryRepostObject *object;
@property (nonatomic, strong) NSDate *published;

@end
