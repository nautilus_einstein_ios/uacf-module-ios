/*
 * UAEntity.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <Foundation/Foundation.h>
#import <UASDK/UAValidationProtocol.h>

@class UAEntityRef, UAEntitySyncMeta;

@protocol UAEntityInterface <NSObject>

/**
 *  A reference to this object.
 */
@property (nonatomic, strong) UAEntityRef *ref;

@end


@interface UAEntity : NSObject <NSCoding, UAValidationProtocol>

@property (nonatomic, readonly) UAEntitySyncMeta *syncMeta;

@end
