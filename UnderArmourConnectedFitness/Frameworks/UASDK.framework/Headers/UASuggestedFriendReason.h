/*
 * UASuggestedFriendReason.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


typedef NS_ENUM(NSUInteger, UASuggestedFriendSource)
{
	UASuggestedFriendSourceUnknown,
	// Native means you have mutual mapmyfitness friends
	UASuggestedFriendSourceNative,
	// Facebook means you have mutual facebook friends
	UASuggestedFriendSourceFacebook,
};

@interface UASuggestedFriendReason : NSObject

/**
 Platform which the reason is based on
 */
@property (nonatomic, assign) UASuggestedFriendSource source;

/**
 Weight of the reason. The sum of all reasons' weights for a suggested
 friend will equal 1.
 */
@property (nonatomic, strong) NSNumber *weightNumber;
@property (nonatomic, assign) double weight;

+ (UASuggestedFriendReason*)reasonFromDictionary:(NSDictionary*)reason;

@end
