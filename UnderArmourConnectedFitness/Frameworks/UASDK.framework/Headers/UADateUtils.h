/*
 * UADateUtils.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <Foundation/Foundation.h>
#import <UASDK/UADateRange.h>

static NSString *const kFullDateTimeTimezone = @"yyyy-MM-dd'T'HH:mm:ssZZZ";

@interface UADateUtils : NSObject

/**
 * Creates a date based on one of the known IOS8601 date formats used by the MMF API
 * @param dateString a string in one of the known API date formats (kUAAPIDateFormat1 or kUAAPIDateFormat2)
 * @return an NSDate
 */
+ (NSDate *)dateFromAPIDateString:(NSString *)dateString;


+ (NSString *)serverStringFromDate:(NSDate *)date;
/**
 * Creates a string based on the date, timezone, and format passed in.
 * @param date an NSDate object to convert to string.
 * @param timezone an NSTimeZone abbreviation (i.e. UTC, CST, etc.).
 * @param format an NSString that adheres to NSDate formatting syntax.
 * @return a string representation of the date passed in with the specified format and timezone.
 */
+ (NSString *)dateStringWithDate:(NSDate *)date timeZone:(NSString *)timezone format:(NSString *)format;

/**
 * Creates a string based on the date, timezone, and format passed in.
 * @param date an NSDate object to convert to string.
 * @param timezone an NSTimeZone name (i.e. US/Central).
 * @param format an NSString that adheres to NSDate formatting syntax.
 * @return a string representation of the date passed in with the specified format and timezone.
 */
+ (NSString *)dateStringWithDate:(NSDate *)date longTimeZone:(NSString *)timezone format:(NSString *)format;

/**
 * Creates a string based on the date and format passed in.
 * @param date an NSDate object to convert to string.
 * @param format an NSString that adheres to NSDate formatting syntax.
 * @return a string representation of the date passed in with the specified format, using the dateformatter's native timezone.
 */
+ (NSString *)dateStringWithDate:(NSDate *)date format:(NSString *)format;

+ (NSString *)dateStringUTCFormatWithDate:(NSDate *)date;


/**
 * Creates a date based on the string, timezone, and format passed in.
 * @param dateString a date-formatted NSString.
 * @param timezone an NSTimeZone abbreviation (i.e. UTC, CST, etc.).
 * @param format an NSString that adheres to NSDate formatting syntax.
 * @return a date representation of the string passed in with the specified format and timezone.
 */
+ (NSDate *)dateFromDateString:(NSString *)dateString timeZone:(NSString *)timezone format:(NSString *)format;

/**
 * Creates a date based on the string, timezone, and format passed in.
 * @param dateString a date-formatted NSString.
 * @param timezone an NSTimeZone abbreviation (i.e. US/Central).
 * @param format an NSString that adheres to NSDate formatting syntax.
 * @return a date representation of the string passed in with the specified format and timezone.
 */
+ (NSDate *)dateFromDateString:(NSString *)dateString longTimeZone:(NSString *)timezone format:(NSString *)format;

+ (NSDate*)dateFromUTCFormattedString:(NSString*)dateString;

+ (NSDate*)dateForBeginningOfDayForDate:(NSDate*)date longTimeZone:(NSString*)timezone;
+ (NSDate*)dateForEndOfDayForDate:(NSDate*)date longTimeZone:(NSString*)timezone;
+ (NSDate*)dateByAdjustingDate:(NSDate*)date backToMultipleOfSecondsSinceBeginningOfDay:(NSTimeInterval)seconds longTimeZone:(NSString*)timezone;

/**
 * Indicates if the specified date is within the specified range.
 * @param date an NSDate to check if it is in the provided range.
 * @param range an UARange where the date will be checked for being within it.
 * @return a flag indicating if the date is within the range.
 */
+ (BOOL)isDate:(NSDate *)date withinRange:(UADateRange *)range;

/**
 * Converts the specified date between the specified timezones.
 * @param date an NSDate to be converted between timezones.
 * @param sourceTimeZone an NSTimeZone that represents the timezone the date is in.
 * @param destinationTimeZone an NSTimeZone that represents the timezone the date is to be converted to.
 * @return the provided date converted between the specified timezones.
 */
+ (NSDate *)dateByConvertingDate:(NSDate *)date fromTimeZone:(NSTimeZone *)sourceTimeZone toTimeZone:(NSTimeZone *)destinationTimeZone;

/**
 * Indicates if a date lies in the same day as an another date.
 * @param date the first NSDate to use in the comparison.
 * @param otherDate the other NSDate to use in the comparison.
 * @return flag indicating if both dates represent the same day.
 */
+ (BOOL)isDate:(NSDate *)date sameDayAsDate:(NSDate *)otherDate;

@end
