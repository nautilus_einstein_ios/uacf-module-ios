/*
 * UARemoteConnectionList.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAEntityList.h>
#import <UASDK/UAEntityListRef.h>

@interface UARemoteConnectionListRef : UAEntityListRef

+ (UARemoteConnectionListRef *)remoteConnectionListRef;

@end

@interface UARemoteConnectionList : UAEntityList <UAEntityListRefInterface>

/**
 *  Reference to this object.
 */
@property (nonatomic, strong) UARemoteConnectionListRef *ref;

/**
 *  A reference to the previous collection of entities.
 *  @discussion If no more entities exist, this value will be nil.
 */
@property (nonatomic, strong) UARemoteConnectionListRef *previousRef;

/**
 *  A reference to the next collection of entities.
 *  @discussion If no more entities exist, this value will be nil.
 */
@property (nonatomic, strong) UARemoteConnectionListRef *nextRef;

@end
