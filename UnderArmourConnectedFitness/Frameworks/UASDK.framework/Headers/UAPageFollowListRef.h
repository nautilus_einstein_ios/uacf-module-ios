/*
 * UAPageFollowListRef.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAEntityListRef.h>

@class UAUserRef, UAPageRef;

@interface UAPageFollowListRef : UAEntityListRef

/**
 *  Generates a collection reference for a given user reference.
 *
 *  @param userRef A reference to a user.
 *
 *  @return an UAPageFollowListRef object pertaining to a user.
 */
+ (UAPageFollowListRef *)pageFollowListRefForUserReference:(UAUserRef *)userRef;

/**
 *  Generates a collection reference for a given page reference.
 *
 *  @param pageReference A reference to a page.
 *
 *  @return an UAPageFollowListRef object pertaining to a page.
 */
+ (UAPageFollowListRef *)pageFollowListRefForPageReference:(UAPageRef *)pageReference;

@end
