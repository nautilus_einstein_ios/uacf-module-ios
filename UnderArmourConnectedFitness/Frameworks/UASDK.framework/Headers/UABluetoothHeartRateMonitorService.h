/*
 * UABluetoothHeartRateMonitorService.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <Foundation/Foundation.h>
#import <UASDK/UABluetoothServiceInterface.h>
#import <UASDK/UABluetoothProtocolConstants.h>

@protocol UABluetoothHeartRateMonitorServiceDelegate;

@interface UABluetoothHeartRateMonitorService : NSObject <UABluetoothServiceInterface>

@property (nonatomic, weak) id<UABluetoothHeartRateMonitorServiceDelegate> delegate;

@end

@protocol UABluetoothHeartRateMonitorServiceDelegate <UABluetoothServiceDelegate>

/**
 *  Called when the device has sent a new heart rate measurement.
 *  @param service The service object which received the heart rate measurement.
 *  @param heartRate The measured heart rate in beats per minute (double).
 *  @param energyExpended The measured energy expended in joules (double). May be nil if the device does not support measuring energy expended.
 *  @param rrIntervals A list of measured intervals between R-waves in seconds (NSTimeInterval). May be nil or empty if the device does not support measuring R-R intervals.
 *  @param contactStatus The status of the skin contacts on the device.
 */
- (void)service:(UABluetoothHeartRateMonitorService *)service didUpdateHeartRateMeasurement:(NSNumber *)heartRate energyExpended:(NSNumber *)energyExpended rrIntervals:(NSArray *)rrIntervals sensorContactStatus:(UABluetoothHeartRateMonitorSensorContactStatus)contactStatus;

@end
