/*
 * UARecordManager.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAManager.h>

/**
 *  This delegate informs the state of a recorder, including the create,
 *  destroy, and recovered states. 
 *
 *  @discussion The recovery state may be useful for instances where a session
 *  was currently in progress, but was lost due to an application termination.
 */
@protocol UARecordManagerDelegate <NSObject>

@optional
- (void)recorderCreated:(NSString *)sessionName;
- (void)recorderDestroyed:(NSString *)sessionName;
- (void)recorderDidRecover:(NSString *)sessionName;

@end

@class UARecorderConfiguration, UARecorder, UAWorkout;

/**
 *  This manager is responsible for creating and managing recorders
 *  that are used to record data points in real time.
 */
@interface UARecordManager : UAManager

/**
 *  A mapping of recorders, keyed off by its name.
 */
@property (nonatomic, copy, readonly) NSDictionary *recorders;

/**
 *  A delegate that responds to UARecordManagerDelegate methods (if implemented).
 */
@property (nonatomic, weak) id<UARecordManagerDelegate> delegate;

/**
 *  Creates a recorder with a given configuration.
 *
 *  @param recorderConfiguration A configuration used to create a new recorder.
 *  @param completionHandler          A block that returns a new session instance, or an error if the session failed to be created.
 */
- (void)createRecorderWithConfiguration:(UARecorderConfiguration *)recorderConfiguration completionHandler:(void (^)(UARecorder *session, NSError *error))completionHandler;

/**
 *  Retrieves a recorder based on its recorder name.
 *
 *  @param name The name of the session.
 *
 *  @return A valid recorder, or nil if no session exists for that name.
 */
- (UARecorder *)getRecorderWithName:(NSString *)name;

@end
