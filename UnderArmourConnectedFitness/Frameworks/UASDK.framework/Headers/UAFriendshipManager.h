/*
 * UAFriendshipManager.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UAManager.h>

@class UAFriendshipSummary, UAUserRef, UASuggestedFriend, UAFriendshipSummaryList, UAFriendshipSummaryRef,
       UASuggestedFriendListRef, UASuggestedFriendList, UAFriendshipListRef;

typedef void (^UAFriendshipResponseBlock)(UAFriendshipSummary *object, NSError *error);
typedef void (^UAFriendshipSummaryListResponseBlock)(UAFriendshipSummaryList *object, NSError *error);
typedef void (^UASuggestedFriendsListResponseBlock)(UASuggestedFriendList *object, NSError *error);

@interface UAFriendshipManager : UAManager

/**
 *  Gets a list of friends for a user with a collection reference
 *
 *  @param reference friendship collection reference
 *  @param response A block called on completion.
 */
- (void)fetchFriendsWithListRef:(UAFriendshipListRef *)reference
                       response:(UAFriendshipSummaryListResponseBlock)response;

/**
 *  Gets a list of suggested friends for users
 *
 *  @param reference collection reference for suggested friends
 *  @param response A block called on completion.
 */
- (void)fetchSuggestedFriendsWithListRef:(UASuggestedFriendListRef *)reference
                                response:(UASuggestedFriendsListResponseBlock)response;

/**
 *  Sends a friend request to a user
 *
 *  @param reference user to create a friendship with
 *  @param response A block called on completion.
 */
- (void)sendFriendRequestToUserWithRef:(UAUserRef *)reference
                              response:(UAFriendshipResponseBlock)response;

/**
 *  Sends multiple friend requests from the authenticated user
 *
 *  @param references An array of {@link UAUserRef} to create a friendship with
 *  @param response A block called on completion.
 */
- (void)sendFriendRequestsToUserWithRefs:(NSArray *)references
								response:(UAFriendshipSummaryListResponseBlock)response;
/**
 *  Gets the friendship status between two users. There are three possible friendship states
 *  for a successful response:
 *  
 *  active
 *  pending
 *  none -- In this case success will still be called with a nil summary object
 *
 *  @param reference user to create a friendship with
 *  @param response A block called on completion.
 */
- (void)fetchFriendStatusForUserWithRef:(UAUserRef *)reference
                               response:(UAFriendshipResponseBlock)response;

/**
 *  Approves a pending friend request
 *
 *  @param summary friendship summary to approve
 *  @param response A block called on completion.
 */
- (void)approvePendingFriendship:(UAFriendshipSummary *)summary
                        response:(UAFriendshipResponseBlock)response;

/**
 *  Deletes a friend or rejects a pending friend request
 *
 *  @param reference UAFriendshipSummaryRef of the friendship to delete or reject
 *  @param response A block called on completion.
 */
- (void)deleteOrDenyFriendshipWithRef:(UAFriendshipSummaryRef *)reference
                             response:(UAResponseBlock)response;

@end
