/*
 * UATimeSeriesCadence.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UATimeSeries.h>

@class UATimeSeriesCadencePoint;

@interface UATimeSeriesCadence : UATimeSeries <UATimeSeriesProtocol>

- (void)addPoint:(UATimeSeriesCadencePoint *)point;

- (UATimeSeriesCadencePoint *)pointAtIndex:(NSUInteger)index;

@end
