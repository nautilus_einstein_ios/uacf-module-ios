/*
 * UATimeSeriesCadencePoint.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UATimeSeriesPoint.h>

@interface UATimeSeriesCadencePoint : UATimeSeriesPoint

+ (instancetype)pointAtTime:(NSTimeInterval)timestamp withCadence:(double)cadence;

/**
 *  Cadence in revolutions per minute
 */
@property (nonatomic, readonly) double cadence;

@end
