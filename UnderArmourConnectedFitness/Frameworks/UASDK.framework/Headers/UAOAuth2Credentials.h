/*
 * UAOAuth2Credentials.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <Foundation/Foundation.h>

@interface UAOAuth2Credentials : NSObject <NSCoding>

@property (nonatomic, strong) NSString *accessToken;
@property (nonatomic, strong) NSString *expiresInSeconds;
@property (nonatomic, strong) NSString *refreshToken;
@property (nonatomic, strong) NSString *scope;

- (bool)isTokenExpired;

@end
