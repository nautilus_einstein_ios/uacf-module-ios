/*
 * UATimeSeriesDistancePoint.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UATimeSeriesPoint.h>

@interface UATimeSeriesDistancePoint : UATimeSeriesPoint

+ (instancetype)pointAtTime:(NSTimeInterval)timestamp withDistance:(double)distance;

/**
 *  Distance in meters
 */
@property (nonatomic, readonly) double distance;

@end
