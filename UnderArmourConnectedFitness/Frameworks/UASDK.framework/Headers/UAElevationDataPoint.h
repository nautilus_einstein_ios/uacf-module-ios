//
//  UAElevationDataPoint.h
//  UASDK
//
//  Created by Morrow, Andrew on 11/20/14.
//  Copyright (c) 2014 Under Armour. All rights reserved.
//

#import <UASDK/UADataPoint.h>

@interface UAElevationDataPoint : UADataPoint

/**
 *  Elevation (double, meters).
 */
@property (nonatomic, readonly) NSNumber *elevation;

/**
 *  Accuracy (double, meters).
 */
@property (nonatomic, readonly) NSNumber *verticalAccuracy;

/**
 *  Instantiates a elevation data point.
 *  @param elevation The elevation in meters. Must not be nil.
 *  @param verticalAccuracy The vertical accuracy in meters. Must not be nil.
 *  @return A newly instantiated data point.
 */
- (instancetype)initWithElevation:(NSNumber *)elevation withVerticalAccuracy:(NSNumber *)verticalAccuracy NS_DESIGNATED_INITIALIZER;

@end
