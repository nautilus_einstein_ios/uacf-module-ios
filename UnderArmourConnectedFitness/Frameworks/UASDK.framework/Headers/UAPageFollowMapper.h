/*
 * UAPageFollowMapper.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <Foundation/Foundation.h>

@class UAPageFollow, UAPageFollowList;

@interface UAPageFollowMapper : NSObject

+ (UAPageFollow *)pageFollowForResponse:(NSDictionary *)response;
+ (UAPageFollowList *)pageFollowsForResponse:(NSDictionary *)response;

+ (NSDictionary *)dictionaryForPageFollow:(UAPageFollow *)pageFollow;
+ (NSDictionary *)dictionaryForPageFollows:(NSArray *)pageFollows;

@end
