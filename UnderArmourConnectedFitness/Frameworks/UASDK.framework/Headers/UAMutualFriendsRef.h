/*
 * UAMutualFriendsRef.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UAEntityRef.h>

@interface UAMutualFriendsRef : UAEntityRef

@property (nonatomic, strong) NSNumber *mutualFriendsCountNumber;
@property (nonatomic, assign) NSInteger mutualFriendsCount;

@end
