/*
 * UA.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


@import Foundation;

#import <UASDK/UAEnumerations.h>

@class UAUserManager, UAUserStatsManager, UARouteManager, UAWorkoutManager, UAActigraphyManager, UAFriendshipManager, 
       UASleepDataManager, UABodyMassManager, UAActivityTypeManager, UAActivityTimeSeriesManager, UAUser, UAUserRef, UAOAuthSession,
	   UAAggregateManager, UAGroupLeaderboardManager, UARecordManager, UARemoteConnectionManager, UARemoteConnectionTypeManager, UAGroupManager,
       UAGroupInviteManager, UAGroupPurposeManager, UAGroupUserManager, UAModerationManager, UAPageManager, UAActivityStoryManager;

@class UAHeartRateZonesManager, UAHeartRateZoneCalculationManager;

/**
 *   @class UA
 *   
 *   The main class to interface with the UASDK.
 *
 */
@interface UA : NSObject

/**
 *  The manager to interface with user objects.
 */
@property (nonatomic, readonly) UAUserManager *userManager;

/**
 *  The manager to interface with user stats objects.
 */
@property (nonatomic, readonly) UAUserStatsManager *userStatsManager;

/**
 *  The manager to interface with route objects.
 */
@property (nonatomic, readonly) UARouteManager *routeManager;

/**
 *  The manager to interface with workout objects.
 */
@property (nonatomic, readonly) UAWorkoutManager *workoutManager;

/**
 *    The manager to interface with activity types objects.
 */
@property (nonatomic, readonly) UAActivityTypeManager *activityTypeManager;

/**
 *    The manager to interface with the actigraphy
 */
@property (nonatomic, readonly) UAActigraphyManager *actigraphyManager;

/**
 *    The manager to interface with the friendship objects
 */
@property (nonatomic, readonly) UAFriendshipManager *friendshipManager;

/**
 *    The manager to interface with the sleep objects
 */
@property (nonatomic, readonly) UASleepDataManager *sleepDataManager;

/**
 *    The manager to interface with the bodymass objects
 */
@property (nonatomic, readonly) UABodyMassManager *bodyMassManager;

/**
 *    The manager to interface with the time series objects.
 */
@property (nonatomic, readonly) UAActivityTimeSeriesManager *activityManager;

/**
 *    The manager to interface with the aggregate objects.
 */
@property (nonatomic, readonly) UAAggregateManager *aggregateManager;

/**
 *    The manager to interface with the leaderboard objects.
 */
@property (nonatomic, readonly) UAGroupLeaderboardManager *groupLeaderboardManager;

/**
 *    The manager to interface with recording.
 */
@property (nonatomic, readonly) UARecordManager *recordManager;

/**
 *  The manager for remote connections.
 */
@property (nonatomic, readonly) UARemoteConnectionManager *remoteConnectionManager;

/**
 *  The manager for remote connection types
 */
@property (nonatomic, readonly) UARemoteConnectionTypeManager *remoteConnectionTypeManager;

/**
 * The manager for heart rate zones.
 */
@property (nonatomic, readonly) UAHeartRateZonesManager *heartRateZonesManager;

/**
 *  The manager for groups
 */
@property (nonatomic, readonly) UAGroupManager *groupManager;

/**
 *  The manager for group invites
 */
@property (nonatomic, readonly) UAGroupInviteManager *groupInviteManager;

/**
 *  The manager for group purpose
 */
@property (nonatomic, readonly) UAGroupPurposeManager *groupPurposeManager;

/**
 *  The manager for group users
 */
@property (nonatomic, readonly) UAGroupUserManager *groupUserManager;

/**
 *  The manager to moderate (flag, unflag) resources
 */
@property (nonatomic, readonly) UAModerationManager *moderationManager;

/**
 *  The manager for pages
 */
@property (nonatomic, readonly) UAPageManager *pageManager;

/**
 *  The manager for activity stories
 */
@property (nonatomic, readonly) UAActivityStoryManager *activityStoryManager;

/**
 *   YES if there are current user credentials present (ie a user is logged in).
 */
@property (nonatomic, readonly, getter=isAuthenticated) BOOL authenticated;

/**
 *    Reference to the authenticated user. If no user is authenticated, this will return nil.
 */
@property (nonatomic, readonly) UAUserRef* authenticatedUserRef;

/**
 *    This is an optional block. This will be invoked whenever the current authenticated becomes invalid. For example,
 *    if the oauth2 token for the current user expires and refreshing the token fails, this block will be invoked which
 *    should prompt the user for their credentials. 
 */
@property (nonatomic, copy) UAAuthenticationRequiredBlock userAuthBlock;

/**
 *  Initializes the UA library with an application consumer and secret.
 *
 *  @param appConsumer An application consumer key obtained from the MapMyFitness developer portal.
 *  @param appSecret   An application secret obtained from the MapMyFitness developer portal.
 *
 *  @return The shared instance of the UA.
 */
+ (UA *)initializeWithApplicationConsumer:(NSString *)appConsumer
						applicationSecret:(NSString *)appSecret;

/**
 *  A singleton for the UA framework.
 *
 *  The UA must be initialized using one of the initalization methods below
 *  for the shared instance to be of use. If the SDK has not been initialized, then
 *  this method will return nil.
 *
 *  @return The shared instance of the UASDK interface.
 */
+ (instancetype)sharedInstance;

/**
 *  This will create the URL needed to authorize a user. The URL should be loaded within a webview.
 *
 *  IMPORTANT: It is not recommended switching to Safari to open this URL as that may violate the App Store
 *             guidelines and cause the app to be rejected by Apple.
 *
 *  When the user authorizes this application, this app will be opened with the following
 *  scheme: uasdk<client id>://
 *  
 *  It is expected that inside the app delegates application:openURL:sourceApplication:annotation: 
 *  implemtation that the url will be passed into the {@link handleUrl:response}.
 *
 *  @return The URL to load to show in a webview.
 */
- (NSURL *)requestUserAuthorizationUrl;

/**
 *  This will process a url that launched the application which was intended for the UASDK. 
 *  The URL will be processed to try and authenticate a user.
 *
 *  @param url           The URL passed in from the app delegate.
 *  @param responseBlock The block that will be called if the url contained the parameters needed for 
 *                       authenticating a user.
 *
 *  @return YES if this URL was intended (handled) by the UASDK. Otherwise NO. Note that if the URL was 
 *          not handled by the UASDK, then the responseBlock will not be executed.
 */
- (BOOL)handleUrl:(NSURL *)url
         response:(UAAuthResponseBlock)response;

/**
 *  Clears the credentials of the autneticated user
 *
 *  @param responseBlock The block that will be called when the logout is complete.
 */
- (void)logout:(UAResponseBlock)response;

@end

