/*
 * UAPageRef.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAEntityRef.h>

@class UAUserRef;

@interface UAPageRef : UAEntityRef

/**
 *  Creates a page reference from a user reference
 */
+ (UAPageRef*)pageReferenceFromUserReference:(UAUserRef*)userRef;

/**
 *  Creates a page reference from a pageID
 */
+ (UAPageRef*)pageReferenceWithPageID:(NSString*)pageID;

@end
