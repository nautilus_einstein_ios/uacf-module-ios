//
//  UAEntityList.h
//  UA
//
//  Created by Corey Roberts on 5/6/14.
//  Copyright (c) 2014 MapMyFitness Inc. All rights reserved.
//

@import Foundation;

#import <UASDK/UAValidationProtocol.h>

@class UAEntityListRef;

@protocol UAEntityListRefInterface <NSObject>

/**
 *  A reference to the previous collection of entities.
 *  @discussion If no collection has previously been visited, this value will be nil.
 */
@property (nonatomic, strong) UAEntityListRef *previousRef;

/**
 *  A reference to the next collection of entities.
 *  @discussion If no more entities exist, this value will be nil.
 */
@property (nonatomic, strong) UAEntityListRef *nextRef;

/**
 *  A reference to this objct
 */
@property (nonatomic, strong) UAEntityListRef *ref;

@end

/**
 *  An object that represents a collection of entities.
 */
@interface UAEntityList : NSObject <NSCoding, UAValidationProtocol>

/**
 *  The total number of items available,
 *  may be greater than the items returned in this particular collection.
 */
@property (nonatomic, strong) NSNumber *totalCount;

@property (nonatomic, strong) NSArray *objects;

@property (nonatomic, readonly) NSArray *objectReferences;

@end
