//
//  UAActivityStoryRepostList.h
//  UASDK
//
//  Created by Jeremiah Smith on 9/25/14.
//  Copyright (c) 2014 Under Armour. All rights reserved.
//

#import <UASDK/UAEntityList.h>
#import <UASDK/UAActivityStoryRepost.h>

@interface UAActivityStoryRepostList : UAEntityList

/**
 @return YES if the currently authenticated user has reposted the associated ActivityStory.
 */
@property (nonatomic, assign) BOOL reposted;

@property (nonatomic, strong) NSArray /* UAActivityStoryRepost */ *reposts;

@end
