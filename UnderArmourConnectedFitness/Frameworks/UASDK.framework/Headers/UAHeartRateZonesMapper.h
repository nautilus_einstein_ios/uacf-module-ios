/*
 * UAHeartRateZonesMapper.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <Foundation/Foundation.h>

@class UAHeartRateZones;
@class UAHeartRateZonesList;

@interface UAHeartRateZonesMapper : NSObject

+ (UAHeartRateZones *)heartRateZonesFromResponse:(NSDictionary *)response;
+ (UAHeartRateZonesList *)heartRateZonesListFromResponse:(NSDictionary *)response;

+ (NSDictionary *)dictionaryFromHeartRateZones:(UAHeartRateZones *)zones;

@end
