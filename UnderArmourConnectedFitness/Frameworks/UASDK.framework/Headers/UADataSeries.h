//
//  UADataSeries.h
//  UASDK
//
//  Created by Morrow, Andrew on 11/19/14.
//  Copyright (c) 2014 Under Armour. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UADataPoint, UADataType;

@interface UADataSeries : NSObject <NSCoding>

@property (readonly) UADataType *dataType;
@property (readonly) NSArray *dataPoints;

- (instancetype)initWithDataType:(UADataType *)type;
- (instancetype)initWithDataType:(UADataType *)type dataPoints:(NSArray *)points;

- (void)addDataPoint:(UADataPoint *)dataPoint;

- (instancetype)init __attribute__((unavailable("init not available")));

+ (instancetype)new __attribute__((unavailable("new not available")));

@end
