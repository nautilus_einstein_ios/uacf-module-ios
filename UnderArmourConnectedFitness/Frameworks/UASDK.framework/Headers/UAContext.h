/*
 * UAContext.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


@import Foundation;

@interface UAContext : NSObject

/**
 *  @internal
 *  Creates a context used with the production server.
 *
 *  @return A context with paths for the production server.
 */
+ (instancetype)production;

/**
 *  @internal
 *  Path used with the OAuth network calls
 */
@property (nonatomic, readonly) NSString *oAuthUrl;

/**
 *  @internal
 *  Path used with the OAuth2 network calls
 */
@property (nonatomic, readonly) NSString *oAuth2Url;

/**
 *  @internal
 *  Path used with the 3.1 network calls
 */
@property (nonatomic, readonly) NSString *apiUrl;

/**
 *  @internal
 *  Base server path
 */
@property (nonatomic, readonly) NSString *wwwUrl;

@end
