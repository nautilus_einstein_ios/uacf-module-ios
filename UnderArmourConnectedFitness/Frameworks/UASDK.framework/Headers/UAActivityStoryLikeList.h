//
//  UAActivityStoryLikesList.h
//  UA
//
//  Created by Jeremy Zedell on 5/29/14.
//  Copyright (c) 2014 MapMyFitness Inc. All rights reserved.
//

#import <UASDK/UAEntityList.h>
#import <UASDK/UAActivityStoryRef.h>

@interface UAActivityStoryLikeList : UAEntityList

@property (nonatomic, strong) NSArray *likes;

/**
 @return YES if the currently authenticated user has liked the associated ActivityStory.
 */
@property (nonatomic, assign)  BOOL replied;

/**
 @discussion replyReference is the story reference to the current user's like.
 This will be nil if the current user has not liked the story to which the
 UAActivityStoryLikeList belongs. Pass this reference into
 UAActivityStoryManager's deleteStory:success:failure: method to "unlike"
 the story.
 */
@property (nonatomic, strong) UAActivityStoryRef *replyReference;

@end
