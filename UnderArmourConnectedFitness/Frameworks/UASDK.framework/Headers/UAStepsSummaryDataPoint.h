//
//  UAStepsSummaryDataPoint.h
//  UASDK
//
//  Created by Morrow, Andrew on 11/19/14.
//  Copyright (c) 2014 Under Armour. All rights reserved.
//

#import <UASDK/UADataPoint.h>

@interface UAStepsSummaryDataPoint : UADataPoint

/**
 *  Sum of steps (NSInteger, unitless).
 */
@property (nonatomic, readonly) NSNumber *stepsSum;

@end
