/*
 * UABluetoothServiceTypes.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#ifndef UASDK_UABluetoothServiceTypes_h
#define UASDK_UABluetoothServiceTypes_h

typedef NS_OPTIONS(NSUInteger, UABluetoothServiceType) {
	UABluetoothServiceTypeNone						= 0,
	UABluetoothServiceTypeArmour39					= 1 << 0,
	UABluetoothServiceTypeCyclingPower				= 1 << 1,
	UABluetoothServiceTypeCyclingSpeedAndCadence	= 1 << 2,
	UABluetoothServiceTypeHeartRate					= 1 << 3,
	UABluetoothServiceTypeRunningSpeedAndCadence	= 1 << 4
};

#endif
