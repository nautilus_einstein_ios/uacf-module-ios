/*
 * UATimeSeriesHeartRatePoint.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UATimeSeriesPoint.h>

@interface UATimeSeriesHeartRatePoint : UATimeSeriesPoint

+ (instancetype)pointAtTime:(NSTimeInterval)timestamp withHeartRate:(NSUInteger)heartRate;

/**
 *  Speed in meters per second
 */
@property (nonatomic, readonly) NSUInteger heartRate;

@end
