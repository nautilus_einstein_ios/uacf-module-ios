//
//  UAHeightDataPoint.h
//  UASDK
//
//  Created by Morrow, Andrew on 11/20/14.
//  Copyright (c) 2014 Under Armour. All rights reserved.
//

#import <UASDK/UADataPoint.h>

@interface UAHeightDataPoint : UADataPoint

/**
 *  Height (double, meters).
 */
@property (nonatomic, readonly) NSNumber *height;

/**
 *  Instantiates a height data point.
 *  @param height The height in meters. Must not be nil.
 *  @return A newly instantiated data point.
 */
- (instancetype)initWithHeight:(NSNumber *)height NS_DESIGNATED_INITIALIZER;

@end
