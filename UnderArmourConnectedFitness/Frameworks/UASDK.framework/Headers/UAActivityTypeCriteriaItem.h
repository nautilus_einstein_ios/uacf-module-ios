/*
 * UAActivityTypeCriteriaItem.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UACriteriaItem.h>

@interface UAActivityTypeCriteriaItem : UACriteriaItem

@property (nonatomic, copy) NSNumber *value;

@end
