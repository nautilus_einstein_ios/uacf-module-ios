/*
 * UAUserStatsReport.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UAEntity.h>
#import <UASDK/UAEntityRef.h>

@class UAUserRef, UADateRange;

@interface UAUserStatsReportRef : UAEntityRef

+ (UAUserStatsReportRef *)userStatsReportRefForUserRef:(UAUserRef *)userRef
											 dateRange:(UADateRange*)dateRange
									 aggregationPeriod:(UAAggregationPeriodType)periodType
								   includeSummaryStats:(BOOL)includeSummaryStats;

@end

@interface UAUserStatsReport : UAEntity <UAEntityInterface>

@property (nonatomic, strong) UAUserStatsReportRef *ref;

/**
 * Array of UserStats aggregated across a date range by activity type
 */
@property (nonatomic, strong) NSArray /* UAUserStats */ *stats;

/**
 * Array of UserStats aggregated across a date range without regard to any activity type.
 */
@property (nonatomic, strong) NSArray /* UAUserStats */ *summary;

@end
