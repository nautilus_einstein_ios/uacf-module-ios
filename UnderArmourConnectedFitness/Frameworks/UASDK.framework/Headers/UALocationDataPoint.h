//
//  UALocationDataPoint.h
//  UASDK
//
//  Created by Morrow, Andrew on 11/19/14.
//  Copyright (c) 2014 Under Armour. All rights reserved.
//

#import <UASDK/UADataPoint.h>

@interface UALocationDataPoint : UADataPoint

/**
 *  Latitude (double, degrees).
 */
@property (nonatomic, readonly) NSNumber *latitude;

/**
 *  Longitude (double, degrees).
 */
@property (nonatomic, readonly) NSNumber *longitude;

/**
 *  Horizontal accuracy (double, meters).
 */
@property (nonatomic, readonly) NSNumber *horizontalAccuracy;

/**
 *  Instantiates a location data point.
 *  @param latitude The latitude in meters. Must not be nil.
 *  @param longitude The longitude in meters. Must not be nil.
 *  @param horizontalAccuracy The horizontal accuracy in meters. Must not be nil.
 *  @return A newly instantiated data point.
 */
- (instancetype)initWithLatitude:(NSNumber *)latitude withLongitude:(NSNumber *)longitude horizontalAccuracy:(NSNumber *)horizontalAccuracy NS_DESIGNATED_INITIALIZER;

@end
