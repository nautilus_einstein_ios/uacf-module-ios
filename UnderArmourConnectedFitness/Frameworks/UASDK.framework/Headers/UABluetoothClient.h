/*
 * UABluetoothClient.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <Foundation/Foundation.h>
#import "UABluetoothServiceTypes.h"
#import "UABluetoothProtocolConstants.h"

typedef NS_ENUM(NSUInteger, UABluetoothClientState) {
	UABluetoothClientStateUnknown = 0,
	UABluetoothClientStateHardwareUnavailable,
	UABluetoothClientStateDisconnected,
	UABluetoothClientStateConnecting,
	UABluetoothClientStateConnected,
};

extern NSString *const UABluetoothClientErrorDomain;

typedef NS_ENUM(NSInteger, UABluetoothClientError) {
	UABluetoothClientErrorUnknown = 1,
	UABluetoothClientErrorInternalError,
	UABluetoothClientErrorBluetoothHardwareUnsupported,
	UABluetoothClientErrorBluetoothHardwareNotAuthorized,
	UABluetoothClientErrorBluetoothHardwarePoweredOff,
	UABluetoothClientErrorBluetoothHardwareNotReady,
	UABluetoothClientErrorInvalidDeviceID,
	UABluetoothClientErrorConnectionFailed,
};

@protocol UABluetoothClientDelegate;

/**
 *  @class UABluetoothClient
 *
 *  A UABluetoothClient instance handles connecting and reconnecting to a specific BTLE device, configuring the device, and parsing data samples sent by the device.
 *  Each instance is configured with a specific BTLE device and list of services (see {@link initWithDeviceUUID:services:}). It will connect to the device when
 *  {@link startSession} is called and will attempt to maintain (or reestablish) a connection to the device until {@link stopSession} is called. Parsed data samples
 *  are passed on via the {@link UABluetoothClientDelegate} interface. All methods in this class are thread-safe unless otherwise noted.
 */
@interface UABluetoothClient : NSObject

/**
 *  The delegate for the object.
 *  @discussion All delegate callbacks happen on a background thread.
 */
@property (atomic, weak) id<UABluetoothClientDelegate> delegate;

/**
 *  The current state of the object.
 *  @discussion This value may safely be read from any thread.
 */
@property (atomic, readonly) UABluetoothClientState state;

/**
 *  Designated initializer.
 *  @param deviceUUID The UUID of the device the instance will connect to, obtained from a CBPeripheral object.
 *  @param services Bit-mask indicating which services the device supports and which the UABluetoothClient will use.
 *  @return Initialized UABluetoothClient object
 */
- (instancetype)initWithDeviceUUID:(NSUUID *)deviceUUID services:(UABluetoothServiceType)services;

/**
 *  Begins a recorder by connecting to the device and starting to parse data samples. Attempts to (re)connect to the device will continue until the session
 *  is terminated by calling {@link stopSession}.
 *  @see stopSession
 */
- (void)startSession;

/**
 *  Ends a recorder by disconnecting from the device. No further delegate callbacks will occur after this method is called. A new session cannot be started
 *  once this method has been called. Instead, destroy the object and initialize a new one.
 *  @see startSession
 */
- (void)stopSession;

/**
 *  Begins a new recording segment.
 *  @discussion The first time this method is called, it resets/initializes accumulator values on the connected device. Subsequent calls will not reset
 *  accumulated values.
 *  @see stopSegment
 */
- (void)startSegment;

/**
 *  Ends a recording segment. A new segment can be started after this method has been called by calling {@link startSegment}.
 *  @discussion Acumulated values are not reset.
 *  @see startSegment
 */
- (void)stopSegment;

@end

@protocol UABluetoothClientDelegate <NSObject>

@optional

/**
 *  Called when the client has changed state.
 *  @param client The client which changed state.
 *  @param error Error which caused the client to change state. (May be nil.)
 *  @discussion This method is called on a background thread.
 */
- (void)bluetoothClient:(UABluetoothClient *)client didUpdateStateWithError:(NSError *)error;

/**
 *  Called when the device has sent a new heart rate measurement.
 *  @param client The client object which received the heart rate measurement.
 *  @param heartRate The measured heart rate in beats per minute (double).
 *  @param energyExpended The measured energy expended in joules (double). May be nil if the device does not support measuring energy expended.
 *  @param rrIntervals A list of measured intervals between R-waves in seconds (NSTimeInterval). May be nil or empty if the device does not support measuring R-R intervals.
 *  @param contactStatus The status of the skin contacts on the device.
 *  @discussion This method is called on a background thread.
 */
- (void)bluetoothClient:(UABluetoothClient *)client didUpdateHeartRateMeasurement:(NSNumber *)heartRate energyExpended:(NSNumber *)energyExpended rrIntervals:(NSArray *)rrIntervals sensorContactStatus:(UABluetoothHeartRateMonitorSensorContactStatus)contactStatus;

/**
 *  Called when the A39 module updates workout data (every five seconds).
 *  Values are reset on the device when {@link startSegment} is called.
 *
 *  @param client The client object which received the measurement.
 *  @param stepCount Number of steps since the workout began.
 *  @param caloriesExpended Number of calories expended since the workout began.
 *  @param willPower Calculated WILLPOWER since the workout began.
 *  @param measuredPosture Measured posture.
 *  @param strideRate Stride rate.
 */
- (void)bluetoothClient:(UABluetoothClient *)client didUpdateWorkoutDataWithStepCount:(uint16_t)stepCount caloriesExpended:(uint16_t)caloriesExpended willPower:(double)willPower measuredPosture:(uint8_t)posture strideRate:(uint8_t)strideRate;

@end
