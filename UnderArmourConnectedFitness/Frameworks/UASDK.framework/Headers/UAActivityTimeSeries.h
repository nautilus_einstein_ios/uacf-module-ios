/*
 * UAActivityTimeSeries.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UAEntity.h>
#import <UASDK/UAActivityRef.h>

@interface UAActivityTimeSeries : UAEntity <UAEntityInterface>

/**
 *  The reference of this activity.
 */
@property (nonatomic, strong) UAActivityRef *ref;

/**
 * The timezone at which this activity occurred in.
 */
@property (nonatomic, strong) NSTimeZone *timezone;

/**
 * A dictionary that represents all of the time series data.
 * 
 * 
 * The following keys are supported:
 * 
 * "calories" - Energy expended (in joules) UATimeSeries with UATimeSeriesPoints 
 * "distance" - Distance (in m) UATimeSeries with UATimeSeriesPoints
 * "steps" - Steps UATimeSeries with UATimeSeriesPoints
 *
 * Note that when creating a point for any of these values, the timestamp should be in seconds, not ms.
 *
 * For an example, see MMFNewActivityTimeSeriesViewController::addNewTimeSeriesPoint in the UASDK Sample App.
 */
@property (nonatomic, copy) NSDictionary *timeSeries;

/**
 * A string that represents what kind of device was used to record this activity.
 */
@property (nonatomic, copy) NSString *recorderType;

/**
 *  A string that represents a unique identifier of the recording device.
 */
@property (nonatomic, copy) NSString *recorderIdentifier;

@end
