/*
 * UAUserStats.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UAEntity.h>
#import <UASDK/UAEntityListRef.h>

@class UAActivityTypeReference;

@interface UAUserStats : UAEntity

@property (nonatomic, strong) NSDate *aggregationPeriodStart;
@property (nonatomic, strong) NSDate *aggregationPeriodEnd;
@property (nonatomic, strong) NSNumber *distance;
@property (nonatomic, strong) NSNumber *duration;
@property (nonatomic, strong) NSNumber *averagePace;
@property (nonatomic, strong) NSNumber *averageSpeed;
@property (nonatomic, strong) NSNumber *energy;

@property (nonatomic, strong) NSNumber *activityCount;

@property (nonatomic, strong) NSNumber *timeInHearRateZone1;
@property (nonatomic, strong) NSNumber *timeInHearRateZone2;
@property (nonatomic, strong) NSNumber *timeInHearRateZone3;
@property (nonatomic, strong) NSNumber *timeInHearRateZone4;
@property (nonatomic, strong) NSNumber *timeInHearRateZone5;

@property (nonatomic, strong) UAActivityTypeReference *activityTypeRef;

@end
