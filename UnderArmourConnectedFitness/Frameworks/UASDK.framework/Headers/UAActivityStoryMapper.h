//
//  UAActivityStoryMapper.h
//  UA
//
//  Created by Jeff Oliver on 6/9/14.
//  Copyright (c) 2014 MapMyFitness Inc. All rights reserved.
//

#import "UAActivityStoryList.h"
#import "UAActivityStory.h"
#import "UAActivityStoryCommentList.h"
#import "UAActivityStoryComment.h"

@interface UAActivityStoryMapper : NSObject

+ (UAActivityStoryList *)activityStoryListFromResponse:(NSDictionary *)dictionary;

+ (UAActivityStory *)storyFromDictionary:(NSDictionary *)dictionary;

+ (UAActivityStoryCommentList *)commentListFromDictionary:(NSDictionary *)dictionary;

+ (UAActivityStoryComment *)commentFromDictionary:(NSDictionary *)dictionary;

+ (UAActivityStoryLikeList *)likesListFromDictionary:(NSDictionary *)dictionary;

@end
