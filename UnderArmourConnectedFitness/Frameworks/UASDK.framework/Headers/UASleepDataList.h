/*
 * UASleepDataList.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UAEntityList.h>
#import <UASDK/UAEntityListRef.h>

@interface UASleepDataListRef : UAEntityListRef <NSCoding>

+ (UASleepDataListRef *)sleepDataListRefWithStartDate:(NSDate *)start
                                                                                  endData:(NSDate *)end
                                                                                    limit:(NSUInteger)limit;

+ (UASleepDataListRef *)sleepDataListRefWithStartDate:(NSDate *)start
                                                                                  endData:(NSDate *)end;

@end

@interface UASleepDataList : UAEntityList <NSCoding, UAEntityListRefInterface>

/**
 *  Reference to this object.
 */
@property (nonatomic, strong) UASleepDataListRef *ref;

/**
 *  Reference to the next paged collection. This will be nil if there is no next paged collection.
 */
@property (nonatomic, strong) UASleepDataListRef *nextRef;

/**
 *  Reference to the previous paged collection. This will be nil if there is no previous paged collection.
 */
@property (nonatomic, strong) UASleepDataListRef *previousRef;

@end
