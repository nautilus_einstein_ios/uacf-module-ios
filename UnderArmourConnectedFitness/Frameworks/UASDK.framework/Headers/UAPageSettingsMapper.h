/*
 * UAPageSettingsMapper.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <Foundation/Foundation.h>

@class UAPageSettings;

@interface UAPageSettingsMapper : NSObject

+ (UAPageSettings *)pageSettingsFromDictionary:(NSDictionary *)dictionary;

@end
