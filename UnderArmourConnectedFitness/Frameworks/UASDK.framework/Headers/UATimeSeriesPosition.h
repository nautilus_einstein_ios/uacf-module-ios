/*
 * UATimeSeriesPosition.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UATimeSeries.h>

@class UATimeSeriesPositionPoint;

@interface UATimeSeriesPosition : UATimeSeries <UATimeSeriesProtocol>

- (void)addPoint:(UATimeSeriesPositionPoint *)point;

- (UATimeSeriesPositionPoint *)pointAtIndex:(NSUInteger)index;

@end
