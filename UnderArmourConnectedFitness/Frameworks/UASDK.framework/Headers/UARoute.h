/*
 * UARoute.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <UASDK/UAEntity.h>
#import <UASDK/UAEntityRef.h>

@class UAUserRef, UAPrivacyRef;

@interface UARouteRef : UAEntityRef

@end


/**
 *  An object that represents a route.
 *
 *  @since 1.0
 */
@interface UARoute : UAEntity <UAEntityInterface, NSCoding>

/**
 *  The name of the route.
 */
@property (nonatomic, copy) NSString *routeName;

/**
 *  The description of the route.
 */
@property (nonatomic, copy) NSString *routeDescription;

/**
 *  The reference of this route.
 */
@property (nonatomic, strong) UARouteRef *ref;

/**
 *  The user in which this route belongs to.
 */
@property (nonatomic, strong) UAUserRef *userRef;

/**
 *
 */
@property (nonatomic, strong) NSArray *activityTypeRefs;

/**
 *  The distance (in meters) of this route.
 *  @discussion Expressed as a float.
 */
@property (nonatomic, strong) NSNumber *distanceNumber;
@property (nonatomic, assign) double distance;

/**
 *  The date in which this route was created.
 */
@property (nonatomic, strong) NSDate *createdDateTime;

/**
 *  The date in which this route was updated.
 */
@property (nonatomic, strong) NSDate *updatedDateTime;

/**
 *  The start point type of this route.
 */
@property (nonatomic, copy) NSString *startPointType;

/**
 *  The starting location point of this route.
 */
@property (nonatomic, assign) CLLocationCoordinate2D startingLocation;

/**
 *  A list of UARouteLocation objects for this route.
 */
@property (nonatomic, copy) NSArray *points;

/**
 *  A list of climb objects for this route.
 */
@property (nonatomic, copy) NSArray *climbs;

/**
 *  The total ascent of this route.
 *  @discussion Expressed as a float.
 */
@property (nonatomic, strong) NSNumber *totalAscentNumber;
@property (nonatomic, assign) double totalAscent;

/**
 *  The total descent of this route.
 *  @discussion Expressed as a float.
 */
@property (nonatomic, strong) NSNumber *totalDescentNumber;
@property (nonatomic, assign) double totalDescent;

/**
 *  The minimum elevation of this route.
 *  @discussion Expressed as a float.
 */
@property (nonatomic, strong) NSNumber *minElevationNumber;
@property (nonatomic, assign) double minElevation;

/**
 *  The maximum elevation of this route.
 *  @discussion Expressed as a float.
 */
@property (nonatomic, strong) NSNumber *maxElevationNumber;
@property (nonatomic, assign) double maxElevation;
/**
 *  The city (if available) in which this route is located.
 */
@property (nonatomic, copy) NSString *city;

/**
 *  The state (if available) in which this route is located.
 */
@property (nonatomic, copy) NSString *state;

/**
 *  The country in which this route is located.
 */
@property (nonatomic, copy) NSString *country;

/**
 *  The postal code (if available) in which this route is located.
 */
@property (nonatomic, copy) NSString *postalCode;

/**
 *  The data source in which route was created from.
 */
@property (nonatomic, copy) NSString *dataSource; //expose?

/**
 *  The privacy state of this route.
 */
@property (nonatomic, strong) UAPrivacyRef *privacyRef;

/**
 *  The URL of the thumbanil of this route.
 */
@property (nonatomic, copy) NSURL *thumbnailURL;

- (NSURL *)thumbnailUrlWithWidth:(NSUInteger)width
                          height:(NSUInteger)height;


/**
 Retrieves the distance in meters covered by
 the location points stored in the route
 */
- (double)calculateRouteDistanceMeters;

@end
