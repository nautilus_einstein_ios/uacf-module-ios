/*
 * UAFriendshipSummaryList.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UAEntityList.h>
#import <UASDK/UAEntityListRef.h>
#import <UASDK/UAFriendshipSummary.h>

@class UAUserRef;

@interface UAFriendshipListRef : UAEntityListRef

+ (UAFriendshipListRef *)getFriendshipListRefForUser:(UAUserRef *)userRef
                                          withStatus:(UAFriendshipStatus)status;

+ (UAFriendshipListRef *)getFriendshipListRefForUser:(UAUserRef *)userRef
                                          withStatus:(UAFriendshipStatus)status
                                           withLimit:(NSInteger)limit;
@end


@interface UAFriendshipSummaryList : UAEntityList <UAEntityListRefInterface>

@property (nonatomic, strong) UAFriendshipListRef *ref;

/**
 *  A reference to the previous collection of entities.
 *  @discussion If no more entities exist, this value will be nil.
 */
@property (nonatomic, strong) UAFriendshipListRef *previousRef;

/**
 *  A reference to the next collection of entities.
 *  @discussion If no more entities exist, this value will be nil.
 */
@property (nonatomic, strong) UAFriendshipListRef *nextRef;


@end
