//
//  UAActivityStoryStatusPostObject.h
//  UA
//
//  Created by Jeremy Zedell on 5/29/14.
//  Copyright (c) 2014 MapMyFitness Inc. All rights reserved.
//

#import <UASDK/UAActivityStoryObject.h>
#import <UASDK/UAPrivacyRef.h>

@interface UAActivityStoryStatusPostObject : UAActivityStoryObject <NSCoding>

@property (nonatomic, strong) UAEntityRef *ref;

@property (nonatomic, strong) UAPrivacyRef *privacyRef;

@property (nonatomic, copy) NSString *text;

@property (nonatomic, assign) UAPrivacyType privacy;

@end
