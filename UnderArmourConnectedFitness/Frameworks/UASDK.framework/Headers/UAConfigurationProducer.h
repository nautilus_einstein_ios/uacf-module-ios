/*
 * UAConfigurationProducer.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import "UAMessageProducer.h"

@class UARecorderContext;

@interface UAConfigurationProducer : UAMessageProducer

/**
 *  Produces a configuration message containing a context.
 *
 *  @param context A context for the recorder.
 */
- (void)produceConfigurationMessageWithContext:(UARecorderContext *)context;

@end
