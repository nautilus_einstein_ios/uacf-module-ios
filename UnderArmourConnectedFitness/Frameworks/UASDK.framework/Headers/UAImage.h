/*
 * UAImage.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <Foundation/Foundation.h>

@class UAImageRef;

@interface UAImage : NSObject

@property (nonatomic, strong) UAImageRef *imageReference;

@property (nonatomic, strong) NSURL *imageUrl;

// @TODO -- move this to an internal header?
@property (nonatomic, copy) NSString *templateImageUrl;

- (NSURL *)imageUrlWithWidth:(NSUInteger)width
                      height:(NSUInteger)height;

@end
