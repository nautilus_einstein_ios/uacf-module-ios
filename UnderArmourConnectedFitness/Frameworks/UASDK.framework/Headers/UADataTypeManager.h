//
//  UADataTypeManager.h
//  UASDK
//
//  Created by mallarke on 11/19/14.
//  Copyright (c) 2014 Under Armour. All rights reserved.
//

#import <UASDK/UAManager.h>

@class UADataType, UADataTypeRef;

@interface UADataTypeManager : NSObject

+ (UADataType *)dataTypeFromDataTypeRef:(UADataTypeRef *)reference;

@end
