//
//  UAActivityStoryLike.h
//  UA
//
//  Created by Jeremy Zedell on 5/29/14.
//  Copyright (c) 2014 MapMyFitness Inc. All rights reserved.
//

#import "UAActivityStory.h"

@interface UAActivityStoryLike : UAActivityStory

@property (nonatomic, copy) NSString *likeID;
@property (nonatomic, strong) UAActivityStoryActor *actor;
@property (nonatomic, strong) NSDate *published;

@end
