/*
 * UAHeartRateZones.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAEntity.h>
#import <UASDK/UAEntityRef.h>

@class UAUserRef;

@interface UAHeartRateZonesRef : UAEntityRef

/**
 *  Builds a reference based on the heart rate zones object ID.
 *  @param heartRateZonesID ID of the heart rate zones object. Must not be nil.
 *  @return Reference to the heart rate zones object.
 */
+ (instancetype)heartRateZonesRefWithID:(NSString *)heartRateZonesID;

@end

@interface UAHeartRateZones : UAEntity <UAEntityInterface, NSCoding>

/**
 *  Reference to the heart rate zones object.
 *  @return Reference to the heart rate zones object.
 */
@property (nonatomic, strong) UAHeartRateZonesRef *ref;

/**
 *  Reference to the user object who owns this object.
 *  @return Reference to the user object.
 */
@property (nonatomic, strong) UAUserRef *userRef;

/**
 *  Zones contained in this object.
 *  @return NSArray of UAHeartRateZone objects
 */
@property (nonatomic, copy) NSArray *zones;

/**
 *  Date the object was created.
 */
@property (nonatomic, strong) NSDate *createdDate;

@end
