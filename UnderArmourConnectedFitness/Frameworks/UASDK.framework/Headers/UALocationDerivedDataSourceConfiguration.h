/*
 * UALocationDerivedDataSourceConfiguration.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import "UADerivedDataSourceConfiguration.h"

@interface UALocationDerivedDataSourceConfiguration : UADerivedDataSourceConfiguration <UADataSourceConfigurationInterface>

@end
