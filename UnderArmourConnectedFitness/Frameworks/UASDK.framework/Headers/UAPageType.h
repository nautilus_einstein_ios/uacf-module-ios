/*
 * UAPageType.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAEntity.h>
#import <UASDK/UAPageTypeRef.h>

/**
 *  An object that characterizes the type of a given UAPage.
 */
@interface UAPageType : UAEntity <UAEntityInterface>

/**
 *  The title of this page type.
 */
@property (nonatomic, strong) NSString *title;

/**
 *  A reference to this page type.
 */
@property (nonatomic, strong) UAPageTypeRef *ref;

@end
