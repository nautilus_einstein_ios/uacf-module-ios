/*
 * UAGroupList.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <Foundation/Foundation.h>
#import <UASDK/UAEntityList.h>
#import <UASDK/UAEntityListRef.h>

typedef NS_ENUM(NSUInteger, UAGroupViewType)
{
	UAGroupViewTypeMember, UAGroupViewTypeInvited,
	UAGroupViewTypeInProgress, UAGroupViewTypeCompleted,
	UAGroupViewTypeAll 
};

@class UAGroupPurposeRef;

@interface UAGroupListRef : UAEntityListRef

+ (instancetype)groupListRefWithUserRef:(UAUserRef *)reference;

+ (instancetype)groupListRefWithUserRef:(UAUserRef *)reference withViewType:(UAGroupViewType)type groupPurposeRef:(UAGroupPurposeRef *)groupPurposeRef;

@end

@interface UAGroupList : UAEntityList <UAEntityListRefInterface>

@property (nonatomic, strong) UAGroupListRef *ref;

/**
*  A reference to the previous collection of groups.
*  @discussion If no collection has previously been visited, this value will be nil.
*/
@property (nonatomic, strong) UAGroupListRef *previousRef;

/**
*  A reference to the next collection of groups.
*  @discussion If no more groups exist, this value will be nil.
*/
@property (nonatomic, strong) UAGroupListRef *nextRef;

@end
