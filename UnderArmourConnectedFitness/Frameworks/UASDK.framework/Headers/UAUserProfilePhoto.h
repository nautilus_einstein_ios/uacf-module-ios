/*
 * UAUserProfilePhoto.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAEntity.h>
#import <UASDK/UAEntityRef.h>

@interface UAUserProfilePhotoRef : UAEntityRef

@end

@interface UAUserProfilePhoto : UAEntity

/**
 *  The reference to this profile photo reference
 */
@property (nonatomic, strong) UAUserProfilePhotoRef *ref;

@property (nonatomic, strong) NSURL *smallImageUrl;
@property (nonatomic, strong) NSURL *mediumImageUrl;
@property (nonatomic, strong) NSURL *largeImageUrl;

@end
