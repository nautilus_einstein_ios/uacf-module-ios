/*
 * UATimeSeriesPowerPoint.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UATimeSeriesPoint.h>

@interface UATimeSeriesPowerPoint : UATimeSeriesPoint

+ (instancetype)pointAtTime:(NSTimeInterval)timestamp withPower:(double)power;

/**
 *  Power in watts
 */
@property (nonatomic, readonly) double power;

@end
