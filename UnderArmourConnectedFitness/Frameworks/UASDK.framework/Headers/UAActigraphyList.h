/*
 * UAActigraphyList.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UAEntityList.h>
#import <UASDK/UAEntityListRef.h>

@class UAUserRef, UADateRange;

@interface UAActigraphyListRef : UAEntityListRef

typedef enum {
    UAActigraphyTimeIntervalDefault = 0,
    UAActigraphyTimeIntervalOneHour = 3600,
    UAActigraphyTimeIntervalThirtyMinutes = 1800,
    UAActigraphyTimeIntervalFifteenMinutes = 900
} UAActigraphyTimeInterval;

/**
 *  Returns a UAActigraphyListRef for the current day
 *
 *  @param userRef an optional reference to the user to create the reference for
 *  @param range         the range of dates to generate the reference for. A start date is required, but end date is optional.
 *  @param interval      the desired interval for the data to be returned in
 *
 *  @return UAActigraphyListRef
 */
+ (UAActigraphyListRef*)actigraphyListRefForUser:(UAUserRef*)userRef
                                                                           dateRange:(UADateRange *)range
                                                                            interval:(UAActigraphyTimeInterval)interval;

/**
 *  Returns a UAActigraphyListRef for the current day
 *
 *  @param userRef an optional reference to the user to create the reference for
 *  @param interval      the desired interval for the data to be returned in
 *
 *  @return UAActigraphyListRef
 */
+ (UAActigraphyListRef*)actigraphyListRefForTodayWithUser:(UAUserRef*)userRef
                                                                                     interval:(UAActigraphyTimeInterval)interval;

@end

@interface UAActigraphyList : UAEntityList <UAEntityListRefInterface>

@property (nonatomic, strong) UAActigraphyListRef *ref;

/**
 *  Date range of the reference. If there is no date specified in the reference, then this will return nil.
 */
@property (nonatomic, readonly) UADateRange *dateRange;

/**
 * Oldest date of actigraphy object fetched.
 */
@property (nonatomic, copy) NSString *oldestDate;

/**
 *  Reference to the previous page
 */
@property (nonatomic, strong) UAActigraphyListRef *previousRef;

/**
 *  Reference to the next page
 */
@property (nonatomic, strong) UAActigraphyListRef *nextRef;

@property (nonatomic, readonly) NSArray *dailySummaries __deprecated_msg("This method is deprecated, use .objects");
@property (nonatomic, readonly, getter = arePastSummariesAvailable) BOOL pastSummariesAvailable __deprecated_msg("This will be removed soon. Check (previousReference != nil)");
@property (nonatomic, readonly, getter = areFutureSummariesAvailable) BOOL futureSummariesAvailable __deprecated_msg("This will be removed soon. Check (nextReference != nil)");

@end
