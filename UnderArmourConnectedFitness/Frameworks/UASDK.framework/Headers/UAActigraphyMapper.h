/*
 * UAActigraphyMapper.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UAObjectMapper.h>

@class UAActigraphyList;

@interface UAActigraphyMapper : UAObjectMapper

+ (NSArray*)summaryObjectsFromResponse:(NSDictionary*)response;

+ (UAActigraphyList *)acigraphyListFromResponse:(NSDictionary *)response;


@end
