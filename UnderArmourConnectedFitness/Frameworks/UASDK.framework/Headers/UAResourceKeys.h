/*
 * UAResourceKeys.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 */


// links
extern NSString * const kUALinksKey;
extern NSString * const kUALinksSelfKey;
extern NSString * const kUALinksDocumentationKey;
extern NSString * const kUALinksHrefKey;
extern NSString * const kUALinksIdKey;
extern NSString * const kUALinksNextKey;
extern NSString * const kUALinksPreviousKey;

// embedded
extern NSString * const kUAEmbeddedKey;

// refs
extern NSString * const kUARefsKey;
extern NSString * const kUARefsLimitKey;
extern NSString * const kUARefsOffsetKey;

// totalCount
extern NSString * const kUATotalCountKey;

// user
extern NSString * const kUAUserKey;
extern NSString * const kUAUserDisplayNameKey;
extern NSString * const kUAUserProfilePictureKey;

// aggregates
extern NSString * const kUAAggregatesKey;
extern NSString * const kUAAggregatesDataTypeKey;
extern NSString * const kUAAggregatesUserKey;
extern NSString * const kUAAggregatesSummaryKey;
extern NSString * const kUAAggregatesPeriodsKey;
extern NSString * const kUAAggregatesPeriodKey;
extern NSString * const kUAAggregatesStartDatetimeKey;
extern NSString * const kUAAggregatesEndDatetimeKey;
extern NSString * const kUAAggregatesDatetimeKey;
extern NSString * const kUAAggregatesValueKey;

// groupLeaderboard
extern NSString * const kUAGroupLeaderboardKey;
extern NSString * const kUAGroupLeaderboardPrevIterationKey;
extern NSString * const kUAGroupLeaderboardNextIterationKey;

// dataFieldMeasurementType
extern NSString * const kUADataFieldMeasurementTypeKey;
extern NSString * const kUADataFieldMeasurementTypeLatitudeKey;
extern NSString * const kUADataFieldMeasurementTypeLongitudeKey;
extern NSString * const kUADataFieldMeasurementTypeHorizontalAccuracyKey;
extern NSString * const kUADataFieldMeasurementTypeElevationKey;
extern NSString * const kUADataFieldMeasurementTypeVerticalAccuracyKey;
extern NSString * const kUADataFieldMeasurementTypeHeartRateKey;
extern NSString * const kUADataFieldMeasurementTypeHeightKey;
extern NSString * const kUADataFieldMeasurementTypeMassKey;
extern NSString * const kUADataFieldMeasurementTypeLeanMassKey;
extern NSString * const kUADataFieldMeasurementTypeFatMassKey;
extern NSString * const kUADataFieldMeasurementTypeSpeedKey;
extern NSString * const kUADataFieldMeasurementTypeWillpowerKey;
extern NSString * const kUADataFieldMeasurementTypeDistanceKey;
extern NSString * const kUADataFieldMeasurementTypeEnergyExpendedKey;
extern NSString * const kUADataFieldMeasurementTypeEnergyConsumedKey;
extern NSString * const kUADataFieldMeasurementTypeStepsKey;
extern NSString * const kUADataFieldMeasurementTypeIntensityKey;

// dataFieldSummaryType
extern NSString * const kUADataFieldSummaryTypeKey;
extern NSString * const kUADataFieldSummaryTypeDistanceSumKey;
extern NSString * const kUADataFieldSummaryTypeActiveTimeSumKey;
extern NSString * const kUADataFieldSummaryTypeEnergyExpendedSumKey;
extern NSString * const kUADataFieldSummaryTypeEnergyConsumedSumKey;
extern NSString * const kUADataFieldSummaryTypeMassMaxKey;
extern NSString * const kUADataFieldSummaryTypeMassMinKey;
extern NSString * const kUADataFieldSummaryTypeMassAvgKey;
extern NSString * const kUADataFieldSummaryTypeMassStartKey;
extern NSString * const kUADataFieldSummaryTypeMassEndKey;
extern NSString * const kUADataFieldSummaryTypeLeanMassMaxKey;
extern NSString * const kUADataFieldSummaryTypeLeanMassMinKey;
extern NSString * const kUADataFieldSummaryTypeLeanMassAvgKey;
extern NSString * const kUADataFieldSummaryTypeLeanMassStartKey;
extern NSString * const kUADataFieldSummaryTypeLeanMassEndKey;
extern NSString * const kUADataFieldSummaryTypeFatMassMaxKey;
extern NSString * const kUADataFieldSummaryTypeFatMassMinKey;
extern NSString * const kUADataFieldSummaryTypeFatMassAvgKey;
extern NSString * const kUADataFieldSummaryTypeFatMassStartKey;
extern NSString * const kUADataFieldSummaryTypeFatMassEndKey;
extern NSString * const kUADataFieldSummaryTypeStepsSumKey;
extern NSString * const kUADataFieldSummaryTypeSpeedMaxKey;
extern NSString * const kUADataFieldSummaryTypeSpeedAvgKey;
extern NSString * const kUADataFieldSummaryTypeSpeedMinKey;
extern NSString * const kUADataFieldSummaryTypeHeartRateMaxKey;
extern NSString * const kUADataFieldSummaryTypeHeartRateAvgKey;
extern NSString * const kUADataFieldSummaryTypeHeartRateMinKey;
extern NSString * const kUADataFieldSummaryTypeSessionsSumKey;
extern NSString * const kUADataFieldSummaryTypeSleepTimeSumKey;
extern NSString * const kUADataFieldSummaryTypeSleepTimeAvgKey;

// friendship
extern NSString * const kUAFriendshipKey;
extern NSString * const kUAFriendshipFriendshipsKey;
extern NSString * const kUAFriendshipToUserKey;
extern NSString * const kUAFriendshipFromUserKey;
extern NSString * const kUAFriendshipStatusKey;
extern NSString * const kUAFriendshipPendingKey;
extern NSString * const kUAFriendshipActiveKey;
extern NSString * const kUAFriendshipCreatedDatetimeKey;
extern NSString * const kUAFriendshipMessageKey;

// suggestedFriends
extern NSString * const kUASuggestedFriendsKey;
extern NSString * const kUASuggestedFriendsSuggestionsKey;
extern NSString * const kUASuggestedFriendsReasonsKey;
extern NSString * const kUASuggestedFriendsNativeKey;
extern NSString * const kUASuggestedFriendsFacebookKey;
extern NSString * const kUASuggestedFriendsMutualFriendsKey;
extern NSString * const kUASuggestedFriendsCountKey;
extern NSString * const kUASuggestedFriendsUserKey;
extern NSString * const kUASuggestedFriendsSuggestedFriendsForKey;
extern NSString * const kUASuggestedFriendsSuggestedFriendsEmailsKey;
extern NSString * const kUASuggestedFriendsSourceKey;
extern NSString * const kUASuggestedFriendsWeightKey;

// heartRateZones
extern NSString * const kUAHeartRateZonesKey;
extern NSString * const kUAHeartRateZonesUserKey;
extern NSString * const kUAHeartRateZonesCreatedDateKey;
extern NSString * const kUAHeartRateZonesZonesKey;
extern NSString * const kUAHeartRateZonesStartKey;
extern NSString * const kUAHeartRateZonesEndKey;
extern NSString * const kUAHeartRateZonesNameKey;

// heartRateZoneCalculations
extern NSString * const kUAHeartRateZoneCalculationsKey;

// remoteConnections
extern NSString * const kUARemoteConnectionsKey;
extern NSString * const kUARemoteConnectionsCreatedDatetimeKey;
extern NSString * const kUARemoteConnectionsLastSyncTimeKey;
extern NSString * const kUARemoteConnectionsRemoteIdKey;
extern NSString * const kUARemoteConnectionsUserKey;

// remoteConnectionTypes
extern NSString * const kUARemoteConnectionTypesKey;
extern NSString * const kUARemoteConnectionTypesTypeKey;
extern NSString * const kUARemoteConnectionTypesNameKey;
extern NSString * const kUARemoteConnectionTypesRecorderTypeKeyKey;
extern NSString * const kUARemoteConnectionTypesIntroCopyHeadingKey;
extern NSString * const kUARemoteConnectionTypesIntroCopyBodyKey;
extern NSString * const kUARemoteConnectionTypesDisconnectCopyKey;
extern NSString * const kUARemoteConnectionTypesDisconnectConfirmKey;
extern NSString * const kUARemoteConnectionTypesDisconnectCancelKey;
extern NSString * const kUARemoteConnectionTypesOauthConnectLinkKey;
extern NSString * const kUARemoteConnectionTypesLogoLinkKey;
extern NSString * const kUARemoteConnectionTypesLogoLinkLightKey;

// recorderPriorities
extern NSString * const kUARecorderPrioritiesKey;
extern NSString * const kUARecorderPrioritiesActivityKey;
extern NSString * const kUARecorderPrioritiesSleepKey;

// actigraphyRecorderPriority
extern NSString * const kUAActigraphyRecorderPriorityKey;
extern NSString * const kUAActigraphyRecorderPriorityQsTypeKey;
extern NSString * const kUAActigraphyRecorderPriorityRecorderTypeKeyKey;

// group
extern NSString * const kUAGroupKey;
extern NSString * const kUAGroupUsersKey;
extern NSString * const kUAGroupGroupUserKey;
extern NSString * const kUAGroupGroupInviteKey;
extern NSString * const kUAGroupGroupPurposeKey;
extern NSString * const kUAGroupGroupOwnerKey;
extern NSString * const kUAGroupGroupInvitesKey;
extern NSString * const kUAGroupActivityFeedKey;
extern NSString * const kUAGroupGroupObjectiveKey;
extern NSString * const kUAGroupCriteriaKey;
extern NSString * const kUAGroupDataTypeKey;
extern NSString * const kUAGroupDataTypeFieldKey;
extern NSString * const kUAGroupStartDatetimeKey;
extern NSString * const kUAGroupEndDatetimeKey;
extern NSString * const kUAGroupIterationStartDatetimeKey;
extern NSString * const kUAGroupIterationEndDatetimeKey;
extern NSString * const kUAGroupIterationKey;
extern NSString * const kUAGroupPeriodKey;
extern NSString * const kUAGroupDescriptionKey;
extern NSString * const kUAGroupInvitationRequiredKey;
extern NSString * const kUAGroupInvitationCodeKey;
extern NSString * const kUAGroupIsPublicKey;
extern NSString * const kUAGroupNameKey;
extern NSString * const kUAGroupGroupsKey;
extern NSString * const kUAGroupCountKey;
extern NSString * const kUAGroupMemberKey;
extern NSString * const kUAGroupInvitedKey;
extern NSString * const kUAGroupInProgressKey;
extern NSString * const kUAGroupCompletedKey;
extern NSString * const kUAGroupAllKey;
extern NSString * const kUAGroupMaxUsersKey;

// activityStory
extern NSString * const kUAActivityStoryKey;
extern NSString * const kUAActivityStoryNameKey;
extern NSString * const kUAActivityStoryCompleteKey;
extern NSString * const kUAActivityStoryUpdateKey;
extern NSString * const kUAActivityStoryInviteKey;
extern NSString * const kUAActivityStoryGroupLeaderboardKey;
extern NSString * const kUAActivityStoryGroupUsersKey;
extern NSString * const kUAActivityStoryResultKey;
extern NSString * const kUAActivityStoryLeaderboardKey;
extern NSString * const kUAActivityStoryRankKey;
extern NSString * const kUAActivityStoryStartTimeKey;
extern NSString * const kUAActivityStoryEndTimeKey;
extern NSString * const kUAActivityStoryValueKey;
extern NSString * const kUAActivityStoryDataTypeKey;
extern NSString * const kUAActivityStoryDataTypeFieldKey;
extern NSString * const kUAActivityStoryInviteAcceptedKey;
extern NSString * const kUAActivityStoryPurposeKey;

// pageFollows
extern NSString * const kUAPageFollowsKey;

