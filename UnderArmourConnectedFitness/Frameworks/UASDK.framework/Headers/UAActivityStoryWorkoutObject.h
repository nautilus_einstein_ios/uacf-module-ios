//
//  UAActivityStoryWorkoutObject.h
//  UA
//
//  Created by Jeremy Zedell on 5/29/14.
//  Copyright (c) 2014 MapMyFitness Inc. All rights reserved.
//

#import <UASDK/UAActivityStoryObject.h>
#import <UASDK/UAActivityStoryHighlight.h>

@class UAWorkoutRef, UAActivityTypeReference, UAPrivacyRef, UARouteRef, UAActivityType, UALocation;

@interface UAActivityStoryWorkoutObject : UAActivityStoryObject <NSCoding>

@property (nonatomic, strong) UAWorkoutRef *ref;

@property (nonatomic, strong) UAActivityTypeReference *activityTypeRef;

@property (nonatomic, strong) UARouteRef *routeRef;

@property (nonatomic, strong) UAPrivacyRef *privacyRef;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, assign) double distance;

@property (nonatomic, assign) double averagePace;

/**
 Highlights are the top three fields for a workout or route object, 
 chosen from a pre-determined list by activity type or object type. 
 An object can have between 0 and 3 highlights, and only fields that
 are present and non-zero will be highlighted.
 
 Distance, duration and pace/speed highlights have an optional percentile callout. 
 Percentiles include: Top 10%, Top 25%. Thresholds are dependent on gender and
 only activity types with a base type of run, bike, or walk can have callouts.
 
 @return NSArray of UAActivityStoryHighlight
 */
@property (nonatomic, strong) NSArray *highlights;

@property (nonatomic, copy) NSString *notes;

@property (nonatomic, assign) UAPrivacyType privacy;

@property (nonatomic, assign) NSInteger steps;

@property (nonatomic, assign) double averageSpeed;

@property (nonatomic, assign) NSInteger duration;

@property (nonatomic, assign) NSInteger energyBurned;

/**
 @discussion This is a partial activityType object, it will include only the activityTypeID and activityTypeName;
 */
@property (nonatomic, strong) UAActivityType *activityType;

// DEPRECATED per Kate Dubuisson 6/19/14
//@property (nonatomic, strong) UAActivityTypeSummary *subActivityType;

@property (nonatomic, strong) UALocation *location;

@end
