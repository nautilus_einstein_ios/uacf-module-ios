/*
 * UAActivityTypeList.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UAEntityList.h>
#import <UASDK/UAEntityListRef.h>

@interface UAActivityTypeListRef : UAEntityListRef <NSCoding>

+ (UAActivityTypeListRef *)activityTypeListRefForAllActivities;

@end

@interface UAActivityTypeList : UAEntityList <NSCoding, UAEntityListRefInterface>

/**
 *  Reference to this object.
 */
@property (nonatomic, strong) UAActivityTypeListRef *ref;

/**
 *  Reference to the next paged collection. This will be nil if there is no next paged collection.
 */
@property (nonatomic, strong) UAActivityTypeListRef *nextRef;

/**
 *  Reference to the previous paged collection. This will be nil if there is no previous paged collection.
 */
@property (nonatomic, strong) UAActivityTypeListRef *previousRef;

@end
