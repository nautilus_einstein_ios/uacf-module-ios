/*
 * UAPageSettings_Internal.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#ifndef UA_UAPageSettings_Internal_h
#define UA_UAPageSettings_Internal_h

@interface UAPageSettings()

@property (nonatomic, assign) BOOL featuredGalleryEnabled;
@property (nonatomic, assign) BOOL graphEnabled;
@property (nonatomic, copy) NSString *ctaText;
@property (nonatomic, copy) NSString *ctaLink;
@property (nonatomic, copy) NSString *ctaTarget;
@property (nonatomic, copy) NSString *templateName;

@end

#endif
