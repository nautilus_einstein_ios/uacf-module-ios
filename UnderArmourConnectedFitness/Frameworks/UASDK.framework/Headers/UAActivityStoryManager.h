//
//  UAActivityStoryManager.h
//  UA
//
//  Created by Jeff Oliver on 6/9/14.
//  Copyright (c) 2014 MapMyFitness Inc. All rights reserved.
//

@import UIKit;

#import <UASDK/UAManager.h>
#import <UASDK/UAEnumerations.h>

@class UAActivityStory, UAActivityStoryRef, UAActivityStoryList, UAActivityStoryListRef, UAActivityStoryComment, UAActivityStoryCommentList,
	   UAUserRef, UAImage, UAUploadRef;

typedef void (^UAActivityStoryResponseBlock)(UAActivityStory *object, NSError *error);
typedef void (^UAActivityStoryListResponseBlock)(UAActivityStoryList *object, NSError *error);
typedef void (^UAActivityStoryCommentResponseBlock)(UAActivityStoryComment *object, NSError *error);

@interface UAActivityStoryManager : UAManager

- (void)fetchFeedForRef:(UAActivityStoryListRef *)reference
               response:(UAActivityStoryListResponseBlock)response;

- (void)fetchStoryWithRef:(UAActivityStoryRef *)reference
                 response:(UAActivityStoryResponseBlock)response;

- (void)updateStoryPrivacy:(UAPrivacyType)privacyType
                  forStory:(UAActivityStory *)story
                  response:(UAResponseBlock)response;

- (void)createStoryForUploadRef:(UAUploadRef *)uploadRef
                 uploadProgress:(NSProgress * __autoreleasing *)progress
                       response:(UAActivityStoryResponseBlock)response
                         cancel:(void(^)(UAActivityStoryRef *))cancel;

- (void)updateStoryText:(NSString *)text
               forStory:(UAActivityStory *)story
               response:(UAActivityStoryResponseBlock)response;

- (void)repostStoryWithActivityStoryRef:(UAActivityStoryRef *)reference
                            withUserRef:(UAUserRef *)userRef
                            withPrivacy:(UAPrivacyType)privacy
                      withShareFacebook:(BOOL)shareFacebook
                       withShareTwitter:(BOOL)shareTwitter
                               withText:(NSString *)text
                         uploadProgress:(NSProgress * __autoreleasing *)progress
                               response:(UAActivityStoryResponseBlock)response;

- (void)cancelUploadForUploadRef:(UAUploadRef *)uploadReference
                      deletePost:(BOOL)deletePost;

- (void)deleteStoryWithRef:(UAActivityStoryRef*)reference
                  response:(UAResponseBlock)response;

- (void)addComment:(NSString *)comment
      withStoryRef:(UAActivityStoryRef *)reference
          response:(UAActivityStoryCommentResponseBlock)response;

- (void)likeStoryWithRef:(UAActivityStoryRef *)reference
                response:(UAActivityStoryResponseBlock)response;

@end
