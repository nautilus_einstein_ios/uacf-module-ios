/*
 * UAAggregateList.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAEntityList.h>
#import <UASDK/UAEntityListRef.h>

@class UADateRange, UAPeriod;
@class UAUserRef;

@interface UAAggregateListRef : UAEntityListRef

/**
 * Create an aggregate list ref for a user for a list of data types
 * @param userRef User to fetch aggregate
 * @param dataTypes Array of UADataTypeRefs to fetch
 * @param range Date range to fetch
 */
+ (instancetype)aggregateListRefWithUserRef:(UAUserRef *)userRef
								  dataTypes:(NSArray *)dataTypes
								  dateRange:(UADateRange *)range;

/**
 * Create an aggregate list ref for a user for a list of data types
 * @param userRef User to fetch aggregate
 * @param dataTypes Array of UADataTypeRefs to fetch
 * @param range Date range to fetch
 * @param period Period the data should come back in. UAPeriodDurationWeek is 
 *		  not supported at this time and a count of 1 should be used for UAPeriodDurationDay, 
 *		  UAPeriodDurationWeekMonth, UAPeriodDurationWeekYear.
 */
+ (instancetype)aggregateListRefWithUserRef:(UAUserRef *)userRef
								  dataTypes:(NSArray *)dataTypes
								  dateRange:(UADateRange *)range
									 period:(UAPeriod *)period;

@end

@interface UAAggregateList : UAEntityList <UAEntityListRefInterface>

@property (nonatomic, strong) UAAggregateListRef *ref;

/**
*  A reference to the previous collection of aggregates.
*  @discussion If no collection has previously been visited, this value will be nil.
*/
@property (nonatomic, strong) UAAggregateListRef *previousRef;

/**
*  A reference to the next collection of aggregates.
*  @discussion If no more aggregates exist, this value will be nil.
*/
@property (nonatomic, strong) UAAggregateListRef *nextRef;

@end
