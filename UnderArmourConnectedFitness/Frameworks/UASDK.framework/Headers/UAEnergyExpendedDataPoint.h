//
//  UAEnergyExpendedDataPoint.h
//  UASDK
//
//  Created by Morrow, Andrew on 11/20/14.
//  Copyright (c) 2014 Under Armour. All rights reserved.
//

#import <UASDK/UADataPoint.h>

@interface UAEnergyExpendedDataPoint : UADataPoint

/**
 *  Energy expended (double, joules).
 */
@property (nonatomic, readonly) NSNumber *energyExpended;

/**
 *  Instantiate an energy expended data point.
 *  @param energyExpended Energy expended in joules. Must not be nil.
 *  @return An instantiated data point.
 */
- (instancetype)initWithEnergyExpended:(NSNumber *)energyExpended NS_DESIGNATED_INITIALIZER;

@end
