/*
 * UAActigraphySettingsMapper.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

@import Foundation;

@class UAActigraphySettings;

@interface UAActigraphySettingsMapper : NSObject

+ (UAActigraphySettings *)actigraphySettingsFromDictionary:(NSDictionary *)dictionary;

@end
