/*
 * UAObjectMapper.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */



#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <objc/runtime.h>

@class UAEntityList;

@interface UAObjectMapper : NSObject
@property(nonatomic, strong) NSString *objectName;

+ (const char *)getPropertyType:(objc_property_t *)property;

+ (NSDictionary *)getObjectPropertiesForClass:(Class)aClass;

+ (NSAttributeType)getAttributeType:(NSString *)stringType;

+ (NSEntityDescription *)buildEntityFromClass:(NSString *)entityName;

+ (NSNumber*)totalCountForListResponse:(NSDictionary*)collection;

@end
