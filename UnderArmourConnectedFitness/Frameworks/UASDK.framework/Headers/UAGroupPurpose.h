/*
 * UAGroupPurpose.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAEntity.h>
#import <UASDK/UAEntityRef.h>

@interface UAGroupPurposeRef : UAEntityRef

+ (UAGroupPurposeRef *)groupPurposeRefWithGroupPurposeID:(NSString *)groupPurposeID;

@end

@interface UAGroupPurpose : UAEntity <UAEntityInterface, NSCoding>

/**
 *  Reference to the group purpose.
 *  @return Reference to the group purpose.
 */
@property (nonatomic, strong) UAGroupPurposeRef *ref;

/**
 *  The group purpose name.
 *  @return The group purpose name.
 */
@property (nonatomic, copy) NSString *purpose;

/**
 *  Boolean indicating if membership of groups in this purpose is restricted.
 *  @return Boolean indicating if membership is restricted.
 *  @discussion If true, special criteria must be met to use this group purpose.
 */
@property (nonatomic, assign) BOOL restrictMembership;

@end
