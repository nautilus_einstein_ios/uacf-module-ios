/*
 * UADataSourceConfiguration.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <Foundation/Foundation.h>

@class UADataSource;

@protocol UADataSourceConfigurationInterface <NSObject>

/**
 *  Builds a UADataSource with given priority settings.
 *
 *  @return A UADataSource.
 */
- (UADataSource *)build;

@end

@interface UADataSourceConfiguration : NSObject <UADataSourceConfigurationInterface>

/**
 *  A custom name for this data source.
 *  @discussion This field is optional, but required if you intend to observe any
 *  changes in state of the data source (i.e. connection status, health).
 */
@property (nonatomic, copy) NSString *name;

@end
