/*
 * UARemoteConnectionTypeManager.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UAManager.h>
#import <UASDK/UAActigraphySettingsManager.h>

@class UARemoteConnectionType, UARemoteConnectionTypeRef, UARemoteConnectionTypeList, UARemoteConnectionTypeListRef;

typedef void (^UARemoteConnectionTypeResponseBlock)(UARemoteConnectionType *object, NSError *error);
typedef void (^UARemoteConnectionTypeListResponseBlock)(UARemoteConnectionTypeList *object, NSError *error);

/**
 *  Manages the interface to fetch remote connection type objects.
 *
 */
@interface UARemoteConnectionTypeManager : UAManager <UAActigraphySettingsManagerInterface>

/**
 *  Fetches a remote connection type resource
 *
 *  @param reference The resource of the remote connection type.
 *  @param response  A block called on completion.
 */
- (void)fetchRemoteConnectionTypeWithRef:(UARemoteConnectionTypeRef *)reference
								response:(UARemoteConnectionTypeResponseBlock)response;


/**
 *  Fetches a list of remote connection type resources
 *
 *  @param reference The list reference for the remote connection types.
 *  @param response  A block called on completion.
 */
- (void)fetchRemoteConnectionTypesWithListRef:(UARemoteConnectionTypeListRef *)reference
									 response:(UARemoteConnectionTypeListResponseBlock)response;

@end
