/*
 * UARouteManager.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


@import Foundation;
@import CoreLocation;

#import <UASDK/UAManager.h>

@class UARoute, UARouteRef, UARouteBookmarkRef, UARouteListRef, UARouteBookmark, UARouteBookmarkList, UAUserRef, UARouteList, UARouteBookmarkListRef;

typedef void (^UARouteResponseBlock)(UARoute *object, NSError *error);
typedef void (^UARouteListResponseBlock)(UARouteList *object, NSError *error);
typedef void (^UARouteBookmarkResponseBlock)(UARouteBookmark *object, NSError *error);
typedef void (^UARouteBookmarkListResponseBlock)(UARouteBookmarkList *object, NSError *error);

/**
 *  UARouteManager is a class designated for modifying and fetching routes.
 */
@interface UARouteManager : UAManager

/**
 *  Fetches a collection of routes nearby.
 *
 *  @param reference a collection reference to a set of routes.
 *  @param response A block called on completion.
 */
- (void)fetchRoutesWithListRef:(UARouteListRef *)reference
                      response:(UARouteListResponseBlock)response;

- (void)fetchRouteWithRef:(UARouteRef *)reference
              withDetails:(BOOL)detailed
                 response:(UARouteResponseBlock)response;

/**
 *  Saves a route to the MapMyFitness servers.
 *
 *  @param route   a route to save.
 *  @param response A block called on completion.
 */
- (void)createRoute:(UARoute *)route
         response:(UARouteResponseBlock)response;

- (void)updateRoute:(UARoute *)route
           response:(UARouteResponseBlock)response;

- (void)deleteRouteWithRef:(UARouteRef *)reference
                  response:(UAResponseBlock)response;


/**
 *  Fetches bookmarked routes for a given user.
 *
 *  @param reference a user to query against.
 *  @param response A block called on completion.
 */
- (void)fetchRouteBookmarksWithListRef:(UARouteBookmarkListRef *)reference
                              response:(UARouteBookmarkListResponseBlock)response;



/**
 *  Adds a bookmark for a given user and route.
 *
 *  @param reference A reference to the route to bookmark.
 *  @param response A block called on completion.
 */
- (void)createBookmarkWithRouteRef:(UARouteRef *)reference
                        response:(UARouteBookmarkResponseBlock)response;

/**
 *  Deletes a bookmark for a route, for the currently authorized user.
 *
 *  @param reference a reference to the route bookmark to delete.
 *  @param response A block called on completion.
 */
- (void)deleteBookmarkWithRouteRef:(UARouteBookmarkRef *)reference
                          response:(UAResponseBlock)response;

/**
 *  Geocodes a route and adds locality, administrative area, and country information.
 *
 *  @param route   a route to geocode.
 *  @param response A block called on completion.
 */
- (void)geocodeRoute:(UARoute *)route
            response:(UARouteResponseBlock)response;

@end
