/*
 * UAErrorCodes.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

@import Foundation;

static NSString * kUASDKErrorDomain = @"com.underarmour.uasdk";

// This is an attempt to unify error codes. This will need to be expanded and documented as it grows
#define __UA_NIL_RESPONSE_CODE__              100
#define __UA_ERROR_INVALID_RESPONSE__         110
#define __UA_ERROR_INVALID_DATA_ELEMENT__     200
#define __UA_ERROR_INVALID_USER_REFERENCE__   210
#define __UA_ERROR_NO_CACHED_USER__           300
#define __UA_ERROR_TIMEOUT__                  400

// Auth Errors
#define __UA_ERROR_USER_AUTH_REQUIRED__             500
#define __UA_ERROR_USER_INVALID_CREDENTIALS__       501

