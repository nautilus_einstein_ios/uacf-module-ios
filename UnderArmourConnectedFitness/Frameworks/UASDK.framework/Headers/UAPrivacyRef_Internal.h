/*
 * UAPrivacyRef_Internal.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAPrivacyRef.h>

@interface UAPrivacyRef ()

+ (UAPrivacyRef *)privacyRefWithPrivacyType:(UAPrivacyType)privacyType;

@end
