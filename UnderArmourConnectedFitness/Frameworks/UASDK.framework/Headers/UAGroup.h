/*
 * UAGroup.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAEntity.h>
#import <UASDK/UAEntityRef.h>

@class UAActivityStoryRef;
@class UAGroupObjective, UAGroupPurposeRef, UAGroupUserListRef, UAGroupUserRef, UAGroupInviteRef;
@class UAUserRef;

/**
 *  An object that references a group.
 */
@interface UAGroupRef : UAEntityRef

+ (instancetype)groupRefWithGroupID:(NSString*)groupID;

+ (instancetype)groupRefWithGroupID:(NSString *)groupID  withInvitationCode:(NSString *)invitationCode;

@end

/**
 *  UAGroup contains group information and details.
 */
@interface UAGroup : UAEntity <UAEntityInterface, NSCoding>

/**
 Reference to this group
 */
@property (nonatomic, strong) UAGroupRef *ref;

/**
* Short name for the group
*/
@property (nonatomic, copy) NSString *name;

/**
* Short description of the group
*/
@property (nonatomic, copy) NSString *groupDescription;

/**
* Bool value to denote if an invitation is required to join the group.
*/
@property (nonatomic, assign) BOOL invitationRequired;

/**
* Invitation code used to accept an invitation to the group
*/
@property (nonatomic, copy) NSString *invitationCode;

/**
* Bool value to donote if the group is public
*/
@property (nonatomic, assign) BOOL isPublic;

/**
 * Objective of the group.
 */
@property (nonatomic, strong) UAGroupObjective *groupObjective;

/**
* Link to the group's users
*/
@property (nonatomic, strong) UAGroupUserListRef *userListRef;

/**
 * The number of users in the group
 */
@property (nonatomic) NSUInteger userListCount;

/**
 * Maximum users allowed to join group.
 */
@property (nonatomic, readonly) NSNumber *maxUsers;

/**
* Link to the group's activity story
*/
@property (nonatomic, strong) UAActivityStoryRef *activityStoryRef;

/**
 Link to the group's purpose
 */
@property (nonatomic, strong) UAGroupPurposeRef *groupPurposeRef;

/**
 Link to the group user
 */
@property (nonatomic, strong) UAGroupUserRef *groupUserRef;

/**
 Link to the group invite
 */
@property (nonatomic, strong) UAGroupInviteRef *groupInviteRef;

/**
 Link to the group owner
 */
@property (nonatomic, strong) UAUserRef *groupOwnerRef;

@end
