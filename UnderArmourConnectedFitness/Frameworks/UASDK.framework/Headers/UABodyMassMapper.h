/*
 * UABodyMassMapper.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <Foundation/Foundation.h>

@class UABodyMass, UABodyMassList;

@interface UABodyMassMapper: NSObject

+ (UABodyMass *)UABodyMassFromResponse:(NSDictionary *)response;

+ (UABodyMassList *)UABodyMassListFromResponse:(NSDictionary *)response;

+ (NSDictionary *)dictionaryFromUABodyMass:(UABodyMass *)bodyMass;

@end
