/*
 * UAGroupLeaderboardManager.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAManager.h>

@class UAGroupLeaderboardList, UAGroupLeaderboardListRef;

typedef void (^UAGroupLeaderboardListResponseBlock)(UAGroupLeaderboardList *object, NSError *error);

@interface UAGroupLeaderboardManager : UAManager

- (void)fetchGroupLeaderboardsWithListRef:(UAGroupLeaderboardListRef *)reference response:(UAGroupLeaderboardListResponseBlock)response;

@end
