/*
 * UAModerationManager.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAManager.h>

@class UAEntityRef;

@interface UAModerationManager : UAManager

/**
 *  Flags an entity
 *
 *  @param reference The reference to the entity to flag
 *  @param response  A block called on completion
 */
- (void)flagEntityWithRef:(UAEntityRef *)reference response:(UAResponseBlock)response;

/**
 *  Unflags an entity
 *
 *  @param reference The reference to the entity to unflag
 *  @param response  A block called on completion
 */
- (void)unflagEntityWithRef:(UAEntityRef *)reference response:(UAResponseBlock)response;

- (void)flag:(UAEntityRef *)reference response:(UAResponseBlock)response __deprecated_msg("Use flagEntityWithRef:response insead");
- (void)unflag:(UAEntityRef *)reference response:(UAResponseBlock)response __deprecated_msg("Use unflagEntityWithRef:response insead");

@end
