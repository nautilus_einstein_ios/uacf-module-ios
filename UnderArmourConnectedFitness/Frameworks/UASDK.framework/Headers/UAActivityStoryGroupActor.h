//
//  UAActivityStoryGroupActor.h
//  UASDK
//
//  Created by DX165 on 2014-11-24.
//  Copyright (c) 2014 Under Armour. All rights reserved.
//

#import <UASDK/UAActivityStoryActor.h>

@class UAGroupRef, UAGroupInviteListRef, UAGroupUserListRef;
@class UADataTypeRef, UAPeriod;

@interface UAActivityStoryGroupActor : UAActivityStoryActor

@property (nonatomic, strong) UAGroupRef *groupRef;

@property (nonatomic, copy) NSString *name;
@property (nonatomic, strong) UADataTypeRef *dataTypeRef;
@property (nonatomic, copy) NSString *dataTypeField;

@property (nonatomic, strong) NSDate *startTime;
@property (nonatomic, strong) NSDate *endTime;
@property (nonatomic, strong) UAPeriod *period;

@property (nonatomic, strong) UAGroupInviteListRef *groupInviteListRef;
@property (nonatomic) NSUInteger groupInviteCount;

@property (nonatomic, strong) UAGroupUserListRef *groupUserListRef;
@property (nonatomic) NSUInteger groupUserCount;

@end
