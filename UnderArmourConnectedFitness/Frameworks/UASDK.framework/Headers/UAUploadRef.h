/*
 * UAUploadRef.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import "UAEntityRef.h"

#pragma mark - UAUploadRef interface -

@interface UAUploadRef : NSObject

@property (nonatomic, strong) NSURLSessionDataTask *task;
@property (nonatomic, assign) BOOL willCancel;

@end
