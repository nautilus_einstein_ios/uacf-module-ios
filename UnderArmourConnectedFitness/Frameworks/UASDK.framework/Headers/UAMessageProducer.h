/*
 * UAMessageProducer.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <Foundation/Foundation.h>
#import <UASDK/UAEnumerations.h>

@class UAMessage, UAProcessorMessageQueue;

@protocol UAMessageProducerInterface <NSObject>

/**
 *  Deserializes a stream of data into a message.
 *
 *  @return A UAMessage.
 *
 *  @bug This is not implemented.
 */
- (UAMessage *)deserialize;

@end

/**
 *  The base class for an object that produces messages containing data.
 */
@interface UAMessageProducer : NSObject

@property (nonatomic, weak, readonly) UAProcessorMessageQueue *processorMessageQueue;

- (instancetype)initWithQueue:(UAProcessorMessageQueue *)queue NS_DESIGNATED_INITIALIZER;

- (void)beginRecorder;
- (void)finishRecorder;

@end
