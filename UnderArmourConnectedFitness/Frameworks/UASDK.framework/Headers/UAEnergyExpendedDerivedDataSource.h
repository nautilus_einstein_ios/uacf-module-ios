/*
 * UAEnergyExpendedDerivedDataSource.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import "UADerivedDataSource.h"

@interface UAEnergyExpendedDerivedDataSource : UADerivedDataSource

@end
