/*
 * UATimeSeriesActigraphy.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UATimeSeries.h>

@class UATimeSeriesTimerStopPoint;

@interface UATimeSeriesActigraphy : UATimeSeries <UATimeSeriesProtocol>

@property (nonatomic, assign) NSTimeInterval interval;

@end
