//
//  UAActivityStoryAttachmentVideo.h
//  UA
//
//  Created by Jeff Oliver on 7/11/14.
//  Copyright (c) 2014 MapMyFitness Inc. All rights reserved.
//

#import "UAActivityStoryAttachment.h"

@interface UAActivityStoryAttachmentVideo : UAActivityStoryAttachment

@property (nonatomic, strong) NSURL *thumbnailURL;
@property (nonatomic, strong) NSString *thumbnailTemplate;

@property (nonatomic, copy) NSString *providerID;

@property (nonatomic, copy) NSString *provider;

@end
