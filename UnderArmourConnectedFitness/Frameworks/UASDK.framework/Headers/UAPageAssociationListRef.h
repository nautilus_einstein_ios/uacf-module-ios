/*
 * UAPageAssociationListRef.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAEntityListRef.h>

@class UAPageRef;

@interface UAPageAssociationListRef : UAEntityListRef

/**
 * @return A reference for a collection of page associations that are associated via a FROM relationship with the given page reference.
 */
+ (UAPageAssociationListRef *)pageAssociationListRefFromPageWithPageReference:(UAPageRef *)reference;

/**
 * @return A reference for a collection of page associations that are associated via a TO relationship with the given page reference.
 */
+ (UAPageAssociationListRef *)pageAssociationListRefToPageWithPageReference:(UAPageRef *)reference;

@end
