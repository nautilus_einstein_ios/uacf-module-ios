//
//  UAActivityStoryToutObject.h
//  UA
//
//  Created by Jeremy Zedell on 5/29/14.
//  Copyright (c) 2014 MapMyFitness Inc. All rights reserved.
//

#import "UAActivityStoryObject.h"

typedef NS_ENUM(NSInteger, UAToutSubtype)
{
    UAToutSubtypeUnknown,
	UAToutSubtypeFindFriends
};

@interface UAActivityStoryToutObject : UAActivityStoryObject <NSCoding>

@property (nonatomic, assign) UAToutSubtype subtype;

@end
