//
//  UAActivityStoryHighlight.h
//  UA
//
//  Created by Jeff Oliver on 6/10/14.
//  Copyright (c) 2014 MapMyFitness Inc. All rights reserved.
//

typedef NS_ENUM(NSInteger, UAHighlightType)
{
    UAHighlightTypeUnknown,
	UAHighlightTypeAveragePace,
    UAHighlightTypeAverageSpeed,
	UAHighlightTypeDistance,
	UAHighlightTypeDuration,
    UAHighlightTypeEnergyBurned,
    UAHighlightTypeRoute,
    UAHighlightTypeSteps
};

@interface UAActivityStoryHighlight : NSObject <NSCoding>

@property (nonatomic, assign) UAHighlightType type;

/**
  25 = Top 25%, 10 = Top 10% etc.
 @return NSInteger representing the top percentile for this highlight.
*/
@property (nonatomic, assign) NSInteger percentile;

@end
