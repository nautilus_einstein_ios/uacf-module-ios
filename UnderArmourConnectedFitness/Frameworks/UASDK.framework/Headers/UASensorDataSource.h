/*
 * UASensorDataSource.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import "UADataSource.h"

@class UASensorMessageProducer, UASensorDataSource;

@protocol UASensorDataSourceObserver <NSObject>


/**
 *  A callback for whenever a sensor's status updates.
 *
 *  @param sensorStatus         The current status of the sensor.
 *  @param dataSourceIdentifier The name of this sensor data source.
 *
 *  @discussion dataSourceIdentifier will be of type UADataSourceIdentifier in the future.
 */
- (void)sensorStatusUpdated:(UASensorStatus)sensorStatus forDataSource:(NSString *)dataSourceIdentifier;

/**
 *  A callback for whenever a sensor's health updates.
 *
 *  @param sensorHealth         The current health of the sensor.
 *  @param dataSourceIdentifier The name of this sensor data source.
 *
 *  @discussion dataSourceIdentifier will be of type UADataSourceIdentifier in the future.
 */
- (void)sensorHealthUpdated:(UASensorHealth)sensorHealth forDataSource:(NSString *)dataSourceIdentifier;

@end

@interface UASensorDataSource : UADataSource

/**
 *  The current status of this sensor data source.
 */
@property (nonatomic, readonly) UASensorStatus status;

/**
 *  The current health of this sensor data source.
 */
@property (nonatomic, readonly) UASensorHealth health;

/**
 *  A producer for this sensor data source.
 */
@property (nonatomic, readonly) UASensorMessageProducer *sensorMessageProducer;

/**
 *  A delegate to listen in for changes in the data source.
 */
@property (nonatomic, weak) id<UASensorDataSourceObserver> observer;

@end
