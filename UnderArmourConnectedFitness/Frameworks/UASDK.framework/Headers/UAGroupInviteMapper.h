/*
 * UAGroupInviteMapper.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <Foundation/Foundation.h>

@class UAGroupInviteList;
@class UAGroupInvite;

@interface UAGroupInviteMapper : NSObject

+ (UAGroupInvite *)groupInviteFromResponse:(NSDictionary *)response;

+ (UAGroupInviteList *)groupInviteListFromResponse:(NSDictionary *)response;

+ (NSDictionary *)dictionaryFromUAGroupInvite:(UAGroupInvite *)invite;

+ (NSDictionary *)dictionaryFromUAGroupInvites:(NSArray *)invites;

@end
