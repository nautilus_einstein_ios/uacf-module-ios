/*
 * UAFriendshipMapper.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import "UAFriendshipSummary.h"
#import <UASDK/UAObjectMapper.h>

@class UAGetFriendsFilter, UAUserList, UAFriendshipSummaryList, UASuggestedFriendList, UAFriendshipSummary;

@interface UAFriendshipMapper : UAObjectMapper

/**
 *  Creates a collection object based off of a suggested friends response.
 */
+ (UASuggestedFriendList *)listFromSuggestedFriendsResponse:(NSDictionary *)response;

/**
 *  Creates a collectiond friendship object from a friendship endpoint reponse
 */
+ (UAFriendshipSummaryList *)listFromFriendshipResponse:(NSDictionary *)response;


/**
 *  Parses a single friendship summary object
 */
+ (UAFriendshipSummary *)friendshipSummaryFromResponse:(NSDictionary *)response;

/**
 *  Parses and returns an array of friendship objects
 */
+ (NSArray *)friendSummaryObjectsFromResponse:(NSDictionary*)response;

/**
 * @return UAFriendshipStatus for given friendship status string ("active" or "pending")
 */
+ (UAFriendshipStatus)friendshipStatusFromString:(NSString*)status;

@end
