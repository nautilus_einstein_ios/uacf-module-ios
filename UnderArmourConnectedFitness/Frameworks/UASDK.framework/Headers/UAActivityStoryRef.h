/*
 * UAActivityStoryRef.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAEntityRef.h>

@class UAPageRef;

@interface UAActivityStoryRef : UAEntityRef

+ (UAActivityStoryRef*)activityStoryReferenceWithStoryID:(NSString*)storyID;

@end
