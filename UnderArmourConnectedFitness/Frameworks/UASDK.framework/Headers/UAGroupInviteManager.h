/*
 * UAGroupInviteManager.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAManager.h>

@class UAGroupInviteListRef;
@class UAGroupInviteList;
@class UAUserRef;
@class UAGroupRef;
@class UAGroupInviteRef;
@class UAGroupInvite;

typedef void (^UAGroupInvitesResponseBlock)(UAGroupInvite *object, NSError *error);
typedef void (^UAGroupInvitesListResponseBlock)(UAGroupInviteList *object, NSError *error);

@interface UAGroupInviteManager : UAManager

- (void)fetchGroupInvitesWithListRef:(UAGroupInviteListRef *)reference
                            response:(UAGroupInvitesListResponseBlock)response;

/*
 * Invite a user to a group.
 * @discusion The invite only requires either a user ref or an email.  If both are supplied, the user ref is used.
 */
- (void)sendInvite:(UAGroupInvite *)invite response:(UAResponseBlock)response;

- (void)sendInvites:(NSArray /* UAGroupInvite */ *)invites response:(UAResponseBlock)response;

- (void)deleteInviteWithRef:(UAGroupInviteRef *)reference response:(UAResponseBlock)response;

@end
