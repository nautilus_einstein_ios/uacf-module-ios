/*
 * UAFollowerListRef.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAEntityListRef.h>

@interface UAFollowerListRef : UAEntityListRef

@end
