/*
 * UAActigraphyData.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <Foundation/Foundation.h>
#import <UASDK/UATimeSeriesActigraphy.h>
#import <UASDK/UADateRange.h>

@interface UAActigraphyDataAggregates : NSObject

/**
 Sum for the data
 */
@property (nonatomic, strong) NSNumber *sum;

/**
 Average for the data
 */
@property (nonatomic, strong) NSNumber *average;

/**
 Min value in the data
 */
@property (nonatomic, strong) NSNumber *min;

/**
 Max value in the data
 */
@property (nonatomic, strong) NSNumber *max;

/**
 Latest value recorded
 */
@property (nonatomic, strong) NSNumber *latest;

/**
 Contains additional aggregate detail and varies depending on the metric type
 @discussion Contains keys that maps to aggregate detail metrics. The value for the key will be
 a UAActigraphyDataAggregates object.
 */
@property (nonatomic, strong) NSDictionary *aggregateDetail;

@end


@interface UAActigraphyData : NSObject

/**
 Start and end date for the data represented by this object
 */
@property (nonatomic, strong) UADateRange *dateRange;

/**
 Aggregated total for this object
 */
@property (nonatomic, strong) UAActigraphyDataAggregates *aggregates;

/**
 Date this data was recorded
 */

// maybe this will not be used here
//@property (nonatomic, strong) NSDate *dateCreated;

/**
 Time series data for this object
 */
@property (nonatomic, strong) UATimeSeriesActigraphy *timeSeries;

/**
 Optional additional data about the actigrpahy data
 */
@property (nonatomic, strong) NSDictionary *metaData;

@end
