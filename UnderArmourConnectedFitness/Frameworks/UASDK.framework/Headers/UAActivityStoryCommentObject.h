//
//  UAActivityStoryCommentObject.h
//  UA
//
//  Created by Jeff Oliver on 6/12/14.
//  Copyright (c) 2014 MapMyFitness Inc. All rights reserved.
//

#import "UAActivityStoryObject.h"

@interface UAActivityStoryCommentObject : UAActivityStoryObject <NSCoding>

@property (nonatomic, copy) NSString *text;

@end
