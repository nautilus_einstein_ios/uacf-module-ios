/*
 * UATimeSeriesDistance.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UATimeSeries.h>

@class UATimeSeriesDistancePoint;

@interface UATimeSeriesDistance : UATimeSeries <UATimeSeriesProtocol>

- (void)addPoint:(UATimeSeriesDistancePoint *)point;

- (UATimeSeriesDistancePoint *)pointAtIndex:(NSUInteger)index;

@end
