/*
 * UASleepDataMapper.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <Foundation/Foundation.h>

@class UASleepData, UASleepDataList;

@interface UASleepDataMapper: NSObject

+ (UASleepData *)UASleepDataFromResponse:(NSDictionary *)response;

+ (UASleepDataList *)UASleepDataListFromResponse:(NSDictionary *)response;

+ (NSDictionary *)dictionaryFromUASleepData:(UASleepData *)sleepData;

@end
