/*
 * UABrand.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UIKit/UIKit.h>

// TODO:  - need reference.

@interface UABrand : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) BOOL popular;
@property (nonatomic, copy) NSString *gearTypeId;

@end
