/*
 * UARemoteConnection.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAEntity.h>
#import <UASDK/UAEntityRef.h>

@class UAUserRef;

@interface UARemoteConnectionRef : UAEntityRef

@end

/**
 *  Class that represent a connection between a user and a remote connection type. 
 */
@interface UARemoteConnection : UAEntity <UAEntityInterface>

/**
 *  A reference to this remote connection.
 */
@property (nonatomic, strong) UARemoteConnectionRef *ref;

/**
 *  A user reference for this remote connection.
 */
@property (nonatomic, strong) UAUserRef *userRef;

/**
 *  A date that represents when this remote connection was created.
 */
@property (nonatomic, strong) NSDate *createdDatetime;

/**
 *  A date that represents the time the user last synced with this remote connection.
 */
@property (nonatomic, strong) NSDate *lastSyncTime;

/**
 *  The type of remote connection
 */
@property (nonatomic, copy) NSString *type;

@end
