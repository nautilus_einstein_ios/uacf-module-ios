/*
 * UAProcessorMessageQueue.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <Foundation/Foundation.h>

@class UAMessage;

/**
 *  A queue used for processing commands. This simulates the BlockingQueue class found in Java.
 *
 *  @see http://docs.oracle.com/javase/7/docs/api/java/util/concurrent/BlockingQueue.html for more info on blocking queues.
 */
@interface UAProcessorMessageQueue : NSObject

/**
 *  Creates an instance of this queue based on a given session name.
 *
 *  @param sessionName The UARecorder name.
 *
 *  @return An instance of a UAProcessorMessageQueue.
 *
 *  @discussion You must use this initializer in order for this class to behave correctly.
 */
- (instancetype)initWithSessionName:(NSString *)sessionName NS_DESIGNATED_INITIALIZER;

/**
 *  Adds a message to the queue.
 *
 *  @param message A UAMessage.
 */
- (void)put:(UAMessage *)message;

/**
 *  Adds a list of messages to the queue.
 *
 *  @param messages An array of UAMessages.
 */
- (void)putGroup:(NSArray *)messages;

/**
 *  Takes an object from the queue.
 *
 *  @return A UAMessage.
 *
 *  @discussion If there are no messages left to take, this method blocks the thread it's on
 *  until a new messages is put onto the queue.
 */
- (UAMessage *)take;

@end
