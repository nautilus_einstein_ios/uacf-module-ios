//
//  UADistanceSummaryDataPoint.h
//  UASDK
//
//  Created by mallarke on 11/26/14.
//  Copyright (c) 2014 Under Armour. All rights reserved.
//

#import <UASDK/UADataPoint.h>

@interface UADistanceSummaryDataPoint : UADataPoint

/**
 * Distance sum (double, meters)
 */
@property (nonatomic, readonly) NSNumber *distanceSum;


@end
