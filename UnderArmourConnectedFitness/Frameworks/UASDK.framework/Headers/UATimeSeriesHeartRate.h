/*
 * UATimeSeriesHeartRate.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UATimeSeries.h>

@class UATimeSeriesHeartRatePoint;

@interface UATimeSeriesHeartRate : UATimeSeries <UATimeSeriesProtocol>

- (void)addPoint:(UATimeSeriesHeartRatePoint *)point;

- (UATimeSeriesHeartRatePoint *)pointAtIndex:(NSUInteger)index;

@end
