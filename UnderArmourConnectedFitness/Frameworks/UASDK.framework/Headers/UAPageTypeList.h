//
//  UAPageTypeList.h
//  UA
//
//  Created by Jeff Oliver on 5/30/14.
//  Copyright (c) 2014 MapMyFitness Inc. All rights reserved.
//

#import <UASDK/UAEntityList.h>
#import <UASDK/UAPageTypeListRef.h>

@interface UAPageTypeList : UAEntityList <UAEntityListRefInterface>

@property (nonatomic, strong) UAPageTypeListRef *ref;

@property (nonatomic, strong) UAPageTypeListRef *previousRef;

@property (nonatomic, strong) UAPageTypeListRef *nextRef;

@end
