/*
 * UATimeSeriesPoint.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


@import Foundation;

/**
 *  Class that represents a general timeseries point
 */
@interface UATimeSeriesPoint : NSObject <NSCoding>

/**
 Epoch timestamp at which the point occurred
 */
@property (nonatomic, readonly) NSTimeInterval timestamp;

/**
 *  Array of values that have been set on this timeseries point. In general, its 
 *  recommended using the specific UATimeSeriesPoint subclass to access value.
 *  
 *  This should be looked at to see if it can be removed.
 */
@property (nonatomic, readonly) NSArray *values;

/**
 *  @internal -- This will most likely be removed
 *  Creates a timeseries point with an array of numbers. Its required
 *  that there are atleast 2 numbers and the first is a time interval.
 *
 *  pointArray[0] -- timestamp
 *  pointArray[1] -- numeric value
 *  ...
 *
 *  Note that subclasses may have different expectations for the lenth of this 
 *  array. Look at the documentation for the specific class.
 *
 *  @param pointArray The array of numbers to create the timeseries point with.
 *
 *  @return UATimeSeriesPoint object
 */
+ (instancetype)pointWithArray:(NSArray *)pointArray;

/**
 *  Creates a timeseries point from a timestamp and a number.
 *
 *  @param number The array of numbers to create the timeseries point with.
 *
 *  @return UATimeSeriesPoint object
 */
+ (instancetype)pointAtTime:(NSTimeInterval)timestamp withQuantity:(NSNumber *)number;

/**
 *  Creates a timeseries point with an array of numbers. Its required
 *  that there are atleast 1 number.
 *
 *  values[0] -- numeric value 1
 *  values[1] -- numeric value 2
 *  ...
 *
 *  @param pointArray The array of numbers to create the timeseries point with.
 *
 *  @return UATimeSeriesPoint object
 */
+ (instancetype)pointAtTime:(NSTimeInterval)timestamp withValues:(NSArray *)values;

@end
