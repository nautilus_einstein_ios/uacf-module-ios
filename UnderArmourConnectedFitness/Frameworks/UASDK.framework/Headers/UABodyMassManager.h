/*
 * UABodyMassManager.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


@import Foundation;

#import <UASDK/UAManager.h>

@class UABodyMass, UABodyMassReference, UABodyMassList, UABodyMassListRef;

typedef void (^UABodyMassResponseBlock)(UABodyMass *object, NSError *error);
typedef void (^UABodyMassListResponseBlock)(UABodyMassList *object, NSError *error);

@interface UABodyMassManager : UAManager

- (void)createBodyMass:(UABodyMass *)bodyMass
             response:(UABodyMassResponseBlock)response;

- (void)fetchBodyMassWithRef:(UABodyMassReference *)reference
                     response:(UABodyMassResponseBlock)response;

- (void)fetchBodyMassWithListRef:(UABodyMassListRef *)reference
                         response:(UABodyMassListResponseBlock)response;

- (void)updateBodyMass:(UABodyMass *)bodyMass
			  response:(UABodyMassResponseBlock)response;

- (void)deleteBodyMassWithRef:(UABodyMassReference *)reference
                     response:(UAResponseBlock)response __attribute__((unavailable("init not available")));

@end
