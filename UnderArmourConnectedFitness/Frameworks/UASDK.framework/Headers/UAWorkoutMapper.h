/*
 * UAWorkoutMapper.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


@import Foundation;

#import "UAObjectMapper.h"

#define kWorkoutRawDateFormat @"yyyy'-'MM'-'dd' 'HH':'mm':'ss'"
#define kWorkoutEndDateFormat @"yyyy-MM-dd HH:mm:ss"
#define kWorkoutDateFormat @"yyyy-MM-dd"
#define kSecondaryDateFormat @"MM/dd/yyyy"
#define kWorkoutDefaultTimeZone @"CST"
#define kTimeOnlyFormat @"hh:mm:ss"

@class UAWorkoutList, UAWorkout, UAWorkoutTimeSeries;

@interface UAWorkoutMapper : UAObjectMapper

+ (UAWorkoutList *)listFromResponse:(NSDictionary *)response;

+ (UAWorkout *)UAWorkoutFromDictionary:(NSDictionary *)result;

+ (NSDictionary *)dictionaryFromWorkout:(UAWorkout *)workout;

+ (NSDictionary *)dictionaryFromWorkoutTimeSeries:(UAWorkoutTimeSeries *)timeSeries;

+ (UAWorkoutTimeSeries *)workoutTimeSeriesFromDictionary:(NSDictionary *)result;

@end
