/*
 * UAActivityTimeSeriesManager.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


@import Foundation;

#import <UASDK/UAManager.h>

@class UAActivityTimeSeries, UAUserRef;

typedef void (^UAActivityTimeSeriesResponseBlock)(UAActivityTimeSeries *object, NSError *error);

@interface UAActivityTimeSeriesManager : UAManager

/**
 *  creates an activity to the server. If the activity with the specified reference key
 *  does not exist, then it is automatically created by the server. Otherwise, it will update
 *  the activity associated with the given reference key.
 *
 *  @param activity an UAActivityTimeSeries object to create.
 *  @param response  a block invoked when the request succeeds.
 */
- (void)createActivityTimeSeries:(UAActivityTimeSeries *)activity
                      response:(UAActivityTimeSeriesResponseBlock)response;

- (void)deleteActivityTimeSeries:(UAActivityTimeSeries *)activity
                        response:(UAResponseBlock)response;

@end
