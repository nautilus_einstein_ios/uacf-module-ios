/*
 * UADataSource.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <Foundation/Foundation.h>

@class UARecorderContext, UADataSourceIdentifier;

@interface UADataSource : NSObject

/**
 *  An identifier for this data source.
 */
@property (nonatomic, strong) UADataSourceIdentifier *dataSourceIdentifier;

/**
 *  The context for this data source.
 */
@property (nonatomic, readonly) UARecorderContext *recorderContext;

/**
 *  The name of this data source.
 */
@property (nonatomic, copy) NSString *name;

/**
 *  An array of type DataTypeRef.
 */
@property (nonatomic, copy, readonly) NSArray *dataTypeRefs;

/**
 *  Configures this data source.
 *  @discussion In the instance the recorder context is changed, it is necessary
 *  to call configure again.
 */
- (void)configureWithContext:(UARecorderContext *)recorderContext;

- (void)beginRecorder;

- (void)finishRecorder;

- (void)startSegment;

- (void)stopSegment;

@end
