/*
 * UADerivedDataSource.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import "UADataSource.h"

@class UADataPoint, UARecorderCalculator, UADataSourceIdentifier, UADataTypeRef;

@protocol UADerivedDataSourceInterface <NSObject>

- (void)deriveDataPointWithCalculator:(UARecorderCalculator *)calculator
				 dataSourceIdentifier:(UADataSourceIdentifier *)dataSourceIdentifier
						  dataTypeRef:(UADataTypeRef *)dataTypeRef
							dataPoint:(UADataPoint *)dataPoint;

@end

@interface UADerivedDataSource : UADataSource <UADerivedDataSourceInterface>

/**
 *  An array of UADataTypeRef objects that determine what kind of data types
 *  will trigger this class to derive data.
 *  
 *  @discussion An example of this would be a subclass for deriving distance
 *  from a location data point. In order for a class, say, UADistanceDerivedDataSource,
 *  to be triggered, it would require knowledge of a location data point. Hence, distance
 *  is dependent on location (a trigger).
 */
@property (nonatomic, copy, readonly) NSArray *dataTypeRefTriggers;

/**
 * Whether updates to the recorder time will trigger this class to derive data.
 */
@property (nonatomic, readonly, getter=isTriggeredByTime) BOOL triggeredByTime;

@end
