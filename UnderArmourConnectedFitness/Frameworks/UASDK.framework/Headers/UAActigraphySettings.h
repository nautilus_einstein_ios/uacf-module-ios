/*
 * UAActigraphySettings.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

@import Foundation;

@interface UAActigraphySettings : NSObject

/**
 *  The prioritized array of activity remote connections. These will be the string type
 *  of the remote connection type object
 */
@property (nonatomic, copy) NSArray *activityPriority;

/**
 *  The prioritized array of sleep remote connections. These will be the string type
 *  of the remote connection type object
 */
@property (nonatomic, copy) NSArray *sleepPriority;

@end
