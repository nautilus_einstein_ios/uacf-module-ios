/*
 * UAUserManager.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

@import Foundation;
@import UIKit;

#import <UASDK/UAManager.h>

@class UAUser, UAUserRef, UAUserList, UAUserListRef, UAUserProfilePhoto, UAUserProfilePhotoRef;

typedef void (^UAUserResponseBlock)(UAUser *object, NSError *error);
typedef void (^UAUserListResponseBlock)(UAUserList *object, NSError *error);
typedef void (^UAUserProfilePhotoResponseBlock)(UAUserProfilePhoto *object, NSError *error);

/**
 *   @class UAUserManager
 *
 *   The main interface to the User related operations.
 *
 */
@interface UAUserManager : UAManager

/**
 *  Fetches a user object.
 *
 *  @param reference A reference to a user.
 *  @param response  A block called on completion.
 */
- (void)fetchUserWithRef:(UAUserRef *)reference
                response:(UAUserResponseBlock)response;

/**
 *  Fetches a user object.
 *
 *  @param reference A reference to a user.
 *  @param cachePolicy Determine how the object will try and be fetched.
 *  @param response  A block called on completion.
 */
- (void)fetchUserWithRef:(UAUserRef *)reference
		 withCachePolicy:(UAObjectCachePolicy)cachePolicy
				response:(UAUserResponseBlock)response;

/**
 *  Fetches a list of users.
 *
 *  @param reference A reference to a list of users
 *  @param response A block called on completion.
 */
- (void)fetchUsersWithListRef:(UAUserListRef *)reference
                     response:(UAUserListResponseBlock)response;

/**
 *  Fetches a list of users.
 *
 *  @param reference A reference to a list of users
 *  @param cachePolicy Determine how the object will try and be fetched.
 *  @param response A block called on completion.
 */
- (void)fetchUsersWithListRef:(UAUserListRef *)reference
              withCachePolicy:(UAObjectCachePolicy)cachePolicy
                     response:(UAUserListResponseBlock)response;

/**
 *  Updates an existing user object.
 *
 *  @param user     The user object to
 *  @param response A block called on completion.
 */
- (void)updateUser:(UAUser *)user
          response:(UAUserResponseBlock)response;

/**
 *  Fetches the currently authed user
 *
 *  @param response A block called on completion.
 */
- (void)fetchAuthenticatedUser:(UAAuthResponseBlock)response;

#pragma mark UAUserProfilePhoto

/**
 *  Updates the profile image for a user.
 *
 *  @param image     The image to upload for the profile.
 *  @param userRef   The user reference that the profile is being uploaded for. This is expected to be the current user.
 *  @param response  A block called on completion.
 */
- (void)updateUserProfilePhotoImage:(UIImage *)image
					 forUserWithRef:(UAUserRef *)userRef
						   response:(UAUserProfilePhotoResponseBlock)response;

/**
 *  Fetches a user profile photo object.
 *
 *  @param reference A reference to a user profile photo.
 *  @param response A block called on completion.
 */
- (void)fetchUserProfilePhotoWithRef:(UAUserProfilePhotoRef *)ref
							response:(UAUserProfilePhotoResponseBlock)response;

@end

