/*
 * UALocationDataSource.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import "UASensorDataSource.h"

/**
 *  A message producer that produces location data from a GPS.
 */
@interface UALocationDataSource : UASensorDataSource 

@end
