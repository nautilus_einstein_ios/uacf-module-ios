//
//  UAActivityStoryList.h
//  UA
//
//  Created by Jeremy Zedell on 5/29/14.
//  Copyright (c) 2014 MapMyFitness Inc. All rights reserved.
//

#import <UASDK/UAEntityList.h>
#import <UASDK/UAActivityStoryListRef.h>

@class UAActivityStoryListRef;

@interface UAActivityStoryList : UAEntityList <UAEntityListRefInterface>

@property (nonatomic, strong) UAActivityStoryListRef *ref;

/**
 *  A reference to the previous collection of activity stories.
 *  @discussion If no more activity stories exist, this value will be nil.
 */
@property (nonatomic, strong) UAActivityStoryListRef *previousRef;

/**
 *  A reference to the next collection of activity stories.
 *  @discussion If no more activity stories exist, this value will be nil.
 */
@property (nonatomic, strong) UAActivityStoryListRef *nextRef;

@end
