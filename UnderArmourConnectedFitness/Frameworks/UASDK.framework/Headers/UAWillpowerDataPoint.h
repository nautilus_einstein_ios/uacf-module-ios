//
//  UAWillpowerDataPoint.h
//  UASDK
//
//  Created by Morrow, Andrew on 11/20/14.
//  Copyright (c) 2014 Under Armour. All rights reserved.
//

#import <UASDK/UADataPoint.h>

@interface UAWillpowerDataPoint : UADataPoint

/**
 *  WILLpower™ (double, unitless).
 */
@property (nonatomic, readonly) NSNumber *willpower;

/**
 *  Initialize a WILLpower™ data point.
 *  @param willpower WILLpower™ (double, unitless, must not be nil).
 *  @return Initialized data point.
 */
- (instancetype)initWithWillpower:(NSNumber *)willpower;

@end
