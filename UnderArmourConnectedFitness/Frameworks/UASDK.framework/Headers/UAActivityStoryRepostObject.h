//
//  UAActivityStoryRepostObject.h
//  UASDK
//
//  Created by Jeremiah Smith on 9/23/14.
//  Copyright (c) 2014 Under Armour. All rights reserved.
//

#import <UASDK/UAActivityStoryStatusPostObject.h>

@class UAActivityStory;

@interface UAActivityStoryRepostObject : UAActivityStoryStatusPostObject

@property (nonatomic, strong) UAActivityStory *activityStory;

@end
