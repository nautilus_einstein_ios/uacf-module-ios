/*
 * UAActivityStoryListRef.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAEntityListRef.h>

@class UAUserRef, UAGroupRef, UAPageRef, UAActivityStoryRef, UAWorkoutRef;

typedef NS_ENUM(NSUInteger, UAActivityStoryFeedType)
{
    UAActivityStoryFeedTypeUser,
    UAActivityStoryFeedTypeGroup,
    UAActivityStoryFeedTypePage
};

typedef NS_ENUM(NSUInteger, UAActivityStoryFeedView)
{
    UAActivityStoryFeedViewDefault,
    UAActivityStoryFeedViewMe,
    UAActivityStoryFeedViewFeatured
};

typedef NS_ENUM(NSInteger, UAActivityStoryViewType)
{
    UAActivityStoryViewTypeAll,
    UAActivityStoryViewTypeStatus,
    UAActivityStoryViewTypePhoto,
    UAActivityStoryViewTypeVideo,
    UAActivityStoryViewTypeWorkout
};

/**
 *  An object that references a collection of activity stories.
 */
@interface UAActivityStoryListRef : UAEntityListRef

+ (UAActivityStoryListRef*)activityStoryListRefForUser:(UAUserRef*)userRef
                                              feedType:(UAActivityStoryFeedType)type
                                              feedView:(UAActivityStoryFeedView)view
                                        collectionSize:(NSInteger)size;

+ (UAActivityStoryListRef*)activityStoryListRefForGroup:(UAGroupRef*)groupRef
                                               feedType:(UAActivityStoryFeedType)type
                                               feedView:(UAActivityStoryFeedView)view
                                         collectionSize:(NSInteger)size;

+ (UAActivityStoryListRef*)activityStoryListRefForPage:(UAPageRef*)pageReference
                                              feedType:(UAActivityStoryFeedType)type
                                              feedView:(UAActivityStoryFeedView)view
                                        collectionSize:(NSInteger)size;

+ (UAActivityStoryListRef*)activityStoryListRefForPublicFeedView:(UAActivityStoryViewType)view
                                                  collectionSize:(NSInteger)size;

/**
 * @return an activity story collection reference for the given story's comments.
 */
+ (UAActivityStoryListRef *)activityStoryListRefToCommentsForStory:(UAActivityStoryRef *)reference
                                                            offset:(int)offset
                                                             limit:(int)limit;

/**
 * @return an activity story collection reference for the given story's likes.
 */
+ (UAActivityStoryListRef *)activityStoryListRefToLikesForStory:(UAActivityStoryRef *)reference
                                                         offset:(int)offset
                                                          limit:(int)limit;

/**
 * @return an activity story collection reference for the given story's reposts.
 */
+ (UAActivityStoryListRef *)activityStoryListRefToRepostsForStory:(UAActivityStoryRef *)reference offset:(int)offset limit:(int)limit;

+ (UAActivityStoryListRef *)activityStoryListRefForWorkout:(UAWorkoutRef *)reference offset:(NSInteger)offset limit:(NSInteger)limit;

@end
