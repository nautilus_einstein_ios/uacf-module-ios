/*
 * UAPage_Internal.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#ifndef UA_UAPage_Internal_h
#define UA_UAPage_Internal_h

@interface UAPage()

@property (nonatomic, copy) NSString *alias;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *pageDescription;
@property (nonatomic, copy) NSURL *url;
@property (nonatomic, copy) NSURL *website;
@property (nonatomic, copy) NSString *headline;
@property (nonatomic, strong) UAImage *profilePhoto;
@property (nonatomic, strong) UAImage *coverPhoto;
@property (nonatomic, strong) UALocation *location;
@property (nonatomic, strong) UAUserRef *userRef;
@property (nonatomic, strong) UAPageTypeRef *pageTypeReference;
@property (nonatomic, strong) UAWorkoutListRef *workoutsReference;
@property (nonatomic, strong) UAActigraphyListRef *actigraphyReference;
@property (nonatomic, strong) UAPageAssociationListRef *toAssocationReference;
@property (nonatomic, strong) UAPageAssociationListRef *fromAssocationReference;
@property (nonatomic, strong) UAFollowerListRef *followerReference;
@property (nonatomic, strong) UAFollowerListRef *followingReference;
@property (nonatomic, strong) UAPageFollowRef *followReference;
@property (nonatomic, strong) UAPageFollowRef *unfollowReference;
@property (nonatomic, strong) UAActivityStoryListRef *activityStoryReference;
@property (nonatomic, strong) UAUserAchievementListRef *userAchievementReference;
@property (nonatomic, strong) UAActivityStoryListRef *featuredActivityFeedReference;

@property (nonatomic, assign) NSUInteger followerCount;
@property (nonatomic, assign) NSUInteger followingCount;
@property (nonatomic, assign) NSUInteger fromAssociationCount;
@property (nonatomic, assign) NSUInteger toAssociationCount;

@property (nonatomic, strong) UAPageSettings *settings;

@end

#endif
