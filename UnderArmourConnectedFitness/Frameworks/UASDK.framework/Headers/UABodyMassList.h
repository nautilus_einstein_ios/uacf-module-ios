/*
 * UABodyMassList.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UAEntityList.h>
#import <UASDK/UAEntityListRef.h>

@interface UABodyMassListRef : UAEntityListRef <NSCoding>

+ (UABodyMassListRef *)bodyMassListRefWithStartDate:(NSDate *)start
                                            endData:(NSDate *)end
                                              limit:(NSUInteger)limit;

+ (UABodyMassListRef *)bodyMassListRefWithStartDate:(NSDate *)start
                                            endData:(NSDate *)end;

@end

@interface UABodyMassList : UAEntityList <NSCoding, UAEntityListRefInterface>

/**
 *  Reference to this object.
 */
@property (nonatomic, strong) UABodyMassListRef *ref;

/**
 *  Reference to the next paged collection. This will be nil if there is no next paged collection.
 */
@property (nonatomic, strong) UABodyMassListRef *nextRef;

/**
 *  Reference to the previous paged collection. This will be nil if there is no previous paged collection.
 */
@property (nonatomic, strong) UABodyMassListRef *previousRef;

@end
