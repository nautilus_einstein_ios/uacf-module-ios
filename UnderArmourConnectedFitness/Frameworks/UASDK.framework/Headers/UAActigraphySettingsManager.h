/*
 * UAActigraphySettingsManager.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UAManager.h>

@class UAActigraphySettings;

typedef void (^UAActigraphySettingsResponseBlock)(UAActigraphySettings *object, NSError *error);

/**
 *  Protocol used to allow us to make a composite manager
 */
@protocol UAActigraphySettingsManagerInterface <NSObject>

/**
 *  Fetchs the actigraphy settings for the current user. 
 *
 *  @param response A block called on completion.
 */
- (void)fetchActigraphySettings:(UAActigraphySettingsResponseBlock)response;

/**
 *  Sets the sleep recorder type to use for sleep in the actigraphy data. This 
 *  should be one of the strings returned in the sleepPriority array in the UAActigraphySettings
 *  object.
 *
 *  @param recorderTypeKey The name of the sleep recorder.
 *  @param response        A block called on completion.
 */
- (void)createSleepPriority:(NSString *)recorderTypeKey response:(UAResponseBlock)response;

/**
 *  Sets the activity (steps) recorder type to use for steps in the actigraphy data. This
 *  should be one of the strings returned in the activityPriority array in the UAActigraphySettings
 *  object.
 *
 *  @param recorderTypeKey The name of the activity recorder.
 *  @param response        A block called on completion.
 */
- (void)createActivityPriority:(NSString *)recorderTypeKey response:(UAResponseBlock)response;

@end

@interface UAActigraphySettingsManager : UAManager <UAActigraphySettingsManagerInterface>

@end
