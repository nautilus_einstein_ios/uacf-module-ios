/*
 * UAEnergyExpendedDerivedDataSourceConfiguration.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import "UADerivedDataSourceConfiguration.h"

@interface UAEnergyExpendedDerivedDataSourceConfiguration : UADerivedDataSourceConfiguration <UADataSourceConfigurationInterface>

@end
