/*
 * UALocation.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */



@interface UALocation : NSObject <NSCoding>

/**
 The location's country. Refer to ISO_3166-1 Alpha 2 (http://en.wikipedia.org/wiki/ISO_3166-1_alpha-2)
 */
@property (nonatomic, copy) NSString *country;

/**
 The location's region (represents state in US, province in Canada). Refer to ISO_3166_2 (http://en.wikipedia.org/wiki/ISO_3166-2).
 */
@property (nonatomic, copy) NSString *region;

/**
 The location's locality (typically represents city).
 */
@property (nonatomic, copy) NSString *locality;

@end
