//
//  UABodyMassDataPoint.h
//  UASDK
//
//  Created by Morrow, Andrew on 11/20/14.
//  Copyright (c) 2014 Under Armour. All rights reserved.
//

#import <UASDK/UADataPoint.h>

@interface UABodyMassDataPoint : UADataPoint

/**
 *  Total body mass (double, kilograms).
 *  @discussion Lean and fat body mass should sum to total body mass.
 */
@property (nonatomic, readonly) NSNumber *mass;

/**
 *  Lean body mass (double, kilograms).
 *  @discussion Lean and fat body mass should sum to total body mass.
 */
@property (nonatomic, readonly) NSNumber *leanMass;

/**
 *  Fat body mass (double, kilograms).
 *  @discussion Lean and fat body mass should sum to total body mass.
 */
@property (nonatomic, readonly) NSNumber *fatMass;

/**
 *  Instantiate an body mass data point.
 *  @param mass Total body mass in kilograms. Must not be nil.
 *  @param leanass Total body lean mass in kilograms. Must not be nil.
 *  @param fatMass Total body fat mass in kilograms. Must not be nil.
 *  @return An instantiated data point.
 */
- (instancetype)initWithMass:(NSNumber *)mass leanMass:(NSNumber *)leanMass fatMass:(NSNumber *)fatMass NS_DESIGNATED_INITIALIZER;

@end
