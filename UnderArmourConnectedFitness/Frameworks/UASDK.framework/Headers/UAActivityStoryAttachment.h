//
//  UAActivityStoryAttachment.h
//  UA
//
//  Created by Jeff Oliver on 6/26/14.
//  Copyright (c) 2014 MapMyFitness Inc. All rights reserved.
//


typedef NS_ENUM(NSInteger, UAActivityStoryAttachmentType)
{
    UAActivityStoryAttachmentTypeUnknown,
	UAActivityStoryAttachmentTypePhoto,
    UAActivityStoryAttachmentTypeVideo
};

typedef NS_ENUM(NSInteger, UAActivityStoryAttachmentStatus)
{
    UAActivityStoryAttachmentStatusUnkown,
    UAActivityStoryAttachmentStatusPending,
    UAActivityStoryAttachmentStatusProcessing,
    UAActivityStoryAttachmentStatusReady
};

@interface UAActivityStoryAttachment : NSObject

@property (nonatomic, readonly) UAActivityStoryAttachmentType type;
@property (nonatomic, assign) UAActivityStoryAttachmentStatus status;

@property (nonatomic, strong) NSDate *published;
@property (nonatomic, strong) NSURL *url;

@end
