/*
 * UAActivityTypeMapper.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <Foundation/Foundation.h>

@class UAActivityType, UAActivityTypeList;

@interface UAActivityTypeMapper : NSObject

+ (UAActivityType *)activityTypeFromResponse:(NSDictionary *)response;

+ (UAActivityTypeList *)activityTypeListFromResponse:(NSDictionary *)response;

@end
