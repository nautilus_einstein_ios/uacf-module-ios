//
//  UAActivityStoryRouteObject.h
//  UA
//
//  Created by Jeremy Zedell on 5/29/14.
//  Copyright (c) 2014 MapMyFitness Inc. All rights reserved.
//

#import <UASDK/UAActivityStoryObject.h>
#import <UASDK/UALocation.h>
#import <UASDK/UARoute.h>
#import <UASDK/UAPrivacyRef.h>


@interface UAActivityStoryRouteObject : UAActivityStoryObject <NSCoding>

@property (nonatomic, strong) UARouteRef *ref;

@property (nonatomic, strong) UAPrivacyRef *privacyRef;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, assign) double distance;

/**
 Highlights are the top three fields for a workout or route object,
 chosen from a pre-determined list by activity type or object type.
 An object can have between 0 and 3 highlights, and only fields that
 are present and non-zero will be highlighted.
 
 Distance, duration and pace/speed highlights have an optional percentile callout.
 Percentiles include: Top 10%, Top 25%. Thresholds are dependent on gender and
 only activity types with a base type of run, bike, or walk can have callouts.
 
 @return NSArray of UAActivityStoryHighlight
 */
@property (nonatomic, strong) NSArray *highlights;

@property (nonatomic, assign) UAPrivacyType privacy;

@property (nonatomic, strong) UALocation *location;

@end
