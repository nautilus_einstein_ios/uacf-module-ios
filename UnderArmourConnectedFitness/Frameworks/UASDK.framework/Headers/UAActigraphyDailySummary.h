/*
 * UAActigraphyDailySummary.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <Foundation/Foundation.h>
#import <UASDK/UAEntity.h>

@class UAActigraphyDataAggregates, UAActigraphyData, UADateRange;

@interface UAActigraphyAggregates : NSObject

/**
 User's active time aggregates for the day in seconds
 */
@property (nonatomic, strong) UAActigraphyDataAggregates *activeTime;

/**
 User's energy burned aggregates for the day in joules
 */
@property (nonatomic, strong) UAActigraphyDataAggregates *energyBurned;

/**
 User's distance aggregates for the day in meters
 */
@property (nonatomic, strong) UAActigraphyDataAggregates *distance;

/**
 User's step aggregates for the day in steps
 */
@property (nonatomic, strong) UAActigraphyDataAggregates *steps;

/**
 User's sleep aggregates for the day in seconds
 */
@property (nonatomic, strong) UAActigraphyDataAggregates *sleep;

/**
 User's weight aggregates for the day in lbs
 */
@property (nonatomic, strong) UAActigraphyDataAggregates *bodyMass;

@end

@interface UAActigraphyDailySummary : UAEntity

/**
 Start and end date for the day represented by this object
 */
@property (nonatomic, strong) UADateRange *dateRange;

/**
 Timezone the daily summary was recorded in
 */
@property (nonatomic, strong) NSTimeZone *timeZone;

/**
 Representation of user's energy burned for the day
 */
@property (nonatomic, strong) UAActigraphyData *energyBurnedData;

/**
 Representation of user's distance data for the day
 */
@property (nonatomic, strong) UAActigraphyData *distanceData;

/**
 Representation of user's sleep data for the day
 @discussion This is an array of UAActigraphyData objects. There will be multiple UAActigraphyData
 objects if the user recorded multiple periods of sleep in the given date range.
 */
@property (nonatomic, strong) NSArray *sleepData;

/**
 Representation of user's step data for the day
 */
@property (nonatomic, strong) UAActigraphyData *stepData;

/**
 Representation of user's weight data for the day
 */
@property (nonatomic, strong) UAActigraphyData *weightData;

/**
 Daily aggregates for all available metrics.
 @discussion These aggregates may differ from the aggregates included in each UAActigraphyData object
 due to some possible overlap between device and workout data.
 */
@property (nonatomic, strong) UAActigraphyAggregates *aggregates;

/**
 Array of UAActigraphyData objects recorded during this day
 */
@property (nonatomic, strong) NSArray *workouts;

@end
