/*
 * UATimeSeriesPower.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UATimeSeries.h>

@class UATimeSeriesPowerPoint;

@interface UATimeSeriesPower : UATimeSeries <UATimeSeriesProtocol>

- (void)addPoint:(UATimeSeriesPowerPoint *)point;

- (UATimeSeriesPowerPoint *)pointAtIndex:(NSUInteger)index;

@end
