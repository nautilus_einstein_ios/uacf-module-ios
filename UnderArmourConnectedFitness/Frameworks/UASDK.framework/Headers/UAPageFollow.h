/*
 * UAPageFollow.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAEntity.h>
#import <UASDK/UAPageFollowRef.h>

@class UAUserRef, UAPageRef;

/**
 *  An object that has references to a user that is following a page.
 */
@interface UAPageFollow : UAEntity <UAEntityInterface>

/**
 *  The reference for this object.
 */
@property (nonatomic, strong) UAPageFollowRef *ref;

/**
 *  The user reference that is following the page.
 */
@property (nonatomic, strong) UAUserRef *userRef;

/**
 *  The page reference that is being followed.
 */
@property (nonatomic, strong) UAPageRef *pageRef;

@end
