//
//  UAActivityStory.h
//  UA
//
//  Created by Jeremy Zedell on 5/29/14.
//  Copyright (c) 2014 MapMyFitness Inc. All rights reserved.
//

#import <UASDK/UAEntity.h>
#import <UASDK/UAActivityStoryRef.h>
#import <UASDK/UAActivityStoryObject.h>
#import <UASDK/UAActivityStoryActor.h>
#import <UASDK/UAActivityStoryTemplate.h>
#import <UASDK/UAActivityStoryList.h>
#import <UASDK/UAActivityStoryLikeList.h>
#import <UASDK/UAActivityStoryCommentList.h>
#import <UASDK/UAActivityStoryRepostList.h>
#import <UASDK/UAActivityStorySource.h>
#import <UASDK/UAImage.h>

typedef NS_ENUM(NSUInteger, UAActivityStoryVerbType)
{
    UAActivityStoryVerbTypeUnknown,
    UAActivityStoryVerbTypeComment,
    UAActivityStoryVerbTypeCreate,
	UAActivityStoryVerbTypeDo,
    UAActivityStoryVerbTypeFriend,
    UAActivityStoryVerbTypeLike,
	UAActivityStoryVerbTypePost,
	UAActivityStoryVerbTypeTrack,
	UAActivityStoryVerbTypeRepost,
    UAActivityStoryVerbTypeInvite,
    UAActivityStoryVerbTypeUpdate,
    UAActivityStoryVerbTypeComplete
};


@interface UAActivityStory : UAEntity <UAEntityInterface>

/**
 * Unparsed Activity Story data structure created from API's JSON definition of a story.
 *
 * @discussion Used in conjunction with UAActivityStoryTemplate.
 */
@property (nonatomic, strong) NSDictionary *rawStory;

@property (nonatomic, strong) UAActivityStoryRef *ref;

@property (nonatomic, strong) UAActivityStoryRef *target;

@property (nonatomic, strong) UAActivityStoryActor *actor;

@property (nonatomic, strong) UAActivityStoryObject *object;

@property (nonatomic, strong) UAActivityStorySource *source;

@property (nonatomic, assign) UAActivityStoryVerbType verb;

@property (nonatomic, strong) UAActivityStoryTemplate *template;

@property (nonatomic, copy) NSString *type;

@property (nonatomic, strong) NSDate *published;

@property (nonatomic, strong) UAActivityStoryCommentList *commentsList;

@property (nonatomic, strong) UAActivityStoryLikeList *likesList;

@property (nonatomic, strong) UAActivityStoryRepostList *repostList;

@property (nonatomic, strong) UAImage *image;

/**
 * NSArray of UAActivityStoryAttachment
 */
@property (nonatomic, strong) NSArray *attachments;

@end
