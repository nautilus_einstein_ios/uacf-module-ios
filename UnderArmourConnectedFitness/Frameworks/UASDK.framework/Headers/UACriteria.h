/*
 * UACriteria.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <Foundation/Foundation.h>

@class UACriteriaItem;

@interface UACriteria : NSObject <NSCoding>

- (UACriteriaItem *)getCriteriaByKey:(NSString *)key;
- (void)addCriteriaItem:(UACriteriaItem *)item;

@end
