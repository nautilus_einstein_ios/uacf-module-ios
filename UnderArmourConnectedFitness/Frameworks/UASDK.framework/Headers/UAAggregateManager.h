/*
 * UAAggregateManager.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAManager.h>

@class UAAggregateList, UAAggregateListRef;

typedef void (^UAAggregateListResponseBlock)(UAAggregateList *object, NSError *error);

@interface UAAggregateManager : UAManager

- (void)fetchAggregatesWithListRef:(UAAggregateListRef *)reference response:(UAAggregateListResponseBlock)response;

@end
