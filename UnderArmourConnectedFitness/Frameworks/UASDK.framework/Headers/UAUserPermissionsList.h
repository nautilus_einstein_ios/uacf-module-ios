/*
 * UAUserPermissionsList.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAEntityList.h>
#import <UASDK/UAEntityListRef.h>

@interface UAUserPermissionsListRef : UAEntityListRef

@end

@interface UAUserPermissionsList : UAEntityList

@property (nonatomic, strong) UAUserPermissionsListRef *previousRef;

@property (nonatomic, strong) UAUserPermissionsListRef *nextRef;

@end
