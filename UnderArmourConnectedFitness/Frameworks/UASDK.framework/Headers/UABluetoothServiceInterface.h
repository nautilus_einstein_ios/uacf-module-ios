/*
 * UABluetoothServiceInterface.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#ifndef UASDK_UABluetoothServiceInterface_h
#define UASDK_UABluetoothServiceInterface_h

#import <CoreBluetooth/CoreBluetooth.h>

@class UARecorderContext;

/**
 *  @protocol UABluetoothServiceInterface
 *  An object implementing this interface is responsible for service-/protocol-specific
 *  message handling. A {@link UABluetoothClient} object will connect to a BTLE device
 *  and instantiate one service object for each BTLE service the device supports. The
 *  service object is responsible for discovering services and characteristics, setting
 *  characteristic notifications, writing configuration values to characteristics,
 *  deserializing values read from characteristics, etc.
 */
@protocol UABluetoothServiceInterface <CBPeripheralDelegate>

@required

/**
 *  The UUID of the service the class implements.
 *  @return A full UUID.
 */
+ (CBUUID *)serviceUUID;

/**
 *  The object's delegate (usually a {@link UABluetoothClient}). The delegate should
 *  implement the {@link UABluetoothServiceDelegate} protocol (or a derivative).
 */
@property (nonatomic, weak) id delegate;

/**
 *  Configures the receiver using information from the recorder context.
 *  @param context The current recorder context.
 *  @discussion This method can be called more than once.
 */
- (void)configureWithRecorderContext:(UARecorderContext *)context;

/**
 *  This method is called the first time a device connects. You should use this method
 *  to discover services or write configuration values etc.
 */
- (void)peripheralConnected:(CBPeripheral *)peripheral;

/**
 *  This method is called at the beginning of a workout segment. It may or may not be
 *  balanced by a call to {@link stopSegment}. You should use this method to command
 *  the BTLE device to reset/start accumulators.
 */
- (void)startSegment;

/**
 *  This method is called at the end of a workout segment. It may or may not be
 *  balanced by a call to {@link startSegment}. You should use this method to stop
 *  any accumulators and read any summary values.
 */
- (void)stopSegment;

@end

@protocol UABluetoothServiceDelegate <NSObject>

/**
 *  This method is called when thet service encounters an error and is unable to
 *  continue interacting with the device.
 *  @param service The service which experienced the failure.
 *  @param error The error encountered. Should not be nil.
 */
- (void)service:(id<UABluetoothServiceInterface>)service didFail:(NSError *)error;

@end

#endif
