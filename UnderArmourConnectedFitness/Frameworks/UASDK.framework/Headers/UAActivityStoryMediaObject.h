//
//  UAActivityStoryMediaObject.h
//  UA
//
//  Created by Jeremy Zedell on 5/29/14.
//  Copyright (c) 2014 MapMyFitness Inc. All rights reserved.
//

#import "UAActivityStoryObject.h"

@interface UAActivityStoryMediaObject : UAActivityStoryObject <NSCoding>

@end
