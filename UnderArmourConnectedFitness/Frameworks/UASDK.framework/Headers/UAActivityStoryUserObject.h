//
//  UAActivityStoryUserObject.h
//  UA
//
//  Created by Jeremy Zedell on 5/29/14.
//  Copyright (c) 2014 MapMyFitness Inc. All rights reserved.
//

@import Foundation;

#import <UASDK/UAActivityStoryObject.h>
#import <UASDK/UAUser.h>
#import <UASDK/UALocation.h>
#import <UASDK/UAEnumerations.h>

@class UAFriendshipSummary;

@interface UAActivityStoryUserObject : UAActivityStoryObject <NSCoding>

@property (nonatomic, strong) UAUserRef *ref;

@property (nonatomic, copy) NSString *firstName;

@property (nonatomic, copy) NSString *lastName;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, assign) UAGender gender;

@property (nonatomic, strong) UAFriendshipSummary *friendship;

@property (nonatomic, assign) UAPrivacyType privacy;

@property (nonatomic, strong) UALocation *location;

@end
