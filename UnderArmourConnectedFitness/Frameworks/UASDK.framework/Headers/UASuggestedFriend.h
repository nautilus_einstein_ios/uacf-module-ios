/*
 * UASuggestedFriend.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


@class UAUserRef, UAUserProfilePhotoRef, UASuggestedFriendReason, UAMutualFriendsRef;

@interface UASuggestedFriend : NSObject

/**
 *  The reference identifier for this friend.
 */
@property (nonatomic, strong) UAUserRef *ref;


@property (nonatomic, strong) UAMutualFriendsRef *mutualFriendsReference;

/**
 *  An image reference for this friend.
 */
@property (nonatomic, strong) UAUserProfilePhotoRef *imageReference;

/**
 Combined first name and last name or first name and last initial.
 @discussion If a user's profile is private and the authenticated
 user is not a friend, this will be set to first name and last initial.
 If the user's profile is public or the authenticated user is a friend,
 this will be set to first name and last name
 */
@property (nonatomic, copy) NSString *displayName;

/**
 List of reasons why this friend has been suggested.
 @discussion This is an array of UASuggestedFriendReason objects
 */
@property (nonatomic, strong) NSArray *reasons;

@end
