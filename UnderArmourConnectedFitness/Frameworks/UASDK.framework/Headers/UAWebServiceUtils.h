/*
 * UAWebServiceUtils.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, UAImageSize)
{
	UAImageSizeSmall,
	UAImageSizeMedium,
	UAImageSizeLarge
};

@class UAEntityListRef, UAWorkoutRef, UAUserRef, UARouteRef;
@class UAFriendshipSummaryRef, UAActivityTypeReference, UARouteBookmarkRef;
@class UAActivityRef, UADeviceRef, UAPrivacyRef, UAAchievementRef;
@class UACourseRef, UARouteListRef, UAPageRef, UAPageAssociationRef;
@class UAPageFollowRef, UAPageTypeRef, UAActivityStoryRef;
@class UAEntityRef;

@interface UAWebServiceUtils : NSObject

#pragma mark - User Path Methods

+ (NSString *)getAuthenticatedUserPath;

+ (NSString *)updateUserPathWithReference:(UAUserRef *)userRef;

+ (NSString *)getUserPathWithReference:(UAUserRef *)userRef;

+ (NSString *)getUserPath;

+ (NSString *)getUserPathForParams:(NSDictionary *)params;

#pragma mark - User Stats Methods

+ (NSString *)getUserStatsPathWithUserRef:(UAUserRef*)userRef params:(NSDictionary *)params;

#pragma mark - User Profile Photo Methods

+ (NSString *)getUserProfilePhotoPathWithReference:(UAUserRef *)userRef;

+ (NSString*)getUserProfilePhotoPathWithUserID:(NSString*)userID size:(UAImageSize)size;

#pragma mark - Privacy Path Methods

+ (NSString *)getPrivacyOptionsPath;

+ (NSString *)getPrivacyOptionWithReference:(UAPrivacyRef *)privacyRef;

#pragma mark - Workout Path Methods

/**
 *  Creates a path to obtain a list of workouts by collection reference.
 *
 *  @param listRef A collection reference to a list of workouts.
 *
 *  @return A path to fetch a list of workouts.
 */
+ (NSString *)getWorkoutsPathWithListRef:(UAEntityListRef *)listRef;

/**
 *  Creates a path to obtain a workout by workout reference.
 *
 *  @param workoutReference A reference to a workout.
 *
 *  @return A path to a workout.
 *
 *  @discussion The href of the UAWorkoutRef will be used if it is available.
 *  Otherwise, it will resort to using the entityID.
 */
+ (NSString *)getWorkoutPathWithReference:(UAWorkoutRef *)workoutReference;

+ (NSString *)getAddWorkoutPath;

+ (NSString *)getWorkoutPath;

#pragma mark - Route Path Methods

+ (NSString *)createRoutePath;

+ (NSString *)getNearbyRoutesPath;

+ (NSString *)getNearbyRoutesPathWithListRef:(UARouteListRef *)listRef;

+ (NSString *)deleteRoutePathWithReference:(UARouteRef *)routeRef;

+ (NSString *)getRouteBookmarkPath;

+ (NSString *)deleteRouteBookmarkPathWithReference:(UARouteBookmarkRef *)routeBookmarkReference;

+ (NSString *)getBookmarkedRoutesForUserPathWithReference:(UAUserRef *)userRef;

+ (NSString *)getRoutePathWithReference:(UARouteRef *)routeRef;

+ (NSString *)getRoutesForUserPathWithReference:(UAUserRef *)userRef;

+ (NSString *)updateRoutePathWithReference:(UARouteRef *)routeRef;

#pragma mark - Achievement Path Methods

+ (NSString *)getAchievementPathWithReference:(UAAchievementRef *)achievementReference;

+ (NSString *)getUserAchievementsPath;

#pragma mark - Activity Type Path Methods

+ (NSString *)getActivityTypePathWithReference:(UAActivityTypeReference *)activityTypeRef;

+ (NSString *)getActivityTypesPath;

#pragma mark - User Stats Path Method

+ (NSString *)getUserStatsWithReference:(UAUserRef *)userRef;

#pragma mark - Friendship Path Methods

+ (NSString *)getSuggestedFriendsPath;

+ (NSString *)getPendingFriendRequestsPath;

+ (NSString *)approvePendingFriendRequestPathWithReference:(UAFriendshipSummaryRef *)friendshipSummaryReference;

+ (NSString *)deleteFriendshipWithReference:(UAFriendshipSummaryRef *)friendshipSummaryReference;

+ (NSString *)getFriendsPath;

+ (NSString *)getFriendshipPath;

#pragma mark - Actigraphy Path Methods

+ (NSString *)getActigraphySummaryPath;

#pragma mark - Activity Path Methods

+ (NSString *)createActivityPath;

+ (NSString *)deleteActivityPathWithReference:(UAActivityRef *)activityReference;

+ (NSString *)getActivityPath;

#pragma mark - Aggregate Path methods 

+ (NSString *)getAggregatePath;

#pragma mark - Sleep Methods

+ (NSString *)getSleepPath;

+ (NSString *)createSleepPath;

#pragma mark - BodyMass Methods

+ (NSString *)getBodyMassPath;

#pragma mark - Device Path Methods

+ (NSString *)getDeviceListingPath;

+ (NSString *)getDeviceConnectionsPath;

+ (NSString *)getDevicePreferencesPathWithReference:(UAUserRef *)userRef;

+ (NSString *)createDevicePreferencesPath;

#pragma mark - Heart Rate Zones Methods

+ (NSString *)getHeartRateZonesPath;
+ (NSString *)getHeartRateZoneCalculationPath;

#pragma mark - URL Methods

+ (NSString *)urlParameterEncodedStringForDictionary:(NSDictionary *)dictionary;

#pragma mark - Group methods

+ (NSString *)getGroupLeaderboardPath;

#pragma mark - JSON

+ (NSString *)jsonStringFromDictionary:(NSDictionary*)dictionary;

#pragma mark - Image Methods

+ (NSString *)getImagePath;

#pragma mark - Group Methods

+ (NSString *)getGroupPath;

+ (NSString *)getGroupUserPath;

+ (NSString *)getGroupPurposePath;

+ (NSString *)getGroupInvitePath;

#pragma mark - Gear Methods

+ (NSString*)userGearPathWithUserGearID:(NSString*)userGearID;

#pragma mark - Page Methods

+ (NSString *)getPagePath;

+ (NSString *)getPagePathWithReference:(UAPageRef *)reference;

+ (NSString *)getPageFollowPath;

+ (NSString *)getPageFollowPathWithPageReference:(UAPageRef *)reference;

+ (NSString *)getPageFollowPathWithUserReference:(UAUserRef *)reference;

+ (NSString *)getPageFollowPathWithPageReference:(UAPageRef *)pageReference andUserReference:(UAUserRef *)userRef;

+ (NSString *)getPageTypesPath;

+ (NSString *)getPageTypePathWithPageTypeReference:(UAPageTypeRef *)reference;

+ (NSString *)getPageAssociationsPath;

/** @return A path to an endpoint for page associations that are associated via a FROM relationship with the given page reference */
+ (NSString *)getPageAssociationsPathFromPageWithPageReference:(UAPageRef *)reference;

/** @return A path to an endpoint for page associations that are associated via a TO relationship with the given page reference */
+ (NSString *)getPageAssociationsPathToPageWithPageReference:(UAPageRef *)reference;

#pragma mark - Activity Story Methods

+ (NSString *)getActivityStoryPath;

+ (NSString *)getActivityStoryPathWithStoryID:(NSString*)storyID;

+ (NSString *)getActivityStoryPathWithPageID:(NSString*)pageID;

+ (NSString *)getActivityStoryPrivacyUpdatePathWithWorkoutReference:(UAWorkoutRef *)reference;

+ (NSString *)getActivityStoryPrivacyUpdatePathWithRouteReference:(UARouteRef *)reference;

+ (NSString *)getActivityStoryCommentsPathWithStoryReference:(UAActivityStoryRef *)reference offset:(int)offset limit:(int)limit;

+ (NSString *)getActivityStoryLikesPathWithStoryReference:(UAActivityStoryRef *)reference offset:(int)offset limit:(int)limit;

+ (NSString *)getActivityStoryRepostsPathWithStoryReference:(UAActivityStoryRef *)reference offest:(int)offset limit:(int)limit;

+ (NSString *)getActivityStoryWorkoutPathWithWorkoutReference:(UAWorkoutRef *)reference offset:(NSInteger)offset limit:(NSInteger)limit;

#pragma mark - Video Methods

+ (NSString *)getFilemobileUploadTokenPath;

@end
