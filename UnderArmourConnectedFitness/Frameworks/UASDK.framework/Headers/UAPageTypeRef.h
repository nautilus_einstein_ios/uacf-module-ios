/*
 * UAPageTypeRef.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAEntityRef.h>

@interface UAPageTypeRef : UAEntityRef

@property (nonatomic, readonly) NSString *pageType;

@end
