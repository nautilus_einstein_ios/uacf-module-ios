/*
 * UASleepData.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UAEntity.h>
#import <UASDK/UAEntityRef.h>
#import <UASDK/UATimeSeries.h>

extern NSString * const kUAManualSleepCreationRecorderType;

@class UASleepDataAggregate, UASleepDataTimeSeries, UAUserRef;

@interface UASleepDataReference : UAEntityRef

@end

@interface UASleepData : UAEntity <UAEntityInterface, NSCoding>

/**
 *  The reference of this sleep object.
 */
@property (nonatomic, strong) UASleepDataReference *ref;

/**
 *  The reference of the user associated with this sleep data
 */
@property (nonatomic, strong) UAUserRef *userRef;

/**
 * The date at which this sleep started.
 */
@property (nonatomic, strong) NSDate *startDateTime;

/**
 * The date at which this sleep ended.
 */
@property (nonatomic, strong) NSDate *endDateTime;

/**
 * The timezone at which this sleep occurred in.
 */
@property (nonatomic, strong) NSTimeZone *timezone;

/**
 *  Timeseries for the sleep
 */
@property (nonatomic, strong) UASleepDataTimeSeries *timeSeries;

/**
 *  Aggregate data for the sleep based off it's timeseries. 
 *
 *  When creating a sleep activity with timeseries, if this is not set, the
 *  aggregate will be calculated based upon the data in the time series.
 */
@property (nonatomic, strong) UASleepDataAggregate *aggregate;

/**
 * A string that represents what kind of device was used to record the sleep.
 * @discussion when manually logging sleep, set to kUAManualSleepCreationRecorderType
 */
@property (nonatomic, copy) NSString *recorderTypeKey;

/**
 *  A string that represents a unique identifier for this recording object. This is generated automatically when saving.
 */
@property (nonatomic, copy) NSString *referenceKey;

/**
 *  A string that represents a unique identifier for this recording object. This is generated automatically when saving.
 */
@property (nonatomic, assign) BOOL hasTimeseries;


- (void)calculateAggregateData;

@end


/**
 *  Represents aggregate data for this sleep
 */
@interface UASleepDataAggregate : NSObject <NSCoding>

/**
 *  The total awake time in time series
 */
@property (nonatomic, assign) NSTimeInterval awakeTimeInS;

/**
 *  The total light sleep in the time series
 */
@property (nonatomic, assign) NSTimeInterval lightSleepTimeInS;

/**
 *  The total deep sleep in the time series
 */
@property (nonatomic, assign) NSTimeInterval deepSleepTimeInS;

/**
 *  The number of times the user was awoken
 */
@property (nonatomic, assign) NSUInteger timesAwakened;

/**
 *  The time before the initial sleep (deep or light) data point in the time series.
 */
@property (nonatomic, assign) NSTimeInterval timeToSleep;

@end

typedef enum {
    UASleepData_Invalid = 0,
    UASleepData_Awake = 1,
    UASleepData_LightSleep = 2,
    UASleepData_DeepSleep = 3,
} UASleepTimeSeriesSleepType;

@interface UASleepDataTimeSeriesPoint : NSObject <NSCoding>

+ (instancetype)sleepDataTimeseriesPointWithSleepType:(UASleepTimeSeriesSleepType)sleepType
                                               atTime:(NSTimeInterval)timestamp;

/**
 *  This is the epoch value since 1970 in seconds.
 */
@property (nonatomic, readonly) NSTimeInterval timestamp;

/**
 *  The state of the user at this time point
 */
@property (nonatomic, readonly) UASleepTimeSeriesSleepType sleepType;

@end

@interface UASleepDataTimeSeries : NSObject <NSCoding>

/**
 *  The points that are assocated with this timeseries. These should be UASleepDataTimeSeriesPoint objects.
 */
@property (nonatomic, strong) NSArray *sleepTimeSeriesPoints;

@end
