//
//  UADistanceDataPoint.h
//  UASDK
//
//  Created by Morrow, Andrew on 11/20/14.
//  Copyright (c) 2014 Under Armour. All rights reserved.
//

#import <UASDK/UADataPoint.h>

@interface UADistanceDataPoint : UADataPoint

/**
 *  Distance (double, meters).
 */
@property (nonatomic, readonly) NSNumber *distance;

/**
 *  Instantiates a distance data point.
 *  @param distance The distance in meters. Must not be nil.
 *  @return A newly instantiated distance data point.
 */
- (instancetype)initWithDistance:(NSNumber *)distance NS_DESIGNATED_INITIALIZER;

@end
