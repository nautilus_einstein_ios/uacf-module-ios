/*
 * UABluetoothA39WorkoutService.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <Foundation/Foundation.h>
#import "UABluetoothServiceInterface.h"
#import <UASDK/UAEnumerations.h>

@protocol UABluetoothA39WorkoutServiceDelegate;

@interface UABluetoothA39WorkoutService : NSObject <UABluetoothServiceInterface>

@property (nonatomic, weak) id<UABluetoothA39WorkoutServiceDelegate> delegate;

/**
 *  User gender.
 *
 *  @discussion Used by the A39 module in calculating calories expended and WILLpower. Written to the A39 when {@link startSegment} is called.
 */
@property (nonatomic) UAGender userGender;

/**
 *  User weight in kilograms.
 *
 *  @discussion Used by the A39 module in calculating calories expended and WILLpower. Written to the A39 when {@link startSegment} is called.
 */
@property (nonatomic) NSUInteger userWeight;

/**
 *  User age in years.
 *
 *  @discussion Used by the A39 module in calculating calories expended and WILLpower. Written to the A39 when {@link startSegment} is called.
 */
@property (nonatomic) NSUInteger userAge;

/**
 *  Maximum heart rate in beats per minute.
 *
 *  @discussion Used by the A39 module in calculating calories expended and WILLpower. Written to the A39 when {@link startSegment} is called.
 */
@property (nonatomic) NSUInteger userMaximumHeartRate;

@end

@protocol UABluetoothA39WorkoutServiceDelegate <UABluetoothServiceDelegate>

/**
 *  Called when the A39 module updates workout data (every five seconds).
 *  Values are reset on the device when {@link startSegment} is called.
 *
 *  @param service The service which received the measurement.
 *  @param stepCount Number of steps since the workout began.
 *  @param caloriesExpended Number of calories expended since the workout began.
 *  @param willPower Calculated WILLPOWER since the workout began.
 *  @param measuredPosture Measured posture.
 *  @param strideRate Stride rate.
 */
- (void)service:(UABluetoothA39WorkoutService *)service didUpdateWorkoutDataWithStepCount:(uint16_t)stepCount caloriesExpended:(uint16_t)caloriesExpended willPower:(double)willPower measuredPosture:(uint8_t)posture strideRate:(uint8_t)strideRate;

@end
