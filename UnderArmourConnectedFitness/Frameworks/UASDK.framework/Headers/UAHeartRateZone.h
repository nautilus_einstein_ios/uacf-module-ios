/*
 * UAHeartRateZone.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <Foundation/Foundation.h>

@interface UAHeartRateZone : NSObject <NSCoding>

- (instancetype)initWithName:(NSString *)name start:(NSInteger)start end:(NSInteger)end;

@property (nonatomic, readonly) NSString *name;
@property (nonatomic, readonly) NSInteger start;
@property (nonatomic, readonly) NSInteger end;

@end
