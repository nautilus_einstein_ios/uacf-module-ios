/*
 * UARouteList.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


@import CoreLocation;

#import <UASDK/UAEntityList.h>
#import <UASDK/UAEntityListRef.h>

@class UAUserRef;

@interface UARouteListRef : UAEntityListRef

+ (UARouteListRef *)routeListRefWithMinimumDistance:(NSUInteger)minimumDistance
                               maximumDistance:(NSUInteger)maximumDistance
                                sourceLocation:(CLLocationCoordinate2D)sourceLocation;

+ (UARouteListRef *)routeListRefWithUserRef:(UAUserRef *)reference;

@end

@interface UARouteList : UAEntityList <UAEntityListRefInterface>

@property (nonatomic, strong) UARouteListRef *ref;

/**
 *  A reference to the previous collection of routes.
 *  @discussion If no collection has previously been visited, this value will be nil.
 */
@property (nonatomic, strong) UARouteListRef *previousRef;

/**
 *  A reference to the next collection of routes.
 *  @discussion If no more routes exist, this value will be nil.
 */
@property (nonatomic, strong) UARouteListRef *nextRef;

@end
