/*
 * UAActivityTimeSeriesMapper.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <Foundation/Foundation.h>

@class UAActivityTimeSeries;

@interface UAActivityTimeSeriesMapper : NSObject

/**
 * Takes an NSDictionary with activity data and creates an UAActivityTimeSeries object.
 * @param output an NSDictionary with corresponding activity data.
 * @return an UAActivityTimeSeries object with the parsed data.
 */
+ (UAActivityTimeSeries *)UAActivityFromDictionary:(NSDictionary *)output;

/**
 * Takes an UAActivityTimeSeries object and transforms it into an NSDictionary object.
 * @param activity an UAActivityTimeSeries object.
 * @return an NSDictionary representation of the UAActivityTimeSeries object.
 */
+ (NSDictionary *)dictionaryFromUAActivity:(UAActivityTimeSeries *)activity;

@end
