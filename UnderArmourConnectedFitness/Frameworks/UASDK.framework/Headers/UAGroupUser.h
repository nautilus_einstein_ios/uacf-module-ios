/*
 * UAGroupUser.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAEntity.h>
#import <UASDK/UAEntityRef.h>

@class UAGroupRef, UAUserRef;

@interface UAGroupUserRef : UAEntityRef

/**
* Ref builder based on the group user's ID.
*/
+ (instancetype)groupUserRefWithGroupUserID:(NSString *)groupUserID;

@end

@interface UAGroupUser : UAEntity <UAEntityInterface, NSCoding>

/**
 * Reference for this group user
 */
@property (nonatomic, strong) UAGroupUserRef *ref;

/**
 * Reference to the group the user is associated with
 */
@property (nonatomic, strong) UAGroupRef *groupRef;

/**
 * Reference to the user
 */
@property (nonatomic, strong) UAUserRef *userRef;

@end
