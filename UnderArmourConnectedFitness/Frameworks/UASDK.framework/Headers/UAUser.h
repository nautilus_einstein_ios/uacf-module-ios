/*
 * UAUser.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UAEntity.h>
#import <UASDK/UAEntityRef.h>
#import <UASDK/UAEnumerations.h>

@class UAUserProfilePhotoRef, UAPrivacyRef, UAUserStatsReportRef, UAUserProfilePhoto, UAFriendshipListRef, UAWorkoutListRef;

typedef NS_ENUM(NSInteger, UADisplayMeasurementSystem)
{
	UADisplayMeasurementUndefined,
    UADisplayMeasurementImperial, // "imperial" (lb/miles)
	UADisplayMeasurementMetric,   // "metric" (kg/km)
    UADisplayMeasurementHybrid    // "hybrid" (lbs/km)
};

/**
 *  An object that references a user.
 */
@interface UAUserRef : UAEntityRef

/**
 *  Creates a user reference from a user ID
 *
 *  @param userID The id of the user to create the reference for
 *
 *  @return A user reference with the user id
 */
+ (UAUserRef *)userRefWithUserID:(NSString *)userID;

@end

/**
 *  UAUser contains the user's information and details.
 */
@interface UAUser : UAEntity <UAEntityInterface>

/**
 *  The reference to this user
 */
@property (nonatomic, strong) UAUserRef *ref;

/**
 *  The reference to the profile photo image
 */
@property (nonatomic, strong) UAUserProfilePhotoRef *imageRef;

/**
 *  The user profile photo object for this user.
 *  Note: This is a convience property to avoid having to fetch the UAUserProfilePhoto
 *  from the imageRef. This will be automatically set by the UASDK.
 */
@property (nonatomic, readonly) UAUserProfilePhoto *userProfilePhoto;

/**
 *  The reference to fetch the list of friendships for this user.
 */
@property (nonatomic, strong) UAFriendshipListRef *friendshipsRef;

/**
 *  The reference to fetch the list of workouts for this user.
 */
@property (nonatomic, strong) UAWorkoutListRef *workoutsRef;

/**
 *  The user's first name
 */
@property (nonatomic, copy) NSString *firstName;

/**
 *  The user's last name
 */
@property (nonatomic, copy) NSString *lastName;

/**
 *  Combined first name and last name.
 */
@property (nonatomic, copy) NSString *displayName;

/**
 *  The first character of the user's last name.
 */
@property (nonatomic, copy) NSString *lastInitial;

/**
 *  The user's username
 */
@property (nonatomic, copy) NSString *username;

/**
 *  The user's email
 */
@property (nonatomic, copy) NSString *email;

/**
 *  The user's birthdate
 */
@property (nonatomic, strong) NSDate *birthdate;

/**
 *  The user's gender
 */
@property (nonatomic, assign) UAGender gender;

/**
 *  The user's height in meters
 */
@property (nonatomic, strong) NSNumber *heightNumber;
@property (nonatomic, assign) double height;

/**
 *  The user's weight in the kilograms
 */
@property (nonatomic, strong) NSNumber *weightNumber;
@property (nonatomic, assign) double weight;

/**
 *  The user's ID
 */
@property (nonatomic, strong) NSString *userID;

/**
 *  The user's last login date.
 */
@property (nonatomic, copy) NSDate *lastLogin;

/**
 *  The user's joined date.
 */
@property (nonatomic, copy) NSDate *dateJoined;

/**
 *  The user's introduction (about me).
 */
@property (nonatomic, copy) NSString *introduction;

/**
 *  The user's hobbies.
 */
@property (nonatomic, copy) NSString *hobbies;

/**
 *  The user's goal statement.
 */
@property (nonatomic, copy) NSString *goalStatement;

/**
 *  The user's profile statement.
 */
@property (nonatomic, copy) NSString *profileStatement;


# pragma mark User Preferences

/**
 *  The user's preferred measurement system.
 */
@property (nonatomic, assign) UADisplayMeasurementSystem displayMeasurementSystem;

/**
 *  The user's time zone (ex. America/Denver or MST). Refer to the IANA Time Zone Database (http://en.wikipedia.org/wiki/List_of_tz_database_time_zones)
 */
@property (nonatomic, copy) NSString *timeZone;


#pragma mark Communications Preferences

/**
 *  Receive promotion emails
 */
@property (nonatomic, strong) NSNumber *promotionsNumber;
@property (nonatomic, assign) BOOL promotions;

/**
 *  Receive newsletter emails
 */
@property (nonatomic, strong) NSNumber *newsletterNumber;
@property (nonatomic, assign) BOOL newsletter;

/**
 *  Receive system message emails.
 */
@property (nonatomic, strong) NSNumber *systemMessagesNumber;
@property (nonatomic, assign) BOOL systemMessages;

#pragma mark Sharing Preferences

/**
 *  Share to Twitter by default.
 */
@property (nonatomic, strong) NSNumber *shareTwitterNumber;
@property (assign) BOOL shareTwitter;

/**
 *  Share to Facebook by default.
 */
@property (nonatomic, strong) NSNumber *shareFacebookNumber;
@property (assign) BOOL shareFacebook;

#pragma mark Location

/**
 *  The user's country. Refer to ISO_3166-1 Alpha 2 (http://en.wikipedia.org/wiki/ISO_3166-1_alpha-2)
 */
@property (nonatomic, copy) NSString *country;

/**
 *  The user's region (represents state in US, province in Canada). Refer to ISO_3166_2 (http://en.wikipedia.org/wiki/ISO_3166-2)
 */
@property (nonatomic, copy) NSString *region;

/**
 *  The user's locality (typically represents city).
 */
@property (nonatomic, copy) NSString *locality;

/**
 *  The user's street address.
 */
@property (nonatomic, copy) NSString *address;

#pragma mark Stats

/**
 *  Reference to fetch the user stats by day
 */
@property (nonatomic, strong) UAUserStatsReportRef *statsGroupedByDayRef;

/**
 *  Reference to fetch the user stats by week
 */
@property (nonatomic, strong) UAUserStatsReportRef *statsGroupedByWeekRef;

/**
 *  Reference to fetch the user stats by month
 */
@property (nonatomic, strong) UAUserStatsReportRef *statsGroupedByMonthRef;

/**
 *  Reference to fetch the user stats by year
 */
@property (nonatomic, strong) UAUserStatsReportRef *statsGroupedByYearRef;

/**
 *  Reference to fetch the user stats by lifetime
 */
@property (nonatomic, strong) UAUserStatsReportRef *statsForLifetimeRef;

#pragma mark Privacy

/**
 *  Setting for the profile privacy
 */
@property (nonatomic, assign) UAPrivacyType privacyProfile;

/**
 *  Setting for the workout privacy
 */
@property (nonatomic, assign) UAPrivacyType privacyWorkout;

/**
 *  Setting for the activity feed privacy
 */
@property (nonatomic, assign) UAPrivacyType privacyActivityFeed;

/**
 *  Setting for the bodymass privacy
 */
@property (nonatomic, assign) UAPrivacyType privacyBodymass;

/**
 *  Setting for the food log privacy
 */
@property (nonatomic, assign) UAPrivacyType privacyFoodLog;

/**
 *  Setting for the email search privacy
 */
@property (nonatomic, assign) UAPrivacyType privacyEmailSearch;

/**
 *  Setting for the route privacy
 */
@property (nonatomic, assign) UAPrivacyType privacyRoute;

/**
 *  Setting for the sleep privacy
 */
@property (nonatomic, assign) UAPrivacyType privacySleep;

@end
