//
//  UASpeedSummaryDataPoint.h
//  UASDK
//
//  Created by Roberts, Corey on 11/25/14.
//  Copyright (c) 2014 Under Armour. All rights reserved.
//

#import <UASDK/UADataPoint.h>

@interface UASpeedSummaryDataPoint : UADataPoint

/**
 *  Max speed (double, meters per second).
 */
@property (nonatomic, readonly) NSNumber *speedMax;

/**
 *  Average speed (double, meters per second).
 */
@property (nonatomic, readonly) NSNumber *speedAvg;

/**
 *  Min speed (double, meters per second).
 */
@property (nonatomic, readonly) NSNumber *speedMin;

@end
