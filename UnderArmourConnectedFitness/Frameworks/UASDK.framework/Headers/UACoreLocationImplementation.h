/*
 * UACoreLocationImplementation.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreLocation/CLLocationManagerDelegate.h>
#import <UIKit/UIKit.h>

/**
 The actual implementation of the location manager
 */
@interface UACoreLocationImplementation : NSObject <CLLocationManagerDelegate>

/**
 The actual Apple CLLocationManager
 */
@property(nonatomic, strong) CLLocationManager *locationManager;

/**
 The delegate is the object getting the location updates
 */
@property(nonatomic, strong) id <CLLocationManagerDelegate> delegate;

/**
 The location is the current location of the location manager implemented, mock or real
 */
@property(nonatomic, retain) CLLocation *location;

@end
