//
//  UADataPoint.h
//  UASDK
//
//  Created by Morrow, Andrew on 11/19/14.
//  Copyright (c) 2014 Under Armour. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UADataType;

@interface UADataPoint : NSObject <NSCoding>

/**
 * Data type associated with this data point.
 */
@property (nonatomic, readonly, weak) UADataType *dataType;

/**
 * Datetime associated with this data point.
 */
@property (nonatomic, readonly) NSDate *datetime;

/**
 * Start datetime associated with this data point.
 */
@property (nonatomic, readonly) NSDate *startDatetime;

- (instancetype)init __attribute__((unavailable("init not available")));
+ (instancetype)new __attribute__((unavailable("new not available")));

/**
 * Retrieves field value by key
 * @param Key field ID
 */
- (id)getFieldValueForKey:(NSString *)key;

@end
