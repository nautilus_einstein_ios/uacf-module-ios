/*
 * UAPage.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAEntity.h>
#import <UASDK/UAPageRef.h>


@class UAUserRef, UAWorkoutListRef, UAActigraphyListRef, UAActivityStoryListRef,
       UAPageTypeRef, UAFollowerListRef, UAPageAssociationListRef,
       UAActivityStoryListRef, UAPageFollowRef, UAUserAchievementListRef,
       UAImage, UAPageSettings, UALocation;

/**
 *
 *  An UAPage is an object that represents a Page resource.
 *  There are three different kinds of Pages: Athlete, Group, and User pages.
 *  A Page's type can be determined using the pageTypeReference property.
 *
 *  An UAPage may or may not have all of its fields populated, depending on the page type.
 *  Here are some fields that are known to *not* be populated for the following page types:
 *
 *  UAPageTypeAthlete: 
 *  - All fields populated
 *  
 *  UAPageTypeGroup: 
 *  - userRef
 *  - workoutsReference
 *  - actigraphyReference
 *
 *  UAPageTypeUser:
 *  - associationReference
 *  - followerReference
 *  - followReference
 *
 */
@interface UAPage : UAEntity <UAEntityInterface>

/**
 *  The alias for this user's page (i.e. username), used in the page url.
 */
@property (nonatomic, copy, readonly) NSString *alias;

/**
 *  The title for this user's page.
 *  @discussion If this is an athlete, the title will be of the format "first_name last_name."
 */
@property (nonatomic, copy, readonly) NSString *title;

/**
 *  The description for this user's page.
 */
@property (nonatomic, copy, readonly) NSString *pageDescription;

/**
 *  A link to this page.
 */
@property (nonatomic, copy, readonly) NSURL *url;

/**
 *  A link to the official website for this page (i.e. brand site, athlete site, etc.).
 */
@property (nonatomic, copy, readonly) NSURL *website;

/**
 *  A headline for this page.
 */
@property (nonatomic, copy, readonly) NSString *headline;

/**
 *  A profile photo for this page.
 *  @warning This currently does not exist in production, and will not be populated
 *  until a future update.
 */
@property (nonatomic, strong, readonly) UAImage *profilePhoto;

/**
 *  A cover photo for this page.
 *  @warning This currently does not exist in production, and will not be populated
 *  until a future update.
 */
@property (nonatomic, strong, readonly) UAImage *coverPhoto;

/**
 *  The location of this person, brand, or athlete.
 */
@property (nonatomic, strong, readonly) UALocation *location;

/**
 *  A reference to this user.
 */
@property (nonatomic, strong, readonly) UAUserRef *userRef;

/**
 *  A reference to this page.
 */
@property (nonatomic, strong) UAPageRef *ref;

/**
 *  A reference to this page type.
 */
@property (nonatomic, strong, readonly) UAPageTypeRef *pageTypeReference;

/**
 *  A reference to a PageFollow object.
 */
@property (nonatomic, strong, readonly) UAPageFollowRef *followReference;

/**
 *  A reference to a unfollowed PageFollow object.
 */
@property (nonatomic, strong, readonly) UAPageFollowRef *unfollowReference;

/**
 *  A collection reference to this user's workouts.
 */
@property (nonatomic, strong, readonly) UAWorkoutListRef *workoutsReference;

/**
 *  A collection reference to this user's actigraphy data.
 */
@property (nonatomic, strong, readonly) UAActigraphyListRef *actigraphyReference;

/**
 *  A collection reference to the page associations pointing TO this page
 */
@property (nonatomic, strong, readonly) UAPageAssociationListRef *toAssocationReference;

/**
 *  A collection reference to the page associations pointing FROM this page
 */
@property (nonatomic, strong, readonly) UAPageAssociationListRef *fromAssocationReference;

/**
 *  A collection reference to this user's followers.
 */
@property (nonatomic, strong, readonly) UAFollowerListRef *followerReference;

/**
 *  A collection reference to this user's following.
 */
@property (nonatomic, strong, readonly) UAFollowerListRef *followingReference;

/**
 *  A collection reference to this user's activity feed.
 */
@property (nonatomic, strong, readonly) UAActivityStoryListRef *activityStoryReference;

/**
 *  A collection reference to this user's activity feed.
 */
@property (nonatomic, strong, readonly) UAUserAchievementListRef *userAchievementReference;

/**
 *  A collection reference to the featured activity feed for this page.
 */
@property (nonatomic, strong, readonly) UAActivityStoryListRef *featuredActivityFeedReference;

/**
 *  Settings for this page.
 */
@property (nonatomic, strong, readonly) UAPageSettings *settings;

/**
 *  Gets the number of pages with which this page has associated itself
 *
 *  @return The number of associations FROM this page to other pages
 */
- (NSUInteger)getFromAssociationCount;

/**
 *  Gets the number of pages that have associated themselves with this page
 *
 *  @return The number of associations TO this page from other pages
 */
- (NSUInteger)getToAssociationCount;

/**
 *  Gets the number of followers for this page.
 *
 *  @return The number of followers for this page.
 */
- (NSUInteger)getFollowerCount;

/**
 *  Gets the number of following for this page.
 *
 *  @return The number of following for this page.
 */
- (NSUInteger)getFollowingCount;

@end
