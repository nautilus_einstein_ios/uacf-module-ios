/*
 * UAMetabolicEnergyCalculator.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <Foundation/Foundation.h>

@class UAUser, UAActivityType;

@interface UAMetabolicEnergyCalculator : NSObject

/**
 *  Calculates the number of joules burned for a user doing a given activity.
 *
 *  @param user             A user.
 *  @param activityType     The activity that this user has done.
 *  @param timeInSeconds    The duration for this activity.
 *  @param distanceInMeters The distance traveled for this activity, if applicable. Optional.
 *
 *  @return The number of joules burned for this activity.
 *
 *  @warning *Important:* A UAUser must have a valid weight and height for this method to work properly.
 */
- (double)calculateJoulesForUser:(UAUser *)user
					activityType:(UAActivityType *)activityType
					 elapsedTime:(NSTimeInterval)timeInSeconds
						distance:(double)distanceInMeters;

@end
