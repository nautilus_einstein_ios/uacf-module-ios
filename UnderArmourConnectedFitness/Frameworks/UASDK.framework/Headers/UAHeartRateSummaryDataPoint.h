//
//  UAHeartRateSummaryDataPoint.h
//  UASDK
//
//  Created by Roberts, Corey on 12/11/14.
//  Copyright (c) 2014 Under Armour. All rights reserved.
//

#import "UADataPoint.h"

@interface UAHeartRateSummaryDataPoint : UADataPoint

/**
 *  Max heartrate (integer, beats per minute).
 */
@property (nonatomic, readonly) NSNumber *heartRateMax;

/**
 *  Average heartrate (double, beats per minute).
 */
@property (nonatomic, readonly) NSNumber *heartRateAvg;

/**
 *  Min heartrate (integer, beats per minute).
 */
@property (nonatomic, readonly) NSNumber *heartRateMin;

@end
