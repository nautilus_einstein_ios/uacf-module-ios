/*
 * UAGroupUserList.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAEntityList.h>
#import <UASDK/UAEntityListRef.h>

@class UAGroupRef, UAUserRef;

@interface UAGroupUserListRef : UAEntityListRef

+ (instancetype)groupUserListRefWithUserRef:(UAUserRef *)reference;

+ (instancetype)groupUserListRefWithGroupRef:(UAGroupRef *)reference;

@end

@interface UAGroupUserList : UAEntityList

@property (nonatomic, strong) UAGroupUserListRef *ref;

/**
*  A reference to the previous collection of group users.
*  @discussion If no collection has previously been visited, this value will be nil.
*/
@property (nonatomic, strong) UAGroupUserListRef *previousRef;

/**
*  A reference to the next collection of group users.
*  @discussion If no more group users exist, this value will be nil.
*/
@property (nonatomic, strong) UAGroupUserListRef *nextRef;

@end
