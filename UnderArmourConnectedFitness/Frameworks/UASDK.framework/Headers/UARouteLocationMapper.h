/*
 * UARouteLocationMapper.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import "UAObjectMapper.h"

@class UARouteLocation, UARouteLocationVO;

/**
 *  A mapper class that can map a dictionary containing appropriate keys to
 *  an UARouteLocation and vice-versa.
 */
@interface UARouteLocationMapper : UAObjectMapper

/**
 *  Creates a dictionary representation of an UARouteLocation, to be primarily
 *  used for web services.
 *
 *  @param routeLocation an UARouteLocation to convert.
 *
 *  @return an NSDictionary representation of an UARouteLocation.
 */
+ (NSDictionary *)dictionaryFromUARouteLocation:(UARouteLocation *)routeLocation;

/**
 *  Creates an UARouteLocation representation of an object from a dictionary.
 *
 *  @param dictionary a dictionary containing keys mapped for an UARouteLocation.
 *
 *  @return an UARouteLocation representation of an NSDictionary object.
 */
+ (UARouteLocation *)routeLocationFromDictionary:(NSDictionary *)dictionary;

@end
