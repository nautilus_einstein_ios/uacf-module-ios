/*
 * UARouteBookmark.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


@import Foundation;

#import <UASDK/UAEntity.h>
#import <UASDK/UAEntityRef.h>

@class UARouteRef, UARouteBookmarkRef, UAUserRef;

/**
 *  An object that references a route bookmark.
 */
@interface UARouteBookmarkRef : UAEntityRef

+ (UARouteBookmarkRef *)routeBookmarkRefWithBookmarkRouteID:(NSString *)bookmarkRouteID;

@end

@interface UARouteBookmark : UAEntity <UAEntityInterface>

@property (nonatomic, strong) UARouteBookmarkRef *ref;

@property (nonatomic, strong) UAUserRef *userRef;

@property (nonatomic, strong) UARouteRef *routeRef;

@end
