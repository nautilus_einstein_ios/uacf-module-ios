/*
 * UATimeSeriesTimerStopPoint.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UATimeSeriesPoint.h>

@interface UATimeSeriesTimerStopPoint : UATimeSeriesPoint

+ (instancetype)pointAtTime:(NSTimeInterval)timestamp withPauseDuration:(NSTimeInterval)pauseDuration;

/**
 *  Time in seconds of the pause
 */
@property (nonatomic, readonly) NSTimeInterval pauseDuration;

@end
