//
//  UABodyMassSummaryDataPoint.h
//  UASDK
//
//  Created by mallarke on 11/27/14.
//  Copyright (c) 2014 Under Armour. All rights reserved.
//

#import <UASDK/UADataPoint.h>

@interface UABodyMassSummaryDataPoint : UADataPoint

/**
 *  Maximum body mass (double, kilograms).
 */
@property (nonatomic, readonly) NSNumber *massMax;

/**
 *  Minimum body mass (double, kilograms).
 */
@property (nonatomic, readonly) NSNumber *massMin;

/**
 *  Average body mass (double, kilograms).
 */
@property (nonatomic, readonly) NSNumber *massAvg;

/**
 * Start body mass (double, kilograms).
 */
@property(nonatomic, readonly) NSNumber *massStart;

/**
 * End body mass (double, kilograms).
 */
@property (nonatomic, readonly) NSNumber *massEnd;

/**
 *  Maximum lean body mass (double, kilograms).
 */
@property (nonatomic, readonly) NSNumber *leanMassMax;

/**
 *  Minimum lean body mass (double, kilograms).
 */
@property (nonatomic, readonly) NSNumber *leanMassMin;

/**
 *  Average lean body mass (double, kilograms).
 */
@property (nonatomic, readonly) NSNumber *leanMassAvg;

/**
 * Start lean mass (double, kilograms).
 */
@property(nonatomic, readonly) NSNumber *leanMassStart;

/**
 * End lean mass (double, kilograms).
 */
@property (nonatomic, readonly) NSNumber *leanMassEnd;

/**
 *  Maximum fat body mass (double, kilograms).
 */
@property (nonatomic, readonly) NSNumber *fatMassMax;

/**
 *  Minimum fat body mass (double, kilograms).
 */
@property (nonatomic, readonly) NSNumber *fatMassMin;

/**
 *  Average fat body mass (double, kilograms).
 */
@property (nonatomic, readonly) NSNumber *fatMassAvg;

/**
 * Start fat mass (double, kilograms).
 */
@property(nonatomic, readonly) NSNumber *fatMassStart;

/**
 * End fat mass (double, kilograms).
 */
@property (nonatomic, readonly) NSNumber *fatMassEnd;

@end
