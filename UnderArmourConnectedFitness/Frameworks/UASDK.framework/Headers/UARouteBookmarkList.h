/*
 * UARouteBookmarkList.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#import <UASDK/UAEntityList.h>
#import <UASDK/UAEntityListRef.h>

@class UAUserRef;

@interface UARouteBookmarkListRef : UAEntityListRef

+ (UARouteBookmarkListRef *)routeBookmarkListRefWithUserRef:(UAUserRef *)reference;

@end

@interface UARouteBookmarkList : UAEntityList <UAEntityListRefInterface>

@property (nonatomic, strong) UARouteBookmarkListRef *ref;

@property (nonatomic, strong) UARouteBookmarkListRef *previousRef;

@property (nonatomic, strong) UARouteBookmarkListRef *nextRef;

@end
