/*
 * UAHeartRateZonesList.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAEntityList.h>
#import <UASDK/UAEntityListRef.h>

@class UAUserRef;

@interface UAHeartRateZonesListRef : UAEntityListRef

/**
 *  Creates a reference to the list of all heart rate zones for the referenced user.
 *  @param userRef A reference to a user.
 *  @return A reference to the list of all heart rate zones for the referenced user.
 */
+ (instancetype)heartRateZonesListRefWithUserRef:(UAUserRef *)userRef;

@end

@interface UAHeartRateZonesList : UAEntityList

/**
 *  A reference to this object.
 */
@property (nonatomic, strong) UAHeartRateZonesListRef *ref;

/**
 *  A reference to the previous collection of heart rate zones.
 *  @discussion If no collection has previously been visited, this value will be nil.
 */
@property (nonatomic, strong) UAHeartRateZonesListRef *previousRef;

/**
 *  A reference to the next collection of heart rate zones.
 *  @discussion If no more heart rate zones exist, this value will be nil.
 */
@property (nonatomic, strong) UAHeartRateZonesListRef *nextRef;

@end
