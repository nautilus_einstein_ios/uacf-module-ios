//
//  UAActivityStoryActigraphyObject.h
//  UA
//
//  Created by Jeremy Zedell on 5/29/14.
//  Copyright (c) 2014 MapMyFitness Inc. All rights reserved.
//

#import <UASDK/UAActivityStoryObject.h>
#import <UASDK/UAActivityStoryHighlight.h>
#import <UASDK/UAPrivacyRef.h>

@interface UAActivityStoryActigraphyObject : UAActivityStoryObject <NSCoding>

@property (nonatomic, strong) UAPrivacyRef *privacyRef;

/**
 Highlights are the top three fields for a workout or route object,
 chosen from a pre-determined list by activity type or object type.
 An object can have between 0 and 3 highlights, and only fields that
 are present and non-zero will be highlighted.
 
 Distance, duration and pace/speed highlights have an optional percentile callout.
 Percentiles include: Top 10%, Top 25%. Thresholds are dependent on gender and
 only activity types with a base type of run, bike, or walk can have callouts.
 
 @return NSArray of UAActivityStoryHighlight
 */
@property (nonatomic, strong) NSArray *highlights;

@property (nonatomic, strong) NSDate *startTime;

@property (nonatomic, strong) NSDate *endTime;

@property (nonatomic, strong) NSDate *published;

@property (nonatomic, assign) NSInteger steps;

@property (nonatomic, assign) UAPrivacyType privacy;

@end
