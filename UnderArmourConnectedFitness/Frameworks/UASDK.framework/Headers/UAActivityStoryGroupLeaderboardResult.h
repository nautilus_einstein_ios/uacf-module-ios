//
//  UAActivityStoryGroupLeaderboardResult.h
//  UASDK
//
//  Created by DX165 on 2014-11-24.
//  Copyright (c) 2014 Under Armour. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UASDK/UADataSeries.h>
#import <UASDK/UAUser.h>

@interface UAActivityStoryGroupLeaderboardResult : NSObject

@property (nonatomic, strong) UADataPoint *value;
@property (nonatomic, strong) UAUserRef *userRef;
@property (nonatomic) NSUInteger rank;

@end
