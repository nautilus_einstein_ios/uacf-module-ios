/*
 * UAGroupInviteList.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <Foundation/Foundation.h>
#import <UASDK/UAEntityListRef.h>
#import <UASDK/UAEntityList.h>

@class UAGroupRef, UAUserRef;

@interface UAGroupInviteListRef : UAEntityListRef

/**
* Ref builder based on the group's reference.
*/
+ (instancetype)groupInviteListRefWithGroupRef:(UAGroupRef *)reference;

/**
* Ref builder based on a user's reference.
*/
+ (instancetype)groupInviteListRefWithUserRef:(UAUserRef *)reference;

@end

@interface UAGroupInviteList : UAEntityList <UAEntityListRefInterface>

@property (nonatomic, strong) UAGroupInviteListRef *ref;

/**
*  A reference to the previous collection of group invites.
*  @discussion If no collection has previously been visited, this value will be nil.
*/
@property (nonatomic, strong) UAGroupInviteListRef *previousRef;

/**
*  A reference to the next collection of group invites.
*  @discussion If no more group invitations exist, this value will be nil.
*/
@property (nonatomic, strong) UAGroupInviteListRef *nextRef;

@end
