/*
 * UAGroupPurposeList.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <UASDK/UAEntityList.h>
#import <UASDK/UAEntityListRef.h>

@interface UAGroupPurposeListRef : UAEntityListRef

/**
 *  Creates a reference to all group purpose objects.
 *  @return A reference to the list of all group purposes.
 */
+ (UAGroupPurposeListRef *)groupPurposeListRef;

@end

@interface UAGroupPurposeList : UAEntityList <UAEntityListRefInterface>

@property (nonatomic, strong) UAGroupPurposeListRef *ref;

/**
 *  A reference to the previous collection of entities.
 *  @discussion If no more entities exist, this value will be nil.
 */
@property (nonatomic, strong) UAGroupPurposeListRef *previousRef;

/**
 *  A reference to the next collection of entities.
 *  @discussion If no more entities exist, this value will be nil.
 */
@property (nonatomic, strong) UAGroupPurposeListRef *nextRef;

@end
