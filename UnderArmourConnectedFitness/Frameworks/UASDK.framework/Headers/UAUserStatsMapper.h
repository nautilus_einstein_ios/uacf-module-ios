/*
 * UAUserStatsMapper.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


@class UAUserStatsReport;

@interface UAUserStatsMapper : NSObject

+ (UAUserStatsReport*)userStatsReportFromDictionary:(NSDictionary *)result;

@end
