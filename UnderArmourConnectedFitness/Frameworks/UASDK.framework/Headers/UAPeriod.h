/*
 * UAPeriod.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <Foundation/Foundation.h>
#import <UASDK/UAValidationProtocol.h>

typedef NS_ENUM(NSUInteger, UAPeriodDuration)
{
	UAPeriodDurationDay, UAPeriodDurationWeek,
	UAPeriodDurationMonth, UAPeriodDurationYear
};

@interface UAPeriod : NSObject <UAValidationProtocol, NSCoding>

/**
 * Number of days/weeks/months associated with the duration
 */
@property (nonatomic, readonly) NSUInteger count;

/**
 * Duration of the period
 */
@property (nonatomic, readonly) UAPeriodDuration duration;

/**
 * Creates a period object for a duration
 * @discussion this will return a duration with a count of 1
 */
- (instancetype)initWithPeriodDuration:(UAPeriodDuration)duration;
+ (instancetype)peridWithPeriodDuration:(UAPeriodDuration)duration;

/**
 * Creates a period object for a duration
 */
- (instancetype)initWithPeriodDuration:(UAPeriodDuration)duration withCount:(NSUInteger)count NS_DESIGNATED_INITIALIZER;
+ (instancetype)periodWithPeriodDuration:(UAPeriodDuration)duration withCount:(NSUInteger)count;

@end
