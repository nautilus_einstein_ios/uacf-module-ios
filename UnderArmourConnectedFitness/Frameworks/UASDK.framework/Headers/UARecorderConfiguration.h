/*
 * UARecorderConfiguration.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */

#import <Foundation/Foundation.h>

@class UAUserRef, UAActivityTypeReference, UADataSourceConfiguration, UADataSourceConfiguration;

/**
 *  This class helps configure a new recorder.
 */
@interface UARecorderConfiguration : NSObject

/**
 *  The name of the recorder.
 * 
 *  @discussion For multiple recorders, it is necessary to have unique recorder names.
 *  Failure to do so will result in undefined behavior.
 */
@property (nonatomic, copy) NSString *recorderName;

/**
 *  A reference to the user.
 */
@property (nonatomic, strong) UAUserRef *userReference;

/**
 *  A reference to the activity type.
 */
@property (nonatomic, strong) UAActivityTypeReference *activityTypeReference;

/**
 *  A list of UADataSourceConfiguration objects.
 */
@property (nonatomic, copy, readonly) NSArray *dataSourceConfigurations;

/**
 *  Adds a new message producer configuration.
 *
 *  @param dataSourceConfiguration A new message producer configuration.
 */
- (void)addMessageProducerConfiguration:(UADataSourceConfiguration *)dataSourceConfiguration;


/**
 *  Creates a new instance of this object with specified parameters.
 *
 *  @param recorderName                  The name of the recorder.
 *  @param userRef                       A reference to a user.
 *  @param activityTypeRef               A reference to an activity type.
 *  @param dataSourceConfigurations A list of message producer configurations.
 *
 *  @return A new instance of UARecorderConfiguration.
 */
- (instancetype)initWithRecorderName:(NSString *)recorderName
							 userRef:(UAUserRef *)userRef
					 activityTypeRef:(UAActivityTypeReference *)activityTypeRef
	   dataSourceConfigurations:(NSArray *)dataSourceConfigurations NS_DESIGNATED_INITIALIZER;

@end
