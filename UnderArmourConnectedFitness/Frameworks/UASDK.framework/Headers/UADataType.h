//
//  UADataType.h
//  UASDK
//
//  Created by Morrow, Andrew on 11/19/14.
//  Copyright (c) 2014 Under Armour. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UASDK/UAEntity.h>
#import <UASDK/UAEntityRef.h>

typedef NS_ENUM(NSInteger, UADataTypePeriod)
{
	UADataTypePeriodUnknown, UADataTypePeriodInstant, UADataTypePeriodInterval
};

// measurement data types
extern NSString * const kUADataTypeLocationKey;
extern NSString * const kUADataTypeElevationKey;
extern NSString * const kUADataTypeHeartRateKey;
extern NSString * const kUADataTypeHeightKey;
extern NSString * const kUADataTypeBodyMassKey;
extern NSString * const kUADataTypeSpeedKey;
extern NSString * const kUADataTypeWillpowerKey;
extern NSString * const kUADataTypeDistanceKey;
extern NSString * const kUADataTypeEnergyExpendedKey;
extern NSString * const kUADataTypeEnergyConsumedKey;
extern NSString * const kUADataTypeStepsKey;
extern NSString * const kUADataTypeIntensityKey;

// summary data types
extern NSString * const kUADataTypeDistanceSummaryKey;
extern NSString * const kUADataTypeActiveTimeSummaryKey;
extern NSString * const kUADataTypeEnergyExpendedSummaryKey;
extern NSString * const kUADataTypeEnergyConsumedSummaryKey;
extern NSString * const kUADataTypeBodyMassSummaryKey;
extern NSString * const kUADataTypeStepsSummaryKey;
extern NSString * const kUADataTypeSpeedSummaryKey;
extern NSString * const kUADataTypeHeartRateSummaryKey;
extern NSString * const kUADataTypeSessionsSummaryKey;
extern NSString * const kUADataTypeSleepSummaryKey;

@class UADataPoint, UADataSeries;

@interface UADataTypeRef : UAEntityRef

+ (instancetype)dataTypeRefWithDataTypeID:(NSString *)dataTypeID;

@end

@interface UADataType : UAEntity <UAEntityInterface, NSCoding>

/**
 * Reference to this data type.
 */
@property (nonatomic, strong) UADataTypeRef *ref;

/** 
 *  Set of UADataField objects present on this data type.
 */
@property (nonatomic, copy, readonly) NSSet *fields;

/**
 * Description of this data type.
 */
@property (nonatomic, copy, readonly) NSString *dataTypeDescription;

/**
 * Period duration of this data type.
 */
@property (nonatomic, readonly) UADataTypePeriod period;

- (UADataPoint *)createDataPoint;
- (UADataSeries *)createDataSeries;

- (instancetype)init __attribute__((unavailable("init not available")));
+ (instancetype)new __attribute__((unavailable("new not available")));

@end
