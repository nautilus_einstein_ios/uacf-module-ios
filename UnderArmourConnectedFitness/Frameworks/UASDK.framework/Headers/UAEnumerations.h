/*
 * UAEnumerations.h
 *
 * Copyright (c) 2015 Under Armour. All rights reserved.
 *
 * See LICENSE.txt for complete licensing information.
 *
 */


#ifndef UA_UAEnumerations_h
#define UA_UAEnumerations_h

@import Foundation;

@class UAUser;

typedef NS_ENUM(NSInteger, UAPrivacyType)
{
	UAPrivacyTypeUnknown = -1,
    UAPrivacyTypePrivate = 0,
	UAPrivacyTypeFriendsOnly = 1,
	UAPrivacyTypePublic = 3
};

typedef NS_ENUM(NSInteger, UAGender)
{
	UAGenderUnknown = 0,
	UAGenderMale = 1,
	UAGenderFemale = 2,
};

typedef NS_ENUM(NSUInteger, UASocialProviderType) {
    UASocialProviderUnknown = 0,
    UASocialProviderFacebook,
    UASocialProviderTwitter
};

typedef NS_ENUM(NSInteger, UAUserPermissionType)
{
    UAUserPermissionTypeNone = 0,
    UAUserPermissionTypeCreateResource = 1 << 0,
    UAUserPermissionTypeManageRoles = 1 << 1,
    UAUserPermissionTypeEditResource = 1 << 2,
    UAUserPermissionTypeEditSettings = 1 << 3,
    UAUserPermissionTypeCreatePost = 1 << 4,
    UAUserPermissionTypeDeletePost = 1 << 5,
    UAUserPermissionTypeCreateReply = 1 << 6,
    UAUserPermissionTypeDeleteReply = 1 << 7,
};

typedef NS_ENUM(NSInteger, UAShareOptionType)
{
    UAShareOptionTypeNone = 0,
    UAShareOptionTypeFacebook = 1 << 0,
    UAShareOptionTypeTwitter = 1 << 1,
};

typedef NSInteger UAShareOptionBitmask;

typedef NS_ENUM(NSInteger, UAAggregationPeriodType)
{
    UAAggregationPeriodTypeDay,
    UAAggregationPeriodTypeWeek,
    UAAggregationPeriodTypeMonth,
    UAAggregationPeriodTypeYear,
    UAAggregationPeriodTypeLifetime
};

typedef NS_ENUM(NSUInteger, UASensorStatus) {
    UASensorStatusNotConnected = 0,
	UASensorStatusConnecting,
    UASensorStatusConnected
};

typedef NS_ENUM(NSUInteger, UASensorHealth) {
	UASensorHealthUnknown = 0,
	UASensorHealthVeryGood,
	UASensorHealthGood,
	UASensorHealthAverage,
	UASensorHealthBad,
	UASensorHealthVeryBad
};

typedef void (^UAAuthenticationRequiredBlock)(void);
typedef void (^UAResponseBlock)(NSError *error);
typedef void (^UAAuthResponseBlock)(UAUser *user, NSError *error);
typedef void (^UAObjectResponseBlock)(id object, NSError *error);
typedef void (^UADidFailWithErrorBlock)(NSError *error);
typedef void (^UADidLoadObjectBlock)(id object);

#endif
