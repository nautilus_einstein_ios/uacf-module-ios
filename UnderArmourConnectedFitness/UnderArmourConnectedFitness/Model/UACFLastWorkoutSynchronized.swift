//
//  UACFLastWorkoutSynchronized.swift
//  UnderArmourConnectedFitness
//
//  Created by Cesar Brenes on 5/8/17.
//  Copyright © 2017 Veux Labs. All rights reserved.
//

import Foundation


public class UACFLastWorkoutSynchronized: NSObject{
    public private(set) var hour: Int
    public private(set) var minutes: Int
    public private(set) var seconds: Int
    public private(set) var year: Int
    public private(set) var month: Int
    public private(set) var day: Int
    public private(set) var recordID: String
    public private(set) var underArmourIdentifier: String
    
    init(hour: Int, minutes: Int, seconds: Int, year: Int, month: Int, day: Int, recordID:String, underArmourIdentifier: String) {
        self.hour = hour
        self.minutes = minutes
        self.seconds = seconds
        self.year = year
        self.month = month
        self.day = day
        self.recordID = recordID
        self.underArmourIdentifier = underArmourIdentifier
    }    
}
