//
//  UACFAuthentication.swift
//  UnderArmourConnectedFitness
//
//  Created by Cesar Brenes on 4/21/17.
//  Copyright © 2017 Veux Labs. All rights reserved.
//

import Foundation
import UASDK
import SafariServices


public class UACFAuthentication: NSObject{
    
    private var completionForAuthentication: ((_ error: NSError?) -> Void)?
    static private let isAuthenticatedKey = "UACFIsAuthenticatedKey"
    private var didTheUserMakeTheAuthenticationSuccessfullAFewSecondsAgo = false
    private var safariViewController: SFSafariViewController?
    public static let sharedInstance = UACFAuthentication()
    
    
    
    //This prevents others from using the default '()' initializer for this class.
    fileprivate override init() {
        super.init()
    }
    
    
    public func initialize(withApplicationConsumer: String, applicationSecret: String){
        UA.initialize(withApplicationConsumer: withApplicationConsumer, applicationSecret: applicationSecret)
    }
    
    public func canTheAppStartTheSynchronizationWithUnderArmour() -> Bool{
        let backupFlag = didTheUserMakeTheAuthenticationSuccessfullAFewSecondsAgo
        didTheUserMakeTheAuthenticationSuccessfullAFewSecondsAgo = false
        return backupFlag
    }
    
    public  func requestUserAuthentication(viewController:UIViewController, completion:@escaping (_ error: NSError?) -> Void){
        completionForAuthentication = completion
        if Reachability.isConnectedToNetwork(){
            let authenticationURL = UA.sharedInstance()?.requestUserAuthorizationUrl()
            guard let authenticationURLUnwraped = authenticationURL else {
                completionForAuthentication = nil
                completion(ErrorHandle.createErrorCode(errorCode: Constants.unexpectedErrorCode))
                return
            }
            let request = NSURLRequest(url: authenticationURLUnwraped)
            if #available(iOS 10.0, *) {
                safariViewController = SFSafariViewController(url: request.url!)
                viewController.present(safariViewController!, animated: true, completion: nil)
            } else {
                UIApplication.shared.openURL(request.url!)
            }
        }
        else{
            completionForAuthentication = nil
            completion(ErrorHandle.createErrorCode(errorCode: Constants.notInternetErrorCode))
        }
    }
    
    public func disconnectWithUnderArmour(completion:@escaping (_ error: NSError?) -> Void){
        if Reachability.isConnectedToNetwork(){
            UA.sharedInstance()?.logout { (error) in
                if error == nil{
                    UserDefaults.standard.set(false, forKey: UACFAuthentication.isAuthenticatedKey)
                    completion(nil)
                }
                else{
                    completion(ErrorHandle.createErrorCode(errorCode: Constants.unexpectedErrorCode))
                }
            }
        }
        else{
            completion(ErrorHandle.createErrorCode(errorCode: Constants.notInternetErrorCode))
        }
    }
    
    public func isAlreadyAuthenticated() -> Bool{
        if Reachability.isConnectedToNetwork(){
            guard let underArmour = UA.sharedInstance() else{
                return false
            }
            UserDefaults.standard.set(underArmour.isAuthenticated, forKey: UACFAuthentication.isAuthenticatedKey)
            return underArmour.isAuthenticated
        }
        else{
            return UserDefaults.standard.bool(forKey: UACFAuthentication.isAuthenticatedKey)
        }
    }
    
    
    public func handle(url: URL) -> Bool{
        guard let underArmour = UA.sharedInstance() else {
            return false
        }
        if (underArmour.handle(url) { (user, errorCode) in
            if errorCode == nil{
                UserDefaults.standard.set(true, forKey: UACFAuthentication.isAuthenticatedKey)
                self.didTheUserMakeTheAuthenticationSuccessfullAFewSecondsAgo = true
                self.completionForAuthentication?(nil)
                self.safariViewController?.dismiss(animated: true, completion: nil)
            }
            else{
                self.completionForAuthentication?(ErrorHandle.createErrorCode(errorCode: Constants.notInternetErrorCode))
            }
        }){
            return true
        }
        return false
    }
    
}
