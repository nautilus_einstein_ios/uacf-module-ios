//
//  Constants.swift
//  UnderArmourConnectedFitness
//
//  Created by Cesar Brenes on 5/2/17.
//  Copyright © 2017 Veux Labs. All rights reserved.
//

import Foundation


struct Constants {
    static let errorCodeKey = "codeKey"
    static let errorDescriptionKey = "descriptionKey"
    static let errorFailureReasonKey = "failureReasonKey"
    static let errorSuggestionKey = "suggestionKey"
    static let notInternetErrorCode = -1009
    static let unexpectedErrorCode = 1015
    static let parametersMissingErrorCode = 1016
    static let parametersHaveWrongUnitErrorCode = 1017
    static let unitOrValueAreMissingErrorCode = 1018
    static let workoutAlreadySynchronizedErrorCode = 1019
    static let userIsNotAuthenticatedWithUACFErrorCode = 1020
    static let zero = 0
    static let underArmourWorkoutKey = "underArmourWorkoutKey"
    static let bundle = Bundle(identifier: "com.veuxlabs.UnderArmourConnectedFitness")!
}

public struct WorkoutKeys{
    public static let START_TIME = "START_TIME"
    public static let TIME_ZONE = "TIME_ZONE"
    public static let NAME = "NAME"
    public static let ACTIVITY_TYPE = "ACTIVITY_TYPE"
    public static let SPEED_AVG = "SPEED_AVG"
    public static let SPEED_UNIT = "SPEED_UNIT"
    public static let DISTANCE = "DISTANCE"
    public static let DISTANCE_UNIT = "DISTANCE_UNIT"
    public static let METABOLIC_ENERGY_TOTAL = "METABOLIC_ENERGY_TOTAL"
    public static let METABOLIC_ENERGY_TOTAL_UNIT = "METABOLIC_ENERGY_TOTAL_UNIT"
    public static let ELAPSED_TIME = "ELAPSED_TIME"
    public static let ELAPSED_TIME_UNIT = "ELAPSED_TIME_UNIT"
    public static let HEART_RATE_AVG = "HEART_RATE_AVG"
    public static let POWER_AVG = "POWER_AVG"
    public static let CONSOLE_USER_NUMBER = "CONSOLE_USER_NUMBER"
    public static let RECORD_ID = "RECORD_ID"
    public static let MACHINE_TYPE = "MACHINE_TYPE"
    public static let DAY = "DAY"
    public static let MONTH = "MONTH"
    public static let YEAR = "YEAR"
    public static let HOUR = "HOUR"
    public static let MINUTES = "MINUTES"
    public static let SECONDS = "SECONDS"
}


public enum WorkoutDistanceUnit: Int {
    case km = 1, miles
}

public enum WorkoutSpeedUnit: Int {
    case kph = 1, mph
}

public enum WorkoutElapsedTime: Int {
    case seconds = 1, minutes
}

public enum WorkoutMetabolicEnergy: Int {
    case jules = 1, calories
}

public enum WorkoutActivityTypes: String {
    case treadmillGeneral = "262",
    ellipticalMachine = "211",
    recumbentBike = "50",
    stationaryBike = "548",
    machineWorkout = "14",
    cardiovascularCircularTraining = "542",
    bowflexTreadClimber = "226",
    treadmillRun = "208"
}




extension String{
    func localize() -> String{
        let bundle: Bundle = Bundle(identifier: "com.veuxlabs.UnderArmourConnectedFitness")!
        return  NSLocalizedString(self, tableName: nil, bundle: bundle, value: "", comment: "")
    }

}






