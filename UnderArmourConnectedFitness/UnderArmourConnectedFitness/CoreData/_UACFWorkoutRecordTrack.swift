// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to UACFWorkoutRecordTrack.swift instead.

import Foundation
import CoreData

public enum UACFWorkoutRecordTrackAttributes: String {
    case consoleUserNumber = "consoleUserNumber"
    case customKey = "customKey"
    case day = "day"
    case hour = "hour"
    case machineType = "machineType"
    case minutes = "minutes"
    case month = "month"
    case primaryKey = "primaryKey"
    case recordID = "recordID"
    case seconds = "seconds"
    case workoutUnderArmourIdentifier = "workoutUnderArmourIdentifier"
    case year = "year"
}

open class _UACFWorkoutRecordTrack: NSManagedObject {

    // MARK: - Class methods

    open class func entityName () -> String {
        return "UACFWorkoutRecordTrack"
    }

    open class func entity(managedObjectContext: NSManagedObjectContext) -> NSEntityDescription? {
        return NSEntityDescription.entity(forEntityName: self.entityName(), in: managedObjectContext)
    }

    // MARK: - Life cycle methods

    public override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }

    public convenience init?(managedObjectContext: NSManagedObjectContext) {
        guard let entity = _UACFWorkoutRecordTrack.entity(managedObjectContext: managedObjectContext) else { return nil }
        self.init(entity: entity, insertInto: managedObjectContext)
    }

    // MARK: - Properties

    @NSManaged open
    var consoleUserNumber: String

    @NSManaged open
    var customKey: String

    @NSManaged open
    var day: NSNumber?

    @NSManaged open
    var hour: NSNumber?

    @NSManaged open
    var machineType: String

    @NSManaged open
    var minutes: NSNumber?

    @NSManaged open
    var month: NSNumber?

    @NSManaged open
    var primaryKey: NSNumber?

    @NSManaged open
    var recordID: String

    @NSManaged open
    var seconds: NSNumber?

    @NSManaged open
    var workoutUnderArmourIdentifier: String?

    @NSManaged open
    var year: NSNumber?

    // MARK: - Relationships

}

