import Foundation
import CoreData

@objc(UACFWorkoutRecordTrack)
open class UACFWorkoutRecordTrack: _UACFWorkoutRecordTrack {
	// Custom logic goes here.
    
    
    class func saveWorkoutInCoreData(workoutDictionary: [String: Any], underArmourIdentifier: String?){
        let workout = UACFWorkoutRecordTrack(managedObjectContext: DatabaseController.managedObjectContext)!
        workout.consoleUserNumber = workoutDictionary[WorkoutKeys.CONSOLE_USER_NUMBER] as! String
        workout.recordID = workoutDictionary[WorkoutKeys.RECORD_ID] as! String
        workout.day = NSNumber(value: workoutDictionary[WorkoutKeys.DAY] as! Int)
        workout.month = NSNumber(value: workoutDictionary[WorkoutKeys.MONTH] as! Int)
        workout.year = NSNumber(value: workoutDictionary[WorkoutKeys.YEAR] as! Int)
        workout.seconds = NSNumber(value: workoutDictionary[WorkoutKeys.SECONDS] as! Int)
        workout.minutes = NSNumber(value: workoutDictionary[WorkoutKeys.MINUTES] as! Int)
        workout.hour = NSNumber(value: workoutDictionary[WorkoutKeys.HOUR] as! Int)
        workout.machineType = workoutDictionary[WorkoutKeys.MACHINE_TYPE] as! String
        workout.primaryKey = getPrimaryKey()
        workout.customKey = getCustomKey(workoutDictionary: workoutDictionary)
        workout.workoutUnderArmourIdentifier = underArmourIdentifier
        DatabaseController.saveContext()
    }
    
    
    func toUACFLastworkoutSynchronized() -> UACFLastWorkoutSynchronized?{
        guard let hour = self.hour?.intValue, let minutes = self.minutes?.intValue, let seconds = self.seconds?.intValue, let year = self.year?.intValue, let month = self.month?.intValue, let day = self.day?.intValue, let underArmourIdentifier = workoutUnderArmourIdentifier else {
            return nil
        }
        return UACFLastWorkoutSynchronized(hour: hour, minutes: minutes, seconds: seconds, year: year, month: month, day: day, recordID: self.recordID, underArmourIdentifier: underArmourIdentifier)
    }
    
    class func getLastWorkoutSaved(consoleUserNumber: String, machineType: String) -> UACFLastWorkoutSynchronized?{
        let predicate = NSPredicate(format: "consoleUserNumber = %@ && machineType = %@", consoleUserNumber, machineType)
        if let workout = fetchWorkout(key: "primaryKey", predicate: predicate){
            return workout.toUACFLastworkoutSynchronized()
        }
        return nil
    }
    
    
    class func checkIfWorkoutAlreadyExists(workoutDictionary: [String: Any]) -> Bool{
        let predicate = NSPredicate(format: "customKey = %@", getCustomKey(workoutDictionary: workoutDictionary))
        let workout = fetchWorkout(key: "customKey", predicate: predicate)
        guard let _ = workout else {
            return false
        }
        return true
    }
    
    
    private class func getLastWorkoutSaved() -> UACFWorkoutRecordTrack?{
        return fetchWorkout(key: "primaryKey", predicate: nil)
    }

    private class func fetchWorkout(key: String, predicate: NSPredicate?) -> UACFWorkoutRecordTrack?{
        let managedContext = DatabaseController.managedObjectContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: UACFWorkoutRecordTrack.entityName())
        fetchRequest.fetchLimit = 1
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: key, ascending: false)]
        if let predicate = predicate{
            fetchRequest.predicate = predicate
        }
        do {
            let result = try managedContext.fetch(fetchRequest)
            guard let workout = result.first else {
                return nil
            }
            return workout as? UACFWorkoutRecordTrack
        }
        catch _ as NSError{
            return nil
        }
    }
    
    private class func getPrimaryKey() -> NSNumber{
        let workout = getLastWorkoutSaved()
        guard let workoutUnwrapped = workout else {
            return NSNumber(value: 1)
        }
        return NSNumber(value: workoutUnwrapped.primaryKey!.intValue + 1)
    }
    
    
    private class func getCustomKey(workoutDictionary: [String: Any]) -> String {
        let consoleUserNumber: String = workoutDictionary[WorkoutKeys.CONSOLE_USER_NUMBER] as! String
        let recordID: String = workoutDictionary[WorkoutKeys.RECORD_ID] as! String
        let day: Int = workoutDictionary[WorkoutKeys.DAY] as! Int
        let month: Int = workoutDictionary[WorkoutKeys.MONTH] as! Int
        let year: Int = workoutDictionary[WorkoutKeys.YEAR] as! Int
        let seconds: Int = workoutDictionary[WorkoutKeys.SECONDS] as! Int
        let minutes: Int = workoutDictionary[WorkoutKeys.MINUTES] as! Int
        let hour: Int = workoutDictionary[WorkoutKeys.HOUR] as! Int
        let machineType: String = workoutDictionary[WorkoutKeys.MACHINE_TYPE] as! String
        let customKey = "\(consoleUserNumber)-\(recordID)-\(day)-\(month)-\(year)-\(seconds)-\(minutes)-\(hour)-\(machineType)"
        return customKey
    }
    
    
    
    
}
