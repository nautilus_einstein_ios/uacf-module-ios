//
//  UACFConnectionViewController.swift
//  UnderArmourConnectedFitness
//
//  Created by Cesar Brenes on 5/8/17.
//  Copyright © 2017 Veux Labs. All rights reserved.
//

import UIKit

class UACFConnectionViewController: UIViewController {
    
    @IBOutlet private weak var leftButtonLabel: UILabel!
    @IBOutlet private weak var rightButtonLabel: UILabel!
    @IBOutlet private weak var leftButtonOutlet: UIControl!
    @IBOutlet private weak var rightButtonOutlet: UIControl!
    @IBOutlet private weak var heightOfDescriptionViewConstraint: NSLayoutConstraint!
    
    @IBOutlet private weak var titleScreenLabel: UILabel!
    @IBOutlet private weak var detailScreenLabel: UILabel!
    @IBOutlet private weak var appIconImageView: UIImageView!
    
    
    var backgroundContainerColor = UIColor.white
    var backgroundButtonsColor = UIColor.white
    var titleAttributeString = NSAttributedString()
    var descriptionAttributeString = NSAttributedString()
    var leftButtonAttributeString = NSAttributedString()
    var rightButtonAttributeString = NSAttributedString()
    var appIcon = UIImage()
    var navigationTitle = ""
    var firstGradientColor: UIColor?
    var secondGradientColor: UIColor?
    
    private let percentageHeightOfDescriptionViewConstraintForConnectScreen: CGFloat = 0.227886
    private let percentageHeightOfDescriptionViewConstraintForDisconnectScreen: CGFloat = 0.1664167916
        
        
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
        setLayout()
        setExclusiveTouch()
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(willEnterForeground), name: Notification.Name.UIApplicationDidBecomeActive, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setStateInButtons(enable: true)
    }
    
    func willEnterForeground(notification: NSNotification!) {
        self.setStateInButtons(enable: true)
    }
    
    private func setExclusiveTouch(){
        leftButtonOutlet.isExclusiveTouch = true
        rightButtonOutlet.isExclusiveTouch = true
    }
    
    private func setStateInButtons(enable: Bool){
        leftButtonOutlet.isEnabled = enable
        rightButtonOutlet.isEnabled = enable
    }

    private func initializeView(){
        leftButtonLabel.attributedText = leftButtonAttributeString
        rightButtonLabel.attributedText = rightButtonAttributeString
        leftButtonOutlet.backgroundColor = backgroundButtonsColor
        rightButtonOutlet.backgroundColor = backgroundButtonsColor
        view.backgroundColor = backgroundContainerColor
        titleScreenLabel.attributedText = titleAttributeString
        detailScreenLabel.attributedText = descriptionAttributeString
        appIconImageView.image = appIcon
        self.title = navigationTitle
        setGradientBackground(firstGradientColor: firstGradientColor, secondGradientColor: secondGradientColor)
    }
    
    private func setLayout(){
        let screenSize: CGRect = UIScreen.main.bounds
        var sectionHeight: CGFloat = 0.0
        sectionHeight = screenSize.height * percentageHeightOfDescriptionViewConstraintForConnectScreen
        if UACFAuthentication.sharedInstance.isAlreadyAuthenticated(){
            sectionHeight = screenSize.height * percentageHeightOfDescriptionViewConstraintForDisconnectScreen
        }
        heightOfDescriptionViewConstraint.constant = sectionHeight
        view.layoutIfNeeded()
    }
    

    
    static func initializeView(backgroundContainerColor: UIColor, backgroundButtonsColor: UIColor, titleAttributeString:NSAttributedString, descriptionAttributeString:NSAttributedString, leftButtonAttributeString:NSAttributedString, rightButtonAttributeString:NSAttributedString, appIcon:UIImage, navigationTitle: String, firstGradientColor: UIColor?, secondGradientColor: UIColor?) -> UACFConnectionViewController{
        let underArmourBundle = Constants.bundle
        let viewController = UIStoryboard(name: "UACFConnection", bundle: underArmourBundle).instantiateViewController(withIdentifier: "UACFConnectionViewController") as! UACFConnectionViewController
        viewController.backgroundContainerColor = backgroundContainerColor
        viewController.backgroundButtonsColor = backgroundButtonsColor
        viewController.titleAttributeString = titleAttributeString
        viewController.descriptionAttributeString = descriptionAttributeString
        viewController.leftButtonAttributeString = leftButtonAttributeString
        viewController.rightButtonAttributeString = rightButtonAttributeString
        viewController.appIcon = appIcon
        viewController.navigationTitle = navigationTitle
        viewController.firstGradientColor = firstGradientColor
        viewController.secondGradientColor = secondGradientColor
        return viewController
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func leftButtonAction(_ sender: UIControl) {
        navigationController?.popViewController(animated: true)
    }
  
    @IBAction func rightButtonAction(_ sender: UIControl) {
        if UACFAuthentication.sharedInstance.isAlreadyAuthenticated(){
            showDisconnectAlertController()
        }
        else{
            setStateInButtons(enable: false)
            UACFAuthentication.sharedInstance.requestUserAuthentication(viewController: self, completion: { (error) in
                guard let _ = error else{
                    self.navigationController?.popViewController(animated: true)
                    return
                }
                self.setStateInButtons(enable: true)
                self.showErrorAlertController(error: error)
            })
        }
    }
    
    
    func showErrorAlertController(error: NSError?){
        if let error = error{
            let message = (error.userInfo[NSLocalizedRecoverySuggestionErrorKey] as! String).localize()
            let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "OK".localize(), style: .default, handler: nil)
            alertController.addAction(alertAction)
            present(alertController, animated: true, completion: nil)
        }
    }
    
    
    func showDisconnectAlertController(){
        let alertController = UIAlertController(title: nil, message: "Are you sure you want to disconnect with Under Armour Connected Fitness?".localize(), preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel".localize(), style: .cancel, handler: nil)
        let okAction = UIAlertAction(title: "OK".localize(), style: .default) { (action) in
            UACFAuthentication.sharedInstance.disconnectWithUnderArmour(completion: { (error) in
                guard let _ = error else{
                    _ = self.navigationController?.popViewController(animated: true)
                    return
                }
                self.showErrorAlertController(error: error)
            })
        }
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
    
    
    func setGradientBackground(firstGradientColor: UIColor?, secondGradientColor: UIColor?) {
        if let firstGradientColor = firstGradientColor, let secondGradientColor = secondGradientColor{
            let colorTop =  firstGradientColor.cgColor
            let colorBottom = secondGradientColor.cgColor
            let gradientLayer = CAGradientLayer()
            gradientLayer.colors = [ colorTop, colorBottom]
            gradientLayer.locations = [ 0.0, 1.0]
            gradientLayer.frame = self.view.bounds
            self.view.layer.insertSublayer(gradientLayer, at: 0)
        }
    }

    
}
