//
//  UACFConnectionScreen.swift
//  UnderArmourConnectedFitness
//
//  Created by Cesar Brenes on 5/8/17.
//  Copyright © 2017 Veux Labs. All rights reserved.
//

import Foundation
import UIKit

public class UACFConnectionScreen: NSObject{


    public class func showUnderArmourConnectionScreen(navigationController: UINavigationController, backgroundContainerColor: UIColor, backgroundButtonsColor: UIColor, titleAttributeString:NSAttributedString, descriptionAttributeString:NSAttributedString, leftButtonAttributeString:NSAttributedString, rightButtonAttributeString:NSAttributedString, appIcon:UIImage, navigationTitle: String,firstGradientColor: UIColor?, secondGradientColor: UIColor?){
        let viewController = UACFConnectionViewController.initializeView(backgroundContainerColor: backgroundContainerColor, backgroundButtonsColor: backgroundButtonsColor, titleAttributeString: titleAttributeString, descriptionAttributeString: descriptionAttributeString, leftButtonAttributeString: leftButtonAttributeString, rightButtonAttributeString: rightButtonAttributeString, appIcon: appIcon, navigationTitle: navigationTitle, firstGradientColor: firstGradientColor, secondGradientColor: secondGradientColor)
        navigationController.pushViewController(viewController, animated: true)
    }
    
    


}
