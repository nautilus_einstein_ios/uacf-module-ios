//
//   UACFWorkout.swift
//  UnderArmourConnectedFitness
//
//  Created by Cesar Brenes on 4/28/17.
//  Copyright © 2017 Veux Labs. All rights reserved.
//

import Foundation
import UASDK
import UIKit





public class  UACFWorkout: NSObject {
    

    
    public class func getLastWorkoutSynchronizedWithUnderArmour(consoleUserNumber: String, machineType: String) -> UACFLastWorkoutSynchronized?{
        return UACFWorkoutRecordTrack.getLastWorkoutSaved(consoleUserNumber: consoleUserNumber, machineType: machineType)
    }
    
    
    public class func sendWorkoutToUnderArmourAPI(workout: [String: Any], completion: @escaping (_ error: NSError?) -> Void){
        if Reachability.isConnectedToNetwork() {
            if UACFAuthentication.sharedInstance.isAlreadyAuthenticated(){
                startWorkoutSynchronizationWithUnderArmourInBackgroundThread(workout: workout, completion: { (error) in
                    completion(error)
                })
            }
            else{
                completion(ErrorHandle.createErrorCode(errorCode: Constants.userIsNotAuthenticatedWithUACFErrorCode))
            }
        }
        else{
            completion(ErrorHandle.createErrorCode(errorCode: Constants.notInternetErrorCode))
        }
    }
    
    private class func startWorkoutSynchronizationWithUnderArmourInBackgroundThread(workout: [String: Any], completion: @escaping (_ error: NSError?) -> Void){
            DispatchQueue.global(qos: .background).async {
                validateMandatoryFieldsInTheUAWorkout(workout: workout, completion: { (error) in
                    DispatchQueue.main.async {
                        completion(error)
                    }
                })
        }
    }
    
    
    private class func validateMandatoryFieldsInTheUAWorkout(workout: [String: Any], completion: @escaping (_ error: NSError?) -> Void){
        if UACFWorkoutValidator.hasTheWorkoutTheMandatoryFields(workout: workout){
            if UACFWorkoutValidator.validateDataTypes(workout: workout){
                validateAllUnitsInTheUAWorkout(workout: workout, completion: { (error) in
                    completion(error)
                })
            }
            else{
                completion(ErrorHandle.createErrorCode(errorCode: Constants.parametersHaveWrongUnitErrorCode))
            }
        }
        else{
            completion(ErrorHandle.createErrorCode(errorCode: Constants.parametersMissingErrorCode))
        }
    }
    
    
    private class func validateAllUnitsInTheUAWorkout(workout: [String: Any], completion: @escaping (_ error: NSError?) -> Void){
        if UACFWorkoutValidator.validateAllUnitsInTheWorkoutDictionary(workout: workout) {
            if UACFWorkoutRecordTrack.checkIfWorkoutAlreadyExists(workoutDictionary: workout) {
                completion(ErrorHandle.createErrorCode(errorCode: Constants.workoutAlreadySynchronizedErrorCode))
            }
            else{
                syncWorkoutWithUnderArmour(workout: workout, completion: { (error) in
                    completion(error)
                })
            }
        }
        else{
            completion(ErrorHandle.createErrorCode(errorCode: Constants.unitOrValueAreMissingErrorCode))
        }
    }
    

    private class func syncWorkoutWithUnderArmour(workout: [String: Any], completion:@escaping (_ error: NSError?) -> Void){
        let activityTypeString = workout[WorkoutKeys.ACTIVITY_TYPE] as! WorkoutActivityTypes
        let activityTypeRef = UAActivityTypeReference.activityTypeRef(withEntityId: activityTypeString.rawValue)
        UA.sharedInstance().activityTypeManager.fetchActivityType(withRef: activityTypeRef) { (activityType, error) in
            if error == nil{
                let workoutUAInstance = createUAWorkoutInstance(workout: workout)[Constants.underArmourWorkoutKey] as! UAWorkout
                workoutUAInstance.activityTypeRef = activityTypeRef
            UA.sharedInstance().workoutManager.createWorkout(workoutUAInstance, response: { (workoutUAInstanceResponse, error) in
                    if error == nil{
                        UACFWorkoutRecordTrack.saveWorkoutInCoreData(workoutDictionary: workout, underArmourIdentifier: workoutUAInstanceResponse?.ref.entityID)
                        completion(nil)
                    }
                    else{
                        completion(ErrorHandle.createErrorCode(errorCode: Constants.unexpectedErrorCode))
                    }
                })
            }
            else{
                 completion(ErrorHandle.createErrorCode(errorCode: Constants.unexpectedErrorCode))
            }
        }
    }

    private class func createUAWorkoutInstance(workout: [String: Any]) -> [String: Any]{
        let workoutUAInstance = UAWorkout()
        let finalDate = (workout[WorkoutKeys.START_TIME] as! Date).addingTimeInterval(TimeInterval(-1 * (workout[WorkoutKeys.TIME_ZONE] as! TimeZone).secondsFromGMT()))
        workoutUAInstance.userRef = UA.sharedInstance().authenticatedUserRef
        workoutUAInstance.workoutName = workout[WorkoutKeys.NAME] as! String
        workoutUAInstance.createdDatetime = NSDate() as Date
        workoutUAInstance.startDatetime = finalDate
        workoutUAInstance.startLocaleTimezone = workout[WorkoutKeys.TIME_ZONE] as! TimeZone
        workoutUAInstance.aggregate = UAWorkoutAggregate()
        workoutUAInstance.aggregate.metabolicEnergyTotal = WorkoutUtils.getMetabolicEnergyForUnderArmourAPI(workout: workout)
        workoutUAInstance.aggregate.speedAvg = WorkoutUtils.getSpeedPreparedForUnderArmourAPI(workout: workout)
        workoutUAInstance.aggregate.distanceTotal = WorkoutUtils.getDistancePreparedForUnderArmourAPI(workout: workout)
        workoutUAInstance.aggregate.powerAvg = WorkoutUtils.getPowerAVGForUnderArmourAPI(workout: workout)
        workoutUAInstance.aggregate.elapsedTimeTotal = WorkoutUtils.getElapsedTimeForUnderArmourAPI(workout: workout)
        workoutUAInstance.aggregate.activeTimeTotal = WorkoutUtils.getElapsedTimeForUnderArmourAPI(workout: workout)
        workoutUAInstance.aggregate.heartrateAvg = WorkoutUtils.getHeartRateAVGForUnderArmourAPI(workout: workout)
        return [Constants.underArmourWorkoutKey: workoutUAInstance]
    }

}
